<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h4>联系方式</h4>
                <p>
                    电子邮箱：zhxs.zhyx@qq.com<br/>
                    手机号码：152 1279 3766<br/>
                </p>
            </div><!-- /col-lg-4 -->

            <div class="col-lg-4">
                <h4>友情链接</h4>
                <p>
                    <a target="_blank" href="http://zhxslvzhyx.php.cn/">张显森的博客</a><br/>
                    <a target="_blank" href="http://www.ahnimeng.com">安徽拟梦软件开发有限公司</a><br/>
                    <a target="_blank" href="https://www.baidu.com/">百度一下</a><br/>
                </p>
            </div><!-- /col-lg-4 -->

            <div class="col-lg-4">
                <h4>关于程序猿</h4>
                <p>一个人静静坐在电脑面前写代码的感觉，那是什么感觉？那是武林高手闭关修炼的感觉。</p>
            </div><!-- /col-lg-4 -->

        </div>

    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://apps.bdimg.com/libs/bootstrap/3.0.3/js/bootstrap.min.js"></script>
