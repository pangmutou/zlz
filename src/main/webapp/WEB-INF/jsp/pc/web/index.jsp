<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<base href="<%=basePath%>"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png">

    <title>胖师兄的网站</title>

    <!-- Bootstrap core CSS -->
    <link href="http://apps.bdimg.com/libs/bootstrap/3.0.3/css/bootstrap.css" rel="stylesheet">
    <%--<link href="plugins/assets/css/bootstrap.css" rel="stylesheet">--%>


    <!-- Custom styles for this template -->
    <link href="plugins/assets/css/main.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="plugins/assets/js/hover.zoom.js"></script>
    <script src="plugins/assets/js/hover.zoom.conf.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<style>
    .navbar-inverse {
         padding-bottom: 0px;
         padding-top: 0px;
    }
    .navbar-inverse {
        background-color: #000000;
        border-color: #000000;
    }
    .navbar-inverse .navbar-nav > li > a {
        font-size: 16px;
    }
    #nav_area a:hover{
        color: red;
    }
</style>

<body>

<!-- Static navbar -->
<div class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">胖师兄的网站</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul id="nav_area" class="nav navbar-nav navbar-right">
                <li><a href="web/blog/list">博客</a></li>
                <li><a href="work.html">工作</a></li>
                <li><a href="about.html">学习</a></li>
                <li><a href="blog.html">生活</a></li>
                <li><a href="blog.html">梦想</a></li>
                <li><a href="contact.html">简历</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<!-- +++++ Welcome Section +++++ -->
<div id="ww">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 centered">
                <img src="https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1394918753,1150100561&fm=15&gp=0.jpg" alt="Stanley">
                <h1>欢迎进入胖师兄的博客！</h1>
                <p>如果调试一个程序让你很苦恼，千万不要放弃，成功永远在拐角之后，除非你走到拐角，否则你永远不知道你离他多远，所以，请记住，坚持不懈，直到成功。</p>
                <p>为什么很多人都想成功，但成功的人只是极少数？不是因为成功有多困难，其实成功简单得很，只要你一开始都有计划，并且能一直做下去，总会成功的。关键是如何面对成功，你失败了，起码还有人同情你，而成功了，连得到同情的机会都没有。</p>

            </div><!-- /col-lg-8 -->
        </div><!-- /row -->
    </div> <!-- /container -->
</div><!-- /ww -->
<div class="copyrights">Collect from <a href="http://www.cssmoban.com/" >企业网站模板</a></div>


<!-- +++++ Projects Section +++++ -->

<%--<div class="container pt">--%>
    <%--<div class="row mt centered">--%>
        <%--<div class="col-lg-4">--%>
            <%--<a class="zoom green" href="work01.html"><img class="img-responsive" src="plugins/assets/img/portfolio/port01.jpg" alt="" /></a>--%>
            <%--<p>APE</p>--%>
        <%--</div>--%>
        <%--<div class="col-lg-4">--%>
            <%--<a class="zoom green" href="work01.html"><img class="img-responsive" src="plugins/assets/img/portfolio/port02.jpg" alt="" /></a>--%>
            <%--<p>RAIDERS</p>--%>
        <%--</div>--%>
        <%--<div class="col-lg-4">--%>
            <%--<a class="zoom green" href="work01.html"><img class="img-responsive" src="plugins/assets/img/portfolio/port03.jpg" alt="" /></a>--%>
            <%--<p>VIKINGS</p>--%>
        <%--</div>--%>
    <%--</div><!-- /row -->--%>
    <%--<div class="row mt centered">--%>
        <%--<div class="col-lg-4">--%>
            <%--<a class="zoom green" href="work01.html"><img class="img-responsive" src="plugins/assets/img/portfolio/port04.jpg" alt="" /></a>--%>
            <%--<p>YODA</p>--%>
        <%--</div>--%>
        <%--<div class="col-lg-4">--%>
            <%--<a class="zoom green" href="work01.html"><img class="img-responsive" src="plugins/assets/img/portfolio/port05.jpg" alt="" /></a>--%>
            <%--<p>EMPERORS</p>--%>
        <%--</div>--%>
        <%--<div class="col-lg-4">--%>
            <%--<a class="zoom green" href="work01.html"><img class="img-responsive" src="plugins/assets/img/portfolio/port06.jpg" alt="" /></a>--%>
            <%--<p>CHIEFS</p>--%>
        <%--</div>--%>
    <%--</div><!-- /row -->--%>
<%--</div><!-- /container -->--%>


<!-- +++++ Footer Section +++++ -->
<%@include file="inc/footer.jsp" %>
</body>
</html>

