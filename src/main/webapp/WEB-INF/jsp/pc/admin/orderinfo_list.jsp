<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>订单列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="${url}" method="get">
                            <fieldset>
                                <div class="row-fluid" style="margin-bottom: 20px;">
                                    <div class="col-md-12">
                                        <label class="control-label" for="orderSn">订单编号</label>
                                        <input class="input-medium focused" type="text" id="orderSn" name="orderSn" placeholder="请输入订单编号" value="${param.orderSn}">
                                        <button type="submit" class="btn btn-primary" onclick="$('#currentPageNo').val(1)"><i class="icon-search"></i>查询</button>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>订单编号</th>
                                        <th>收货人</th>
                                        <th>订单状态</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.orderSn}</td>
                                            <td>${item.consignee}</td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${item.orderStatus == 0}">待付款</c:when>
                                                    <c:when test="${item.orderStatus == 1}">待发货</c:when>
                                                    <c:when test="${item.orderStatus == 2}">待收货</c:when>
                                                    <c:when test="${item.orderStatus == 10}">交易成功</c:when>
                                                    <c:when test="${item.orderStatus == 20}">退货</c:when>
                                                    <c:when test="${item.orderStatus == 31}">退款成功</c:when>
                                                    <c:otherwise>未知的订单状态</c:otherwise>
                                                </c:choose>
                                            <td class="center">
                                                <a href="order/info?orderId=${item.orderId}" class="btn btn-primary">订单详细</a>
                                                <%--<div class="btn-group group" style="float:left;margin-right:4px;">--%>
                                                    <%--<button class="btn dropdown-toggle" data-toggle="dropdown">--%>
                                                        <%--<i class="icon-wrench"></i>操作<span class="caret"></span>--%>
                                                    <%--</button>--%>
                                                    <%--<ul class="dropdown-menu">--%>
                                                        <%--<li><a href="order/info?orderId=${item.orderId}">订单详细</a>--%>
                                                        <%--</li>--%>
                                                        <%--<li>--%>
                                                            <%--<a href="javascript:showModal('删除','您确定删除吗？','delProduct(${item.orderId})');">删除</a>--%>
                                                        <%--</li>--%>
                                                    <%--</ul>--%>
                                                <%--</div>--%>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function delProduct(id) {
        urlUtils("orderinfo/delete?orderId=" + id);
    }
</script>
</html>