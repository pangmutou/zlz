<%--
  Created by IntelliJ IDEA.
  User: zhangxiansen
  Date: 2016/4/13
  Time: 17:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>订单商品信息列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="${url}" method="post">
                            <fieldset>
                                <div class="control-group" >
                                    <label class="control-label" for="productName">订单编号</label>
                                    <div class="controls" >
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="productName"
                                                   name="goodsName" placeholder="请输入订单编号" value="${order.orderSn}" >
                                        </div>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <button type="submit" class="btn" ><i class="icon-search" ></i>查询</button>
                                        ${button}
                                        <button type="button" class="btn"
                                                onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回
                                        </button>
                                        <a href="goods/add" class="btn">添加</a>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>订单编号</th>
                                        <th>订单状态</th>
                                        <th>支付状态</th>
                                        <th>商品名称</th>
                                        <th>商品数量</th>
                                        <th>商品价格</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.orderSn}</td>
                                            <td>${item.orderStatus==0?"<span>未确认</span>":"<span>已确认</span>"}</td>
                                            <td>${item.payStatus==0?"<span>未支付</span>":"<span>已支付</span>"}</td>
                                            <td>${item.goodsName}</td>
                                            <td>${item.goodsNumber}</td>
                                            <td>${item.goodsPrice}</td>
                                            <td class="center">
                                                <div class="btn-group group" style="float:left;margin-right:4px;" >
                                                    <button class="btn dropdown-toggle" data-toggle="dropdown" ><i class="icon-wrench"></i>操作<span class="caret" ></span></button>
                                                    <ul class="dropdown-menu" >
                                                        <li><a href="goods/detail?goodsId=${item.goodsId}">详细</a></li>
                                                        <li><a href="javascript:showModal('删除','您确定删除吗？','delOrderGoods(${item.recId})');">删除</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage" ></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event,obj,type,page){
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function delOrderGoods(id){
        urlUtils("ordergoods/delete?recId="+id);
    }
</script>
</html>
