<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}</h2>
                    </div>
                    <div class="box-content">
                        <form action="org/category/${method}" method="post" class="form-horizontal">
                            <div class="control-group">
                                <label for="input1" class="control-label">类目名</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="input1" name="itemName" value="${object.itemName}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="input2" class="control-label">类目值</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="input2" name="itemCode" value="${object.itemCode}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="input3" class="control-label">顺序</label>
                                <div class="controls">
                                    <input type="text" class="form-control" id="input3" name="orderby" value="${object.orderby}">
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <input type="hidden" value="${object.id}" name="id"/>
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <%--<button type="reset" class="btn btn-default">重置</button>--%>
                                    <button type="button" class="btn btn-default"
                                            onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
</html>