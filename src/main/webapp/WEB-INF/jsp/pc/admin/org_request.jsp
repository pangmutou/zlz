<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <ry:binding parentCode="USERSEX" bingdingName="customersexs"></ry:binding>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>

        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}</h2>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="span4">
                                <form action="org/request/${method}" method="post" class="form-horizontal"
                                      onsubmit="return $(this).validate()">
                                    <div class="control-group">
                                        <label class="control-label" for="requestUserName">姓名</label>
                                        <div class="controls">
                                            <input type="text" id="requestUserName" name="requestUserName"
                                                   placeholder="请输入申请人姓名" class="userHandle" valitype="require"
                                                   value="${orgRequest.requestUserName}">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="requestPhone">手机号</label>
                                        <div class="controls">
                                            <input type="text" id="requestPhone" name="requestPhone"
                                                   placeholder="请输入手机号码" class="userHandle" valitype="require"
                                                   value="${orgRequest.requestPhone}">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="requestTel">座机号</label>
                                        <div class="controls">
                                            <input type="text" id="requestTel" name="requestTel"
                                                   placeholder="请输入座机号码" class="userHandle" valitype="require"
                                                   value="${orgRequest.requestTel}">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="identityCardNo">身份证</label>
                                        <div class="controls">
                                            <input type="text" id="identityCardNo" name="identityCardNo"
                                                   placeholder="请输入身份证号" class="userHandle" valitype="require"
                                                   value="${orgRequest.identityCardNo}">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="orgName">商铺名称</label>
                                        <div class="controls">
                                            <input type="text" id="orgName" name="orgName" placeholder="请输入营业执照上的名称"
                                                   value="${orgRequest.orgName}" class="userHandle" valitype="require">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="orgAddress">商铺地址</label>
                                        <div class="controls">
                                            <input type="text" id="orgAddress" name="orgAddress"
                                                   placeholder="请录入营业执照上面的地址"
                                                   value="${orgRequest.orgAddress}" class="userHandle"
                                                   valitype="require">
                                        </div>
                                    </div>
                                    <%--<div class="control-group">--%>
                                    <%--<label class="control-label" for="orgAddress">地区</label>--%>
                                    <%--<div class="controls">--%>
                                    <%--<select name="areaCode" data-rel="chosen" valitype="require" ${method == 'check' ? 'readonly' : ''}>--%>
                                    <%--<c:forEach items="${areaList}" var="item">--%>
                                    <%--<option value="${item.areaCode}" ${orgRequest.areaCode == item.areaCode ? 'checked' : ''}>${item.areaName}</option>--%>
                                    <%--</c:forEach>--%>
                                    <%--</select>--%>
                                    <%--<span class="help-inline">必选</span>--%>
                                    <%--</div>--%>
                                    <%--</div>--%>
                                    <div class="control-group">
                                        <label class="control-label">所在小区</label>
                                        <div class="controls">
                                            <div class="span4" style="width: auto;">
                                                <select data-placeholder="选择所在小区" class="userHandle"
                                                        name="communityId" data-rel="chosen"
                                                        valitype="require" ${method == 'check' ? 'readonly' : ''}>
                                                    <c:forEach items="${communityList}" var="item">
                                                        <option value="${item.communityId}">${item.communityName}</option>
                                                    </c:forEach>
                                                </select>
                                                <span class="help-inline">必选</span>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div class="control-group">--%>
                                    <%--<label class="control-label">店铺类型</label>--%>
                                    <%--<div class="controls">--%>
                                    <%--<div class="span4" style="width: auto;">--%>
                                    <%--<select data-placeholder="选择店铺类型" class="userHandle"--%>
                                    <%--name="flag1" onchange="queryNorm()" data-rel="chosen" valitype="require" ${method == 'check' ? 'readonly' : ''}>--%>
                                    <%--<c:forEach items="${orgCategoryList}" var="item">--%>
                                    <%--<option value="${item.itemCode}">${item.itemName}</option>--%>
                                    <%--</c:forEach>--%>
                                    <%--</select>--%>
                                    <%--<span class="help-inline">必选</span>--%>
                                    <%--</div>--%>
                                    <%--</div>--%>
                                    <%--</div>--%>
                                    <div class="control-group">
                                        <label class="control-label" for="orgAddress">备注</label>
                                        <div class="controls">
                                            <textarea name="remark" placeholder="您可以添加更多的信息，以便审核"
                                                      class="userHandle"
                                                      style="width: 20em; height: 10em;">${orgRequest.remark}</textarea>
                                        </div>
                                    </div>
                                    <c:if test="${method == 'check'}">
                                        <div class="control-group">
                                            <label class="control-label">审核结果</label>
                                            <div class="controls">
                                                <label class="radio inline">
                                                    <input type="radio" name="handleResult" value="1" required>通过
                                                </label>
                                                <label class="radio inline">
                                                    <input type="radio" name="handleResult" value="0" required>拒绝
                                                </label>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="handleRemark">审核备注</label>
                                            <div class="controls">
                                                <textarea type="text" id="handleRemark" name="handleRemark"
                                                          placeholder="请输入审核备注信息"
                                                          class="serviceHandle" style="width: 20em; height: 10em;"
                                                          required>${orgRequest.handleRemark}</textarea>
                                            </div>
                                        </div>
                                    </c:if>
                                    <div class="control-group">
                                        <div class="controls">
                                            <input name="requestId" value="${orgRequest.requestId}" type="hidden">
                                            <button type="submit" class="btn btn-primary">提交</button>
                                            <button type="reset" class="btn btn-default">重置</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/span-->
        </div><!--/row-->
    </div>
    <!--/span-->
</div>
<!--/row-->
<!-- content ends -->
</div>
<!--/#content.span10-->
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<!-- Ajax验证手机号是否存在 -->
<script type="text/javascript">
    $("#shopType").find("option[value='${orgRequest.flag1}']").attr("selected", "selected");
    if ('${method}' == "check") {
        $(".userHandle").attr("readonly", "readonly");
    }
    var result = true;
    function searchCustomer() {
        var phone = $('#customerTelephone').val();
        var id = $('#customerId').val();
        var url = "customer/queryCustomer?customerId=" + id;
        var bool = true;
        $.ajax({
            type: "post",
            url: url,
            dataType: "json",
            data: {phone: phone},
            success: function (customer) {
                if (customer != null) {
                    $("#customerTelephone").lightHelp(0, 1);
                    $("#customerTelephone").eachSelect(".controls").find(".help-inline").html("手机号码已存在");
                    result = false;
                } else {
                    if ($("#customerTelephone").validate()) {
                        $("#customerTelephone").lightHelp(" ", 1);
                        $("#customerTelephone").eachSelect(".controls").find(".help-inline").html("");
                        result = true;
                    }
                }
            }
        });
        return result;
    }

    $("#customerTelephone").blur(function () {
        searchCustomer();
    });

    function checkForm() {
        var v_result = searchCustomer();
        var v_form = $("#customerForm").validate();
        if (v_result && v_form) {
            $("#customerForm").submit();
        }
    }
</script>
</html>