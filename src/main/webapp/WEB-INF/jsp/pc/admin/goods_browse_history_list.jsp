<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>浏览商品历史记录列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="${url}" method="post">
                            <fieldset>
                                <div class="control-group" >
                                    <label class="control-label" for="productName">商品名称</label>
                                    <div class="controls" >
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="productName"
                                                   name="goodsName" placeholder="请输入商品名" value="${goods.goodsName}" >
                                        </div>
                                    </div>
                                    ${search}
                                    <label class="control-label" for="orgName">所属店铺</label>
                                    <div class="span4" style="width: auto;">
                                        <input class="input-medium focused" type="text" id="orgName"
                                               name="orgName" placeholder="请输入所属店铺名" value="${org.orgName}" >
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <button type="submit" class="btn" ><i class="icon-search" ></i>查询</button>
                                        ${button}
                                        <button type="button" class="btn"
                                                onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回
                                        </button>
                                        <a href="goods/add" class="btn">添加</a>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>商品名称</th>
                                        <th>图片</th>
                                        <th>商品库存数量</th>
                                        <th>本店价格</th>
                                        <th>商品搜索关键字</th>
                                        <th>商品简介</th>
                                        <th>创建时间</th>
                                        <th>所属店铺</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.goodsName}</td>
                                            <td><img width="50px"  height="50px" src="${item.goodsImg}"/></td>
                                            <td>${item.goodsNumber}</td>
                                            <td>${item.shopPrice}</td>
                                            <td><a href="#">点击查看</a></td>
                                            <td><a href="#">点击查看</a></td>
                                            <td>${item.addTime}</td>
                                            <td><a href="#">所属店铺</a></td>
                                            <td class="center">
                                                <div class="btn-group group" style="float:left;margin-right:4px;" >
                                                    <button class="btn dropdown-toggle" data-toggle="dropdown" ><i class="icon-wrench"></i>操作<span class="caret" ></span></button>
                                                    <ul class="dropdown-menu" >
                                                            <%--<li><a href="javascript:void(0);" onclick="urlUtils('order/product/add?productId=${item.product_id}')"; >加入购物车</a></li>--%>
                                                        <li><a href="goods/update?goodsId=${item.goodsId}">编辑菜单</a></li>
                                                            <%--<li><a href="goods/add" >添加菜单</a></li>--%>
                                                        <li><a href="javascript:showModal('删除','您确定删除吗？','delProduct(${item.goodsId})');">删除</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage" ></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event,obj,type,page){
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function delProduct(id){
        urlUtils("product/delete?productId="+id);
    }
</script>
</html>