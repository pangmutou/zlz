<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>订单列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="order/page" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="shopName">客户姓名</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="shopName"
                                                   name="shopName" placeholder="请输入客户姓名" value="${shop.shopName}">
                                        </div>
                                        <div class="span3" style="width: auto;">
                                            <button type="submit" class="btn"><i class="icon-search"></i>查询</button>
                                            <a href="shop/edit" class="btn btn-primary"><i
                                                    class="icon-plus icon-white"></i>添加</a>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>订单编号</th>
                                        <th>用户名</th>
                                        <th>手机号码</th>
                                        <th>收货地址</th>
                                        <th>付款状态</th>
                                        <th>订单总金额</th>
                                        <th>订单状态</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.orderNo}</td>
                                            <td>${item.createUserName}</td>
                                            <td>${item.userPhone}</td>
                                            <td>${item.userAddress}</td>
                                            <td>${item.payStatus}</td>
                                            <td>${item.orderTotal}</td>
                                            <td>${item.orderStatus}</td>
                                            <td class="center">
                                                <div class="btn-group group" style="float:left;margin-right:4px;">
                                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><i
                                                            class="icon-wrench"></i>操作<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                            <%-- <li><a href="shop/shop?shopId=${item.shopId}" >进入店铺</a></li>
                                                             <li><a href="menu/edit?shopId=${item.shopId}" >添加菜单</a></li>--%>
                                                        <li><a href="javascript:void(0);"
                                                               onclick="urlUtils('shop/update?shopId=${item.orderNo}')"
                                                               ;>编辑</a></li>
                                                        <li>
                                                            <a href="javascript:showModal('删除','您确定删除吗？','delShop(${item.orderNo})');">删除</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}"
                                     totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <!--/span-->

            </div>
            <!--/row-->

            <!-- content ends -->
        </div>
        <!--/#content.span10-->

    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function delShop(id) {
        urlUtils("shop/delShop?shopId=" + id);
    }
</script>
</html>