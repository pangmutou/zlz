<%@ page import="com.ahnimeng.common.util.SpringContextUtil" %>
<%@ page import="com.ahnimeng.system.model.TDictionary" %>
<%@ page import="com.ahnimeng.system.service.DictionaryService" %>
<%@ page import="org.hibernate.criterion.Criterion" %>
<%@ page import="org.hibernate.criterion.Order" %>
<%@ page import="org.hibernate.criterion.Restrictions" %>
<%@ page import="java.util.List" %>
<%@ page import="com.ahnimeng.system.model.TArea" %>
<%@ page import="com.ahnimeng.system.service.AreaService" %>
<%
    DictionaryService dictionaryService = SpringContextUtil.getBean(DictionaryService.class);
    List orgCategoryList = dictionaryService.list(TDictionary.class, new Criterion[]{Restrictions.eq("parentCode", "ORG_CATEGORY")}, new Order[]{Order.desc("orderby")}, 0, Integer.MAX_VALUE);
    request.setAttribute("orgCategoryList", orgCategoryList);

    AreaService areaService = SpringContextUtil.getBean(AreaService.class);
    List areaList = areaService.getAll(TArea.class);
    request.setAttribute("areaList", areaList);

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">

        <div id="content" class="span12">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well">
                        <h2><i class="icon-edit"></i>申请开店</h2>
                    </div>
                    <div class="box-content">
                        <form action="register" method="post" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">手机号(登录账号)</label>
                                <div class="controls">
                                    <input type="text" name="requestPhone" placeholder="请输入手机号码" value="${orgRequest.requestPhone}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">店主姓名</label>
                                <div class="controls">
                                    <input type="text" name="requestUserName" placeholder="请输入申请人姓名" value="${orgRequest.requestUserName}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">座机号</label>
                                <div class="controls">
                                    <input type="text" name="requestTel" placeholder="请输入座机号码" value="${orgRequest.requestTel}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">身份证</label>
                                <div class="controls">
                                    <input type="text" name="identityCardNo" placeholder="请输入身份证号" value="${orgRequest.identityCardNo}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">商铺名称</label>
                                <div class="controls">
                                    <input type="text" name="orgName" placeholder="请输入商铺名称" value="${orgRequest.orgName}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">商铺地址</label>
                                <div class="controls">
                                    <input type="text" name="orgAddress" placeholder="请输入商铺地址" value="${orgRequest.orgAddress}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">店铺类型</label>
                                <div class="controls">
                                    <div class="span4" style="width: auto;">
                                        <select data-placeholder="选择店铺类型" name="flag1">
                                            <c:forEach items="${orgCategoryList}" var="item">
                                                <option value="${item.itemCode}">${item.itemName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">地区</label>
                                <div class="controls">
                                    <div class="span4" style="width: auto;">
                                        <select name="areaCode" data-rel="chosen">
                                            <c:forEach items="${areaList}" var="item">
                                                <option value="${item.areaCode}">${item.areaName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注</label>
                                <div class="controls">
                                    <textarea name="remark" placeholder="您可以添加更多的信息，以便审核" style="width: 20em; height: 10em;">${orgRequest.remark}</textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="reset" class="btn btn-default">重置</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<!-- Ajax验证手机号是否存在 -->
<script type="text/javascript">
    $("#shopType").find("option[value='${orgRequest.flag1}']").attr("selected", "selected");
    if ('${method}' == "check") {
        $(".userHandle").attr("readonly", "readonly");
    }
    var result = true;
    function searchCustomer() {
        var phone = $('#customerTelephone').val();
        var id = $('#customerId').val();
        var url = "customer/queryCustomer?customerId=" + id;
        var bool = true;
        $.ajax({
            type: "post",
            url: url,
            dataType: "json",
            data: {phone: phone},
            success: function (customer) {
                if (customer != null) {
                    $("#customerTelephone").lightHelp(0, 1);
                    $("#customerTelephone").eachSelect(".controls").find(".help-inline").html("手机号码已存在");
                    result = false;
                } else {
                    if ($("#customerTelephone").validate()) {
                        $("#customerTelephone").lightHelp(" ", 1);
                        $("#customerTelephone").eachSelect(".controls").find(".help-inline").html("");
                        result = true;
                    }
                }
            }
        });
        return result;
    }

    $("#customerTelephone").blur(function () {
        searchCustomer();
    });

    function checkForm() {
        var v_result = searchCustomer();
        var v_form = $("#customerForm").validate();
        if (v_result && v_form) {
            $("#customerForm").submit();
        }
    }
</script>
</html>