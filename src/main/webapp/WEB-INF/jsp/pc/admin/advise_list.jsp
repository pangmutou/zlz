<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>
                            投诉与建议
                        </h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="workorder/list" method="get">
                            <fieldset>
                                <%--<div class="control-group">--%>
                                <%--<label class="control-label">标题</label>--%>
                                <%--<div class="controls">--%>
                                <%--<div class="span4" style="width: auto;">--%>
                                <%--<input class="input-medium" type="text" name="title" placeholder="标题" value="${workorder.title}">--%>
                                <%--</div>--%>
                                <%--<div class="span3" style="width: auto;">--%>
                                <%--<button type="submit" class="btn btn-primary">查询</button>--%>
                                <%--&lt;%&ndash;<a href="workorder/add" class="btn btn-success"><i class="icon-plus icon-white"></i>创建工单</a>&ndash;%&gt;--%>
                                <%--</div>--%>
                                <%--</div>--%>
                                <%--</div>--%>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>内容</th>
                                        <th>创建时间</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.id}</td>
                                            <td>${item.content}</td>
                                            <td>
                                                <fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate></td>
                                            <td>
                                                <a href="advise/detail?id=${item.id}">详情</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
</script>
</html>