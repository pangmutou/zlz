<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>百度地图</title>
    <style type="text/css">
        html, body {
            margin: 0;
            padding: 0;
            overflow: hidden;
            width: 100%;
            height: 100%;
            /*background: #EEEEEE;*/
        }

        .search {
            width: 80%;
            height: auto;
            margin: 3px 0 15px 0;
            text-align: center;
        }

        .searchText {
            width: 150px;
            height: 20px;
        }

        .search button {
            height: 28px;
            width: 50px;
        }
        .form-control{
            background: #EEEEEE;
        }
    </style>
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <%--<script src="//cdn.bootcss.com/jquery/3.0.0-beta1/jquery.js"></script>--%>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
</head>
<body>
<div class="search">
    <label>要查询的地址：</label>
    <input id="search_input" type="text" class="searchText"/>
    <label>查询结果(经纬度)：</label>
    <input id="longitude" type="text" class="searchText form-control" readonly/>
    <input id="latitude" type="text" class="searchText form-control" readonly/>
    <input type="button" value="查询" onclick="searchByStationName();"/>
    <%--<input type="button" value="保存" onclick="javascript:parent.choseRepairStation();" />--%>
    <input id="province" type="hidden">
    <input id="city" type="hidden">
    <input id="district" type="hidden">
</div>
<div id="mapContent" style="height:85%;border:#ccc solid 1px;margin:0 auto;"></div>
</body>
<script type="text/javascript">
    var map = new BMap.Map("mapContent"),
            point = new BMap.Point(116.404, 39.915),
            marker = new BMap.Marker(point);          // 创建标注，为要查询的地方对应的经纬度
    marker.enableDragging();
    var localSearch = new BMap.LocalSearch(map),
            gc = new BMap.Geocoder();//地址解析类

    marker.addEventListener("dragend", function (e) {
        //获取地址信息
        gc.getLocation(e.point, function (rs) {
            showLocationInfo(e.point, rs);
        });
    });

    localSearch.setSearchCompleteCallback(function (searchResult) {
        map.clearOverlays();//清空原来的标注
        var poi = searchResult.getPoi(0);
        if (!poi) {
            alert("未找到地址");
            return;
        }
        $("#longitude").val(poi.point.lng);
        $("#latitude").val(poi.point.lat);
        map.centerAndZoom(poi.point, 14);
        marker.setPosition(poi.point);
        map.addOverlay(marker);
    });

    function searchByStationName() {
        var placeName = document.getElementById("search_input").value;
        if (placeName) {
            try {
                localSearch.search(placeName);
            } catch (e) {
                alert(e.message);
            }
        }
    }

    //显示地址信息窗口
    function showLocationInfo(pt, rs) {
        try {
            var addComp = rs.addressComponents;
            $("#province").val(addComp.province);
            $("#city").val(addComp.city);
            $("#district").val(addComp.district);
            document.getElementById("search_input").value = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
            document.getElementById("longitude").value = pt.lng;
            document.getElementById("latitude").value = pt.lat;
        } catch (e) {
            alert(e.message);
        }
    }

    $(function () {
        pageInit:{
            map.centerAndZoom(point, 12);     // 初始化地图,设置中心点坐标和地图级别
            map.enableScrollWheelZoom();    //启用滚轮放大缩小，默认禁用
            map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用
            map.addOverlay(marker);
        }
    })
</script>
</html>