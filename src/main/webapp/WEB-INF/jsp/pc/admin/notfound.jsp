<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<html lang="en">
  <head>
    <meta charset="UTF-8">
		<!--<link href='http://fonts.googleapis.com/css?family=Creepster|Audiowide' rel='stylesheet' type='text/css'>-->
		
		<style>
			*{
				margin:0;
				padding:0;
			}
			body{
				font-family: 'Audiowide', cursive, arial, helvetica, sans-serif;
				background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAUElEQVQYV2NkYGAwBuKzQAwDID4IoIgxIikAMZE1oRiArBDdZBSNMIXoJiFbDZYDKcSmCOYimDuNSVKIzRNYrUYOFuQgweoZbIoxgoeoAAcAEckW11HVTfcAAAAASUVORK5CYII=) repeat;
				background-color:#212121;
				color:white;
				font-size: 18px;
				padding-bottom:20px;
			}
			@media (min-width: 1200px) {
				.error-code{
					font-family: 'Creepster', cursive, arial, helvetica, sans-serif;
					font-size: 200px;
					color: white;
					color: rgba(255, 255, 255, 0.98);
					width: 50%;
					text-align: right;
					margin-top: 5%;
					text-shadow: 5px 5px hsl(0, 0%, 25%);
					float: left;
				}
			}
			@media (min-width: 768px) and (max-width: 979px) {
				.error-code{
					font-family: 'Creepster', cursive, arial, helvetica, sans-serif;
					font-size: 100px;
					color: white;
					color: rgba(255, 255, 255, 0.98);
					width: 50%;
					text-align: right;
					margin-top: 5%;
					text-shadow: 5px 5px hsl(0, 0%, 25%);
					float: left;
				}
			}
			@media (max-width: 767px) {
				.error-code{
					font-family: 'Creepster', cursive, arial, helvetica, sans-serif;
					font-size: 50px;
					color: white;
					color: rgba(255, 255, 255, 0.98);
					width: 50%;
					text-align: right;
					margin-top: 5%;
					text-shadow: 5px 5px hsl(0, 0%, 25%);
					float: left;
				}
			}
			@media (min-width: 1200px) {
				.not-found{
					width: 47%;
					float: right;
					margin-top: 5%;
					font-size: 20px;
					color: white;
					text-shadow: 2px 2px 5px hsl(0, 0%, 61%);
					padding-top: 250px;
				}
			}
			@media (min-width: 768px) and (max-width: 979px) {
				.not-found{
					width: 47%;
					float: right;
					margin-top: 5%;
					font-size: 10px;
					color: white;
					text-shadow: 2px 2px 5px hsl(0, 0%, 61%);
					padding-top: 200px;
				}
			}
			@media (max-width: 767px) {
				.not-found{
					width: 47%;
					float: right;
					margin-top: 5%;
					font-size: 10px;
					color: white;
					text-shadow: 2px 2px 5px hsl(0, 0%, 61%);
					padding-top: 80px;
				}
			}
			.clear{
				float:none;
				clear:both;
			}
			.content{
				text-align:center;
				line-height: 30px;
			}
			input[type=text]{
				border: hsl(247, 89%, 72%) solid 1px;
				outline: none;
				padding: 5px 3px;
				font-size: 16px;
				border-radius: 8px;
			}
			a{
				text-decoration: none;
				color: #9ECDFF;
				text-shadow: 0px 0px 2px white;
			}
			a:hover{
				color:white;
			}

		</style>
		<title>系统提示</title>
	</head>
	<body>
		<p class="error-code">
			Sorry
		</p>
		<p class="not-found" >检查网络，重新连接...</p>
		<div class="clear"></div>
		<div class="content">
			<br/><form><a href="<c:url value="/login" />" >重新登录</a></form>
		</div>
	</body>
</html>