<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <script type="text/template" id="qq-template">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector">Drop files here to upload</span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Upload a file</div>
            </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
            <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <div class="qq-thumbnail-wrapper">
                        <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                    </div>
                    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                    <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                        <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                        Retry
                    </button>

                    <div class="qq-file-info">
                        <div class="qq-file-name">
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>
                        </div>
                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                        <span class="qq-upload-size-selector qq-upload-size"></span>
                        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                            <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                            <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                            <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                        </button>
                    </div>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>

    </script>

    <%--<link href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.1.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>--%>
    <link href="js/fine-uploader-master/client/fineuploader-gallery.css" rel="stylesheet" type="text/css"/>
    <link href="js/fine-uploader-master/client/fineuploader-new.css" rel="stylesheet" type="text/css"/>
    <!--<link href="../../client/fineuploader.css" rel="stylesheet" type="text/css"/>-->
    <link href="js/fine-uploader-master/styles.css" rel="stylesheet" type="text/css"/>
    <script src="js/fine-uploader-master/_build/all.fine-uploader.js"></script>
    <script src="js/fine-uploader-master/devenv.js"></script>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>

        <div id="content" class="span10">
            <!-- content starts -->

            <ul id="foobar"></ul>
            <h1>Fine Uploader Development</h1>

            <!--<img id="paste-test-img" src="http://fineuploader.smartimage.com/thumbnail/c7DolT/960px/Fine-Uploader-Avatar.png?ref=ce8936f6e"/>-->

            <div id="examples">
                <div class="example">
                    <h3>Manually Trigger Uploads</h3>
                    <ul id="manual-example" class="unstyled"></ul>
                    <button type="button" id="triggerUpload" class="btn btn-primary">Upload Queued Files</button>
                </div>
            </div>

            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Home</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="#">测试控件页面</a>
                    </li>
                </ul>
            </div>
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i> 这里面专门就来测试控件</h2>
                    </div>
                    <div class="box-content">
                        <div class="container">
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload" type="file" name="files" multiple>
                            </span>
                            <br>
                            <br>
                            <!-- The global progress bar -->
                            <%--<div id="progress" class="progress">--%>
                                <%--<div class="progress-bar progress-bar-success"></div>--%>
                            <%--</div>--%>

                            <div id="progress" class="progress progress-success" style="width: 200px;">
                                <div class="bar progress-bar" style="" data-max="20" data-min="0"></div>
                            </div>
                            <!-- The container for the uploaded files -->
                            <div id="files" class="files"></div>
                            <br>
                        </div>
                        <form action="example/upload" enctype="multipart/form-data" method="post">
                            <input name="files" type="file"/>
                            <button type="submit">上传</button>
                        </form>
                        <canvas id="canvas" style=""/>
                        <%--这上面放控件--%>
                    </div>
                </div>
                <!--/span-->

            </div>
            <!--/row-->
			<div id="imgValue"></div>
            <!-- content ends -->
        </div>
        <!--/#content.span10-->

    </div>
</div>
<%--<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>--%>
<script src="http://apps.bdimg.com/libs/jquery/1.11.3/jquery.js"></script>
<script src="js/jQuery-File-Upload-9.11.2/js/vendor/jquery.ui.widget.js"></script>
<script src="http://apps.bdimg.com/libs/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="js/jQuery-File-Upload-9.11.2/js/jquery.fileupload.js"></script>

<script type="text/javascript">
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'example/upload';
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
//                $.each(data.result, function (index, file) {
                    $('<p/>').text(data.result.msg).appendTo('#files');
//                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
    })
$(':file').on('change',function(){
    var file = this.files[0];
    var url = URL.createObjectURL(file);

    /* 生成图片
    * ---------------------- */
    var $img = new Image();
    $img.onload = function() {

        //生成比例
        var width = $img.width,
                height = $img.height,
                scale = width / height;
        width = parseInt(800);
        height = parseInt(width / scale);

        //生成canvas
        var $canvas = $('#canvas');
        var ctx = $canvas[0].getContext('2d');
        $canvas.attr({width : width, height : height});
        ctx.drawImage($img, 0, 0, width, height);
        var base64 = $canvas[0].toDataURL('image/jpeg',0.5);
		$("#imgValue").text(base64);
        //发送到服务端
    }
    $img.src = url;

});
</script>
</body>
</html>