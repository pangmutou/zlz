<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well data-original-title">
                        <h2><i class="icon-user"></i> 角色管理</h2>
                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <br>
                    <div style="padding-left: 15px;">
                       <a href="role/add" class="btn  btn-success" style="margin-bottom:10px;" ><i class="icon-plus icon-white" ></i>添加</a>
                    </div>
                    <div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable ">
                            <thead>
                            <tr>
                                <th>角色名称</th>
                                <th>排序</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${roles}">
	                            <tr>
	                                <td>${item.roleName }</td>
	                                <td>${item.orderby }</td>
	                                <td class="center">
	                                    <a class="btn btn-info" href="role/edit?roleId=${item.roleId}"><i class="icon-edit icon-white"></i>编辑</a>
	                                    <a class="btn btn-danger" href="javascript:showModal('删除','您确定删除该角色吗？','delOk(${item.roleId})');">
	                                        <i class="icon-trash icon-white"></i>删除</a>
	                                </td>
	                            </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <!-- content ends -->
        </div>
        <!--/#content.span10-->
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
function delOk(id){
	urlUtils("role/del?roleId="+id);
}
</script>
</html>