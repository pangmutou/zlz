<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <style>
        .ellipsis {
            text-overflow: ellipsis;
            white-space: nowrap;
            /*display: block;*/
            overflow: hidden;
        }

        .table th,
        .table td {
            text-align: center;
            vertical-align: middle;
        }

        .circle_img {
            margin:0 auto;
            text-align: center;
            width: 800px;
        }
        .circle_img ul{width:auto; margin:0 auto; padding:0}
        .circle_img ul li{display:inline-block}
        .circle_img ul li img{
            width: 150px;
            height: 150px;
            margin-left: 20px;
            margin-bottom: 20px;
            border: solid #555555 1px;
            border-radius: 8px;
            /*padding:0 10px*/
        }
    </style>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well data-original-title">
                        <h2><i class="icon-zoom-in"></i>朋友圈详细信息</h2>
                    </div>
                    <div class="box-content">
                        <c:if test="${circle.title != null}">
                            <div class="" style="text-align: center;">
                                <h1>${circle.title}</h1>
                            </div>
                        </c:if>
                        <div style="text-align: center; line-height: 60px;">
                            <div class="fl" style="float: left; margin-left: 250px;">作者：${user.username}</div>
                            <div class="fl" >发布时间：<ry:formatDate date="${circle.createTime}" toFmt="yyyy-MM-dd"></ry:formatDate></div>
                        </div>
                        <c:if test="${circle.keywords != null}">
                            <div class="" style="text-align: center; color: red; line-height: 60px;">
                                    ${circle.keywords}
                            </div>
                        </c:if>
                        <hr/>
                        <c:if test="${circle.content != null}">
                            <div class="" style="text-align: center;">
                                    ${circle.content}
                            </div>
                            <hr />
                        </c:if>
                        <c:if test="${!empty imgUrl}">
                            <div class="circle_img">
                                <ul>
                                    <c:forEach items="${imgUrl}" var="img">
                                        <li><img src="${img}" style=""></li>
                                    </c:forEach>
                                </ul>
                            </div>
                            <hr />
                        </c:if>
                        <c:if test="${!empty circleReportList}">
                            <div class="" style="text-align: center;">
                                <h1>被举报的列表</h1>
                            </div>
                            <c:forEach items="${circleReportList}" var="item" varStatus="vs">
                                <div style="line-height: 60px;">举报的序数:${vs.count}</div>
                                <div style="line-height: 60px;">举报人：${item.username}</div>
                                <div style="line-height: 60px;">举报报告：${item.reportContent}</div>
                                <div style="line-height: 60px; margin-bottom: 40px;">举报时间：<ry:formatDate date="${item.reportTime}" toFmt="yyyy-MM-dd"></ry:formatDate></div>
                            </c:forEach>
                        </c:if>
                        <form class="form-horizontal">
                            <fieldset>
                                <div class="form-actions">
                                    <c:if test="${circle.checkStatus == 0}">
                                        <a class="btn btn-success inline" href="javascript:void(0);" onclick="toUpdate('admin/circle/update?circleId=${circle.circleId}&checkStatus=1');" >审核举报通过</a>
                                        <a class="btn btn-success inline" href="javascript:void(0);" onclick="toUpdate('admin/circle/update?circleId=${circle.circleId}&checkStatus=2');" >审核举报不通过</a>
                                    </c:if>
                                    <button type="button" class="btn btn-primary" onclick="back();">返回</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/row-->
    <!-- content ends -->
</div>
<!--/#content.span10-->
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    //删除产品规格
    function toUpdate(url){
        window.location.href = url;
//        urlUtils(url);
    }
</script>
</html>
