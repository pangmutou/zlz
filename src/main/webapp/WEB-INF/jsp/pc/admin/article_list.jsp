<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <style>
        .ellipsis {
            text-overflow: ellipsis;
            white-space: nowrap;
            /*display: block;*/
            overflow: hidden;
        }
    </style>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>文章列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="${url}" method="GET">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="productName">文章标题</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="productName" name="title" placeholder="请输入文章标题" value="${param.title}">
                                        </div>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <button type="submit" class="btn btn-primary"><i class="icon-search"></i>查询</button>
                                        <a href="article/add" class="btn btn-success">添加</a>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>文章标题</th>
                                        <th>小区</th>
                                        <%--<th>文章内容</th>--%>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.title}</td>
                                            <td>${item.communityName}</td>
                                            <%--<td>${item.content}</td>--%>
                                            <td>
                                                <a class="btn btn-primary" href="article/update?articleId=${item.articleId}">编辑</a>
                                                <a class="btn btn-danger" href="javascript:showModal('删除','您确定删除吗？','del(${item.articleId})')">删除</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                            <input type="hidden" name="catId" value="${param.catId}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function del(id) {
        urlUtils("article/delete?articleId=" + id);
    }
</script>
</html>

