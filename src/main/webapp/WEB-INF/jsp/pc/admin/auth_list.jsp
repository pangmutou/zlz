<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <script type="text/javascript" src="js/dtree/dtree.js"></script>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid ">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
			<div id="content1" class="span10 box">
				<div class="box-header well data-original-title">
						<h2><i class="icon-edit"></i>权限信息 </h2>
					</div>
			<div class="span4" style="margin-top: 4px;">
				<script type="text/javascript">
			 	var d = new dTree('d');
				function buildTree() {
					try{
						<c:forEach var="item" items="${authList}" varStatus="status">
							d.add("${item.authCode}","${item.parentAuthCode}",'<font  color="black">${item.authName}</font>','javascript:updateNode(${item.authId})','javascript:void(0);','_blank');
						</c:forEach>
						document.write(d);
						}catch(e){
							alert(e.message);
						}
					}
					buildTree();
					<c:if test="${not empty authCode}">
					d.openTo('${authCode}', true);
					</c:if>
				</script>
			</div>
        	<div class="box-content span6" id="upageNode"></div>
        </div>
        <!--/#content.span10-->

    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script type="text/javascript">
function ajaxSuc(res){
	document.getElementById("upageNode").innerHTML=res;
}
//修改菜单
function updateNode(authId){
	var myAjax=new MyJqueryAjax("auth/edit?authId="+authId,null,ajaxSuc,"text");
	myAjax.request();
}
//添加菜单
function addNode(parentAuthCode){
	var myAjax=new MyJqueryAjax("auth/add?parentAuthCode="+parentAuthCode,null,ajaxSuc,"text");
	myAjax.request();
}
//保存或修改权限菜单
function saveOrUpdateNode(){
	$("#myform").submit();
}
//清除
function clearAuth(){
	$("#upageNode").html("");
}
function dels(id){
//	alert(id);
	var url = "auth/deleteAuth?authCode="+id;
	window.location.href=basePath+url;
}
</script>
</body>
</html>