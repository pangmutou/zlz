<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>广告管理</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="ad/list" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <%--<label class="control-label">广告名称</label>--%>
                                    <%--<div class="controls">--%>
                                    <%--<div class="span4" style="width: auto;">--%>
                                    <%--<input class="input-medium focused" type="text" name="adName" value="${object.adName}">--%>
                                    <%--</div>--%>
                                    <%--</div>--%>
                                    <%--<div class="span3" style="width: auto;">--%>
                                    <%--<button type="submit" class="btn"><i class="icon-search"></i>查询</button>--%>
                                    <a href="ad/save" class="btn btn-success">新增</a>
                                    <%--</div>--%>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>广告名称</th>
                                        <th class="hide">广告位置</th>
                                        <th>广告图片</th>
                                        <th class="hide">媒介类型</th>
                                        <th class="hide">是否启用</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.adName}</td>
                                            <td class="hide">${item.positionId}</td>
                                            <td><img src="${item.adCode}" style="height: 45px;"/></td>
                                            <td class="hide">${item.mediaType}</td>
                                            <td class="hide">${item.enabled == 1 ? '启用' : '停用'}</td>
                                            <td class="center">
                                                <a class="btn btn-primary" href="javascript:void(0)" onclick="urlUtils('ad/edit?adId=${item.adId}')">编辑</a>
                                                <a class="btn btn-danger" href="javascript:showModal('删除','您确定删除吗？','del(${item.adId})');">删除</a>
                                                <%--<div class="btn-group group" style="float:left;margin-right:4px;">--%>
                                                    <%--<button class="btn dropdown-toggle" data-toggle="dropdown">--%>
                                                        <%--<i class="icon-wrench"></i>操作<span class="caret"></span>--%>
                                                    <%--</button>--%>
                                                    <%--<ul class="dropdown-menu">--%>
                                                        <%--<li>--%>
                                                            <%--<a href="javascript:void(0)" onclick="urlUtils('ad/edit?adId=${item.adId}')">编辑</a>--%>
                                                        <%--</li>--%>
                                                        <%--<li>--%>
                                                            <%--<a href="javascript:showModal('删除','您确定删除吗？','del(${item.adId})');">删除</a>--%>
                                                        <%--</li>--%>
                                                    <%--</ul>--%>
                                                <%--</div>--%>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function del(id) {
        urlUtils("ad/delete?adId=" + id);
    }
</script>
</html>