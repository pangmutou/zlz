<%--
  Created by IntelliJ IDEA.
  User: zhangxiansen
  Date: 2016/4/11
  Time: 16:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp"%>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}支付方式信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label class="control-label">支付方式名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused"  placeholder="请输入商品名称" name="payName" value="${payment.payName}">
                                    <span class="help-inline">支付方式名称不能为空</span>
                                </div>
                            </div>
                            <input type="hidden" name="payId" value="${payment.payId}" />
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-default">提交</button>
                                    <button type="reset" class="btn btn-default">重置</button>
                                    <button type="button" class="btn btn-primary"
                                            onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回
                                    </button>
                                </div>
                            </div>
                            <div id="img">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
</html>
