<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>广告位置</h2>
                    </div>
                    <div class="box-content">
                        <form class="form-horizontal" action="ad/list" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <a href="ad/position/save" class="btn btn-primary">新增</a>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>广告位名称</th>
                                        <th>位置宽度</th>
                                        <th>位置高度</th>
                                        <th>广告位描述</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.positionName}</td>
                                            <td>${item.adWidth}</td>
                                            <td>${item.adHeight}</td>
                                            <td>${item.positionDesc}</td>
                                            <td class="center">
                                                <div class="btn-group group" style="float:left;margin-right:4px;">
                                                    <button class="btn dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-wrench"></i>操作<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="javascript:void(0)" onclick="urlUtils('ad/position/edit?positionId=${item.positionId}')">编辑</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:showModal('删除','您确定删除吗？','del(${item.positionId})');">删除</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }

    function del(id) {
        urlUtils("ad/position/delete?positionId=" + id);
    }
</script>
</html>