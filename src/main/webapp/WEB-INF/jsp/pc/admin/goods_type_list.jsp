<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well">
                        <h2><i class="icon-user"></i>商品类型</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="goods/type/list" method="get">
                            <fieldset>
                                <div class="control-group" >
                                    <%--<label class="control-label" for="productName">商品名称</label>--%>
                                    <%--<div class="controls" >--%>
                                        <%--<div class="span4" style="width: auto;">--%>
                                            <%--<input class="input-medium focused" type="text" id="productName"--%>
                                                   <%--name="goodsName" placeholder="请输入商品名" value="${goods.goodsName}" >--%>
                                        <%--</div>--%>
                                    <%--</div>--%>
                                    <%--${search}--%>
                                    <%--<label class="control-label" for="catName">所属店铺</label>--%>
                                    <%--<div class="span4" style="width: auto;">--%>
                                        <%--<input class="input-medium focused" type="text" id="catName"--%>
                                               <%--name="catName" placeholder="请输入所属店铺名" value="${categlry.catName}" >--%>
                                    <%--</div>--%>
                                    <%--<div class="span3" style="width: auto;">--%>
                                        <%--<button type="submit" class="btn" ><i class="icon-search" ></i>查询</button>--%>
                                        <%--${button}--%>
                                        <%--<button type="button" class="btn"--%>
                                                <%--onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回--%>
                                        <%--</button>--%>
                                        <%--<ry:authorize ifAllGranted="${authMap['EXPORT_EXCEL_AUTH']}">--%>
                                            <%--<a href="category/exportExcel" class="btn" ><i class="icon-share-alt"></i>导出</a>--%>
                                        <%--</ry:authorize>--%>
                                        <a href="goods/type/add" class="btn btn-success">添加</a>
                                    <%--</div>--%>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>商品属性名称</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.typeName}</td>
                                            <td>
                                                <%--<div class="btn-group group" style="float:left;margin-right:4px;" >--%>
                                                    <%--<button class="btn dropdown-toggle" data-toggle="dropdown" ><i class="icon-wrench"></i>操作<span class="caret" ></span></button>--%>
                                                    <%--<ul class="dropdown-menu" >--%>
                                                        <%--<li></li>--%>
                                                        <%--<li></li>--%>
                                                        <%--<li></li>--%>
                                                    <%--</ul>--%>
                                                <%--</div>--%>
                                                    <a class="btn btn-primary" href="attribute/list?typeId=${item.typeId}">属性列表</a>
                                                    <a class="btn btn-primary" href="goods/type/edit?typeId=${item.typeId}">编辑</a>
                                                    <a class="btn btn-danger" href="javascript:showModal('删除','您确定删除吗？','delCategory(${item.catId})');">删除</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage" ></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event,obj,type,page){
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function delCategory(id){
        urlUtils("category/delete?catId="+id);
    }
</script>
</html>
