<%@ page import="java.util.List" %>
<%@ page import="com.ahnimeng.common.util.SpringContextUtil" %>
<%@ page import="com.ahnimeng.system.service.CategoryService" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="com.ahnimeng.system.model.TCategory" %>
<%--
  Created by IntelliJ IDEA.
  User: zhangxiansen
  Date: 2016/4/11
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%
    CategoryService categoryService = SpringContextUtil.getBean("categoryService");
    List list = categoryService.getAll(TCategory.class);
    String categoryList = JSONArray.fromObject(list).toString();
    request.setAttribute("categoryList", categoryList);
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}分类</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label for="productName" class="control-label">分类名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="productName" placeholder="请输入分类名称" name="catName" value="${category.catName}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">上级分类</label>
                                <div class="controls">
                                    <select name="parentId" id="parentId">
                                    </select>
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <%--<div class="control-group">--%>
                                <%--<label class="control-label">上传商品图片</label>--%>
                                <%--<div class="controls">--%>
                                    <%--<div id="img-uploader"></div>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                            <div class="hidden">
                            <div class="control-group">
                                <label for="productName" class="control-label">排序</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="sortOrder" name="sortOrder" placeholder="排序" value="1">
                                    <%--<span class="help-inline">分类名称不能为空</span>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">数量单位</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="measureUnit" placeholder="请输入数量单位" name="measureUnit" value="${category.catName}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">是否显示</label>
                                <div class="controls">
                                    <input type="hidden" class="input-xlarge focused" id="isShow" placeholder="请输入是否显示" name="isShow" value="${category.isShow}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">是否显示在导航栏</label>
                                <div class="controls">
                                    <input type="hidden" class="input-xlarge focused" id="showInNav" placeholder="是否显示在导航栏" name="showInNav" value="${category.showInNav}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">设置为首页推荐</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="style" placeholder="设置为首页推荐" name="style" value="${category.catName}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">筛选属性</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="filterAttr" placeholder="请输入筛选属性" name="filterAttr" value="${category.catName}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">价格区间个数</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="grade" placeholder="请输入分类名称" name="grade" value="0">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">分类的样式表文件</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="templateFile" placeholder="请输分类的样式表文件" name="templateFile" value="${category.catName}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">关键字</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="keywords" placeholder="请输入分类名称" name="keywords" value="${category.catName}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="productName" class="control-label">分类描述</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="catDesc" placeholder="请输入分类名称" name="catDesc" value="${category.catName}">
                                    <span class="help-inline">分类名称不能为空</span>
                                </div>
                            </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <c:choose>
                                        <c:when test="${methodName=='查看'}">
                                        </c:when>
                                        <c:otherwise>
                                            <button type="submit" class="btn btn-primary">提交</button>
                                            <button type="reset" class="btn btn-default">重置</button>
                                        </c:otherwise>
                                    </c:choose>
                                    <button type="button" class="btn"
                                            onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="catId" value="${category.catId}"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script src="js/fine-uploader-master/all.fine-uploader.js"></script>
<link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
<script type="text/template" id="img-template">
    <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="拖拽文件到此即可上传">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector">拖拽文件到此即可上传</span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div>请点击上传</div>
        </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals" style="overflow-x: hidden;">
            <li>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <div class="qq-thumbnail-wrapper">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </div>
                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                    <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                    Retry
                </button>

                <div class="qq-file-info">
                    <%--<div class="qq-file-name">--%>
                    <%--<span class="qq-upload-file-selector qq-upload-file"></span>--%>
                    <%--<span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>--%>
                    <%--</div>--%>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                    </button>
                </div>
                <input type="hidden" name="img">
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-ok-button-selector">是</button>
                <button type="button" class="qq-cancel-button-selector">否</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
<script>
    $(function() {


        var parentId = 0, level = 0, orderArray = [], categoryList = eval('${categoryList}');
        var rootNode = {"catId": 0, "level": 0, "catName": "顶级分类"};
        orderArray.push(rootNode)
        findChildren(rootNode);
        function findChildren(parentNode){
            $(categoryList).each(function(i, json){
                if(json.parentId == parentNode.catId) {
                    json.level = json.parentId == 0 ? 0 : parentNode.level + 1;
                    var indent = "";
                    for(var i = 0; i < json.level; i++) { indent += "&nbsp;&nbsp;&nbsp;&nbsp;"; }
                    json.catName = indent + json.catName;
                    orderArray.push(json);
                    findChildren(json);
                }
            })
        }

        var $categorySelect = $("#parentId");
        $(orderArray).each(function (i, json) {
            var option = document.createElement("option");
            option.value = json.catId, option.innerHTML = json.catName;
            $categorySelect.append(option);
        });

        var categoryParentId = eval(${category.parentId});
        $categorySelect.val(categoryParentId);
    })
</script>
</body>
</html>
