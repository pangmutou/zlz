<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>
                            ${param.type==0?'公共设施报修':''}
                            ${param.type==1?'业主报修':''}
                            ${param.type==2?'投诉':''}
                        </h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="workorder/list" method="get">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label">标题</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium" type="text" name="title" placeholder="标题" value="${workorder.title}">
                                        </div>
                                        <div class="span3" style="width: auto;">
                                            <button type="submit" class="btn btn-primary">查询</button>
                                            <%--<a href="workorder/add" class="btn btn-success"><i class="icon-plus icon-white"></i>创建工单</a>--%>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>工单编号</th>
                                        <th>标题</th>
                                        <%--<th>内容</th>--%>
                                        <th>创建时间</th>
                                        <th>创建人</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.workorderId}</td>
                                            <td>${item.title}</td>
                                            <%--<td>${item.content}</td>--%>
                                            <td><fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate></td>
                                            <td>${item.createUserName}</td>
                                            <td><c:choose>
                                                <c:when test="${item.status == 0}">无效</c:when>
                                                <c:when test="${item.status == 1}">待处理</c:when>
                                                <c:when test="${item.status == 2}">已结束</c:when>
                                            </c:choose></td>
                                            <td class="center">
                                                <c:choose>
                                                    <c:when test="${item.status == 1}"><a class="btn btn-primary" href="workorder/handle?workorderId=${item.workorderId}">处理工单</a></c:when>
                                                    <c:when test="${item.status == 2}"><a class="btn btn-primary" href="workorder/handle?workorderId=${item.workorderId}">查看工单</a></c:when>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                                <input type="hidden" name="type" value="${object.type}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
</script>
</html>