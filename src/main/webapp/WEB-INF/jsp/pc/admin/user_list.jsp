<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<c:if test="${param.isIframe != 1}">
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well data-original-title">
                        <h2><i class="icon-user"></i>用户管理</h2>
                    </div>
                    </c:if>
                    <form action="user/info/list" id="myform" method="post">
                        <div class="box-content">
                            <div style="padding-left: 15px;">
                                <span>登录名：</span><input name="username" value="${param.username}" class="input-large focused" type="text">
                                <span>姓名：</span><input name="realName" value="${param.realName}" class="input inline input-large" type="text">
                                <button type="submit" class="btn btn-primary" style="margin-bottom: 10px;">查询</button>
                                <c:if test="${param.isIframe != 1}">
                                    <ry:authorize ifAnyGranted="A_1040">
                                        <a class="btn btn-success inline" href="user/info/add" style="margin-bottom: 10px;">添加用户</a>
                                    </ry:authorize>
                                </c:if>
                            </div>
                            <table class="table table-striped table-bordered bootstrap-datatable">
                                <thead>
                                <tr>
                                    <th>用户id</th>
                                    <th>登录名</th>
                                    <th>姓名</th>
                                    <th>性别</th>
                                    <th>联系电话</th>
                                    <th>创建时间</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="item" items="${pageList.result}">
                                    <tr>
                                        <td class="center">${item.userId}</td>
                                        <td class="center">${item.username}</td>
                                        <td>${item.realName}</td>
                                        <td><ry:show parentCode="USERSEX" itemCode="${item.sex}"></ry:show></td>
                                        <td>${item.telephone}</td>
                                        <td>
                                            <ry:formatDate date="${item.createTime}" toFmt="yyyy-MM-dd"></ry:formatDate></td>
                                        <td>
                                            <span class="label ${item.status == 1? 'label-success':'label-important'}">
                                                <ry:show parentCode="USERSTATUS" itemCode="${item.status}"></ry:show>
                                            </span>
                                        </td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${param.isIframe != 1}">
                                                    <c:if test="${systemUser.userId != item.userId }">
                                                        <a class="btn btn-info" href="user/info/edit?userId=${item.userId}" ><i class="icon-edit icon-white"></i>修改</a>
                                                        <a class="btn btn-danger" href="javascript:showModal('操作','您确定${item.status == 1 ? '冻结':'解冻'}用户  ${item.username} 吗？','delOk(${item.userId})')">
                                                            <i class="icon-trash icon-white"></i>${item.status == 1 ? '冻结':'解冻'}
                                                        </a>
                                                    </c:if>
                                                </c:when>
                                                <c:when test="${param.isIframe != 1}">
                                                    <a class="btn btn-info" href="javascript:selectUser(${item.userId},'${item.realName}')" onclick="selectUser">选择</a>
                                                </c:when>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <div class="paginator" id="page" currentPage="${pageList.cur}" totalPages="${pageList.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                            <input id="cur" type="hidden" name="cur" value="${pageList.cur}"/>
                            <input type="hidden" name="isIframe" value="${param.isIframe}"/>
                            <input type="hidden" name="communityId" value="${param.communityId}"/>
                        </div>
                    </form>
                    <c:if test="${param.isIframe != 1}">
                </div>
                <!--/span-->
            </div>
            <!-- content ends -->
        </div>
        <!--/#content.span10-->
    </div>
</div>
</c:if>

<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#cur").val(page);
        document.getElementById("myform").submit();
    }

    function selectUser(userId, realName) {
        window.parent.selectUser(userId, realName);
    }
</script>
</body>
</html>