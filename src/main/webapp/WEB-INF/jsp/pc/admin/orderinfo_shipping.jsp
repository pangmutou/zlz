<%--
  Created by IntelliJ IDEA.
  User: zhangxiansen
  Date: 2016/4/10
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <ry:binding parentCode="USERSEX" bingdingName="customersexs"></ry:binding>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp"%>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}配送方式信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" id="form" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label for="tagWords" class="control-label">配送方式名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="tagWords" onblur="searchTagWords();" placeholder="请输入配送方式名称" name="shippingName" value="${shipping.shippingName}">
                                    <span class="help-inline">配送方式名称不能为空</span>
                                </div>
                            </div>
                            <input type="hidden" name="shippingId" value="${shipping.shippingId}"/>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="button" onclick="checkForm();" class="btn btn-default">提交</button>
                                    <button type="reset" class="btn btn-default">重置</button>
                                    <button type="button" class="btn btn-primary" onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script>
    var result = true;
    function searchTagWords(){
        var tagWords=$("#tagWords").val();
        var url = "tag/searchTagName";
        var bool = true;
        $.ajax({
            type:"post",
            url:url,
            dataType:"json",
            data:{tagWords:tagWords},
            success:function(tag){
                if(tag!=null){
                    $("#tagWords").lightHelp(0,1);
                    $("#tagWords").eachSelect(".controls").find(".help-inline").html("该配送方式名称已存在");
                    result = false;
                }else{
                    if($("#tagWords").validate()){
                        $("#tagWords").lightHelp(" ",1);
                        $("#tagWords").eachSelect(".controls").find(".help-inline").html("");
                        result = true;
                    }
                }
            }
        });
        return result;
    }

    function checkForm(){
        var v_result = searchTagWords();
        var v_form = $("#form").validate();
        if(v_result && v_form){
            $("#form").submit();
        }
    }
</script>
</html>