<%--
  Created by IntelliJ IDEA.
  User: zhangxiansen
  Date: 2016/4/21
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.ahnimeng.system.model.TDictionary" %>
<%@ page import="org.hibernate.criterion.Criterion" %>
<%@ page import="org.hibernate.criterion.Restrictions" %>
<%@ page import="org.hibernate.criterion.Order" %>
<%@ page import="com.ahnimeng.common.util.SpringContextUtil" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="com.ahnimeng.system.service.CategoryService" %>
<%@ page import="com.ahnimeng.system.model.TCategory" %>
<%@ page import="com.ahnimeng.system.service.DictionaryService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
    <%--<link href="js/fine-uploader-master/fineuploader-new.css" rel="stylesheet" type="text/css"/>--%>
    <%
        ApplicationContext applicationContext = SpringContextUtil.getApplicationContext();
        CategoryService categoryService = applicationContext.getBean(CategoryService.class);
        DictionaryService dictionaryService = applicationContext.getBean(DictionaryService.class);
        request.setAttribute("thislabel", categoryService.list(new TCategory()));
        request.setAttribute("goodsTaste", dictionaryService.list(TDictionary.class, new Criterion[]{Restrictions.eq("parentCode", "GOODS_TASTE")}, new Order[]{Order.desc("orderby")}, 0, Integer.MAX_VALUE));
    %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>查看商品信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" id="form" class="form-horizontal" enctype="multipart/from-data"  method="post">
                            <div class="control-group">
                                <label  class="control-label">商品名称:</label>
                                <div class="controls">
                                    <span  class="control-label">${goods.goodsName}</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label  class="control-label">商品价格:</label>
                                <div class="controls">
                                    <span  class="control-label">${goods.shopPrice}</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label  class="control-label">市场价格</label>
                                <div class="controls">
                                    <label  class="control-label">${goods.marketPrice}</label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label  class="control-label">商品库存</label>
                                <div class="controls">
                                    <label  class="control-label">${goods.goodsNumber}</label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">选择商品类型</label>
                                <div class="controls">
                                    <label  class="control-label">${category.catName}</label>
                                    <%--<div class="span4" style="width: auto;">--%>
                                        <%--<select data-placeholder="商品类型"   name="catId" id="category" data-rel="chosen">--%>
                                            <%--<option value="">--%>
                                                <%--请选择商品类型--%>
                                            <%--</option>--%>
                                            <%--<c:forEach items="${thislabel}" var="item">--%>
                                                <%--<option value="${item.catId}">${item.catName}</option>--%>
                                            <%--</c:forEach>--%>
                                        <%--</select>--%>
                                    <%--</div>--%>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">类型/口味</label>

                                <div class="controls">
                                    <div class="span3" style="width: auto;">
                                        ${goodsType.typeName}
                                        <%--<select data-placeholder="选择口味" onchange="selectType()" name="typeId" id="select-goods-type" data-rel="chosen">--%>
                                            <%--<option value="">--%>
                                                <%--请选择商品类型--%>
                                            <%--</option>--%>
                                            <%--<c:forEach items="${goodsTypeList}" var="type">--%>
                                                <%--<option value="${type.typeId}">${type.typeName}</option>--%>
                                            <%--</c:forEach>--%>
                                        <%--</select>--%>
                                        <%--<select id="select-goods-type" name="goodsType"--%>
                                        <%--data-rel="chosen" data-placeholder="请选择类型"--%>
                                        <%--onchange="selectType()" valitype="require">--%>
                                        <%--</select>--%>
                                        <%--<span class="help-inline">类型必选</span>--%>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        ${attribute.attrName}
                                        <%--<select id="select-attribute" name="attrId"--%>
                                                <%--data-rel="chosen"--%>
                                                <%--data-placeholder="请选择分类" valitype="require">--%>
                                            <%--<option value="">--%>
                                                <%--请选择分类--%>
                                            <%--</option>--%>
                                        <%--</select>--%>
                                        <%--<span class="help-inline">分类必选</span>--%>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <%--<input type="text" name="attrValue" value="${goodsAttr.attrValue}"  placeholder="请填写口味"/>--%>
                                            ${goodsAttr.attrValue}
                                    </div>
                                </div>
                            </div>
                            <%--<div class="control-group">--%>
                            <%--<label for="goodsTaste" class="control-label">选择口味</label>--%>
                            <%--<div class="controls">--%>
                            <%--<div class="span4" style="width: auto;">--%>
                            <%--<select data-placeholder="选择口味" name="goodsTaste" id="goodsTaste" data-rel="chosen">--%>
                            <%--<option value="">--%>
                            <%--请选择商品口味--%>
                            <%--</option>--%>
                            <%--<c:forEach items="${goodsTaste}" var="item">--%>
                            <%--<option value="${item.itemCode}">${item.itemName}</option>--%>
                            <%--</c:forEach>--%>
                            <%--</select>--%>
                            <%--</div>--%>
                            <%--</div>--%>
                            <%--</div>--%>
                            <div class="control-group">
                                <label class="control-label">上传商品视频</label>
                                <div class="controls">
                                    <div id="video-uploader" style="width: 250px;"><img src="${goods.goodsVideo}"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">上传商品图片</label>
                                <div class="controls">
                                    <div id="img-uploader"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">商品简介</label>
                                <div class="controls row-fluid" style="width: auto;">
                                    <div class="span4" style="width: auto;">
                                        <textarea name="goodsBrief" rows="6" valitype="require" style="resize: none;width: 200%;">${goods.goodsBrief}</textarea>
                                        <span class="help-inline">请输入商品简介</span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">商品详述</label>
                                <div class="controls row-fluid" style="width: auto;">
                                    <div class="span4" style="width: auto;">
                                        <textarea name="goodsDesc" valitype="require" class="cleditor">${goods.goodsDesc}</textarea>
                                        <span class="help-inline">请输入商品详述</span>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="goodsId" value="${goods.goodsId}" <c:if test="${methodName == '添加'}">disabled="disabled"</c:if>/>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="button" onclick="checkForm();" class="btn btn-default">提交</button>
                                    <button type="reset" class="btn btn-default">重置</button>
                                    <button type="button" class="btn btn-primary" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script type="text/template" id="video-template">
    <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="拖拽文件到此即可上传">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector">拖拽文件到此即可上传</span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div>请点击上传视频</div>
        </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals" style="overflow-x: hidden;">
            <li>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <div class="qq-thumbnail-wrapper">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </div>
                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                    <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                    Retry
                </button>

                <div class="qq-file-info">
                    <%--<div class="qq-file-name">--%>
                    <%--<span class="qq-upload-file-selector qq-upload-file"></span>--%>
                    <%--<span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>--%>
                    <%--</div>--%>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                    </button>
                </div>
                <input type="hidden" name="goodsVideo">
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-ok-button-selector">是</button>
                <button type="button" class="qq-cancel-button-selector">否</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
<script type="text/template" id="img-template">
    <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="拖拽文件到此即可上传">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector">拖拽文件到此即可上传</span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div>请点击上传</div>
        </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals" style="overflow-x: hidden;">
            <li>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <div class="qq-thumbnail-wrapper">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </div>
                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                    <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                    Retry
                </button>

                <div class="qq-file-info">
                    <%--<div class="qq-file-name">--%>
                    <%--<span class="qq-upload-file-selector qq-upload-file"></span>--%>
                    <%--<span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>--%>
                    <%--</div>--%>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                    </button>
                </div>
                <input type="hidden" name="imgUrl">
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-ok-button-selector">是</button>
                <button type="button" class="qq-cancel-button-selector">否</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
<script src="js/fine-uploader-master/all.fine-uploader.js"></script>
<script>
    var imgUploader, videoUploader;
    $(function () {
        "use strict";

        videoUploader = new qq.FineUploader({
            element: document.getElementById("video-uploader"),
            template: 'video-template',
            autoUpload: true,
            debug: false,
            uploadButtonText: "Select Files",
            editFilename: {
                //编辑名字
                enable: false
            },
            messages: {
                typeError: "{file} has an invalid extension. Valid extension(s): {extensions}.",
                sizeError: "{file} 太大了, 文件大小不能超过 {sizeLimit}.",
                minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                emptyError: "{file} is empty, please select files again without it.",
                noFilesError: "No files to upload.",
                tooManyItemsError: "最多上传{itemLimit}个视频文件.",
                maxHeightImageError: "Image is too tall.",
                maxWidthImageError: "Image is too wide.",
                minHeightImageError: "Image is not tall enough.",
                minWidthImageError: "Image is not wide enough.",
                retryFailTooManyItems: "Retry failed - you have reached your file limit.",
                onLeave: "The files are being uploaded, if you leave now the upload will be canceled.",
                unsupportedBrowserIos8Safari: "Unrecoverable error - this browser does not permit file uploading of any kind due to serious bugs in iOS8 Safari.  Please use iOS8 Chrome until Apple fixes these issues."
            },
            display: {
                fileSizeOnSubmit: true
            },
            request: {
                endpoint: "file/goods/upload",
                inputName: "file"
            },
            deleteFile: {
                enabled: true,
                endpoint: "#",
                confirmMessage: "确定要删除 {filename} 吗?",
                forceConfirm: false,
                params: {
                    foo: "bar"
                }
            },
            chunking: {
                enabled: false,
                concurrent: {
                    enabled: false
                },
                success: {
                    endpoint: "#"
                }
            },
            resume: {
                enabled: false
            },
            retry: {
                enableAuto: false
            },
            thumbnails: {
                placeholders: {
                    waitingPath: "js/fine-uploader-master/waiting-generic.png",
                    notAvailablePath: "js/fine-uploader-master/not_available-generic.png"
                }
            },
            // scaling: {
            //    sizes: [{name: "small", maxSize: 300}]
            // },
            <c:if test="${url=='goods/update'}">
            session: {
                endpoint: "goods/video/list",
                params: {'goodsId': '${goods.goodsId}'}
            },
            </c:if>
            validation: {
                allowedExtensions: ['mp4', 'wmv'],
                sizeLimit: 100000000,
                itemLimit: 1,
                acceptFiles: ['audio/*', 'video/*']
            },
            callbacks: {
                onError: function (id, fileName, reason) {
                    if (id)
                        alert(fileName + "上传失败");
                    return qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
                },
                onComplete: function (id, name, response) {
                    updateFileInputValue(videoUploader, id, response.obj);
                },
                onSessionRequestComplete: function (data) {
                    $(data).each(function (i, e) {
                        updateFileInputValue(videoUploader, i, e.uuid)
                    })
                }
            }
        });

        imgUploader = new qq.FineUploader({
            element: document.getElementById("img-uploader"),
            template: 'img-template',
            autoUpload: true,
            debug: false,
            uploadButtonText: "Select Files",
            editFilename: {
                //编辑名字
                enable: false
            },
            messages: {
                typeError: "{file} has an invalid extension. Valid extension(s): {extensions}.",
                sizeError: "{file} 太大了, 文件大小不能超过 {sizeLimit}.",
                minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                emptyError: "{file} is empty, please select files again without it.",
                noFilesError: "No files to upload.",
                tooManyItemsError: "最多上传{itemLimit}张图片.",
                maxHeightImageError: "Image is too tall.",
                maxWidthImageError: "Image is too wide.",
                minHeightImageError: "Image is not tall enough.",
                minWidthImageError: "Image is not wide enough.",
                retryFailTooManyItems: "Retry failed - you have reached your file limit.",
                onLeave: "The files are being uploaded, if you leave now the upload will be canceled.",
                unsupportedBrowserIos8Safari: "Unrecoverable error - this browser does not permit file uploading of any kind due to serious bugs in iOS8 Safari.  Please use iOS8 Chrome until Apple fixes these issues."
            },
            display: {
                fileSizeOnSubmit: true
            },
            request: {
                endpoint: "file/goods/upload",
                inputName: "file"
            },
            deleteFile: {
                enabled: true,
                endpoint: "#",
                confirmMessage: "确定要删除 {filename} 吗?",
                forceConfirm: false,
                params: {
                    foo: "bar"
                }
            },
            chunking: {
                enabled: false,
                concurrent: {
                    enabled: false
                },
                success: {
                    endpoint: "#"
                }
            },
            resume: {
                enabled: false
            },
            retry: {
                enableAuto: false
            },
            thumbnails: {
                placeholders: {
                    waitingPath: "js/fine-uploader-master/waiting-generic.png",
                    notAvailablePath: "js/fine-uploader-master/not_available-generic.png"
                }
            },
            // scaling: {
            //    sizes: [{name: "small", maxSize: 300}]
            // },
            <c:if test="${url=='goods/update'}">
            session: {
                endpoint: "goods/gallery/list",
                params: {'goodsId': '${goods.goodsId}'}
            },
            </c:if>
            validation: {
                allowedExtensions: ['jpg', 'png', 'bmp'],
                sizeLimit: 5000000,
                itemLimit: 5,
                acceptFiles: ['image/*']
            },
            callbacks: {
                onError: function (id, fileName, reason) {
                    if (id)
                        alert("上传失败");
                    return qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
                },
                onComplete: function (id, name, response) {
                    updateFileInputValue(imgUploader, id, response.obj);
                },
                onSessionRequestComplete: function (data) {
                    $(data).each(function (i, e) {
                        updateFileInputValue(imgUploader, i, e.uuid)
                    })
                }
            }
        });

    });
//    function formatTemplate(dta, tmpl) {
//        var format = {
//            name: function (x) {
//                return x
//            }
//        };
//        return tmpl.replace(/{(\w+)}/g, function (m1, m2) {
//            if (!m2)
//                return "";
//            return (format && format[m2]) ? format[m2](dta[m2]) : dta[m2];
//        });
//    }
    function updateFileInputValue(fineUploaderObject, id, value) {
        var element = fineUploaderObject._options.element;
        var $li = $(element).find(".qq-file-id-" + id);
        if (fineUploaderObject._options.validation.acceptFiles[0] == 'image/*') {
            $li.find('.qq-thumbnail-selector').attr('src', value)
        }
        $li.find('input[name]').val(value)
    }

    function checkForm(){
        var v_form = $("#form").validate();
        if( v_form ){
            $("#form").submit();
        }
    }

    /*下拉框的默认选定*/
    $("#goodsTaste").find("option[value='${goods.goodsTaste}']").attr("selected", "selected");
    $("#category").find("option[value='${goods.catId}']").attr("selected", "selected");
    <%--$("#select-goods-type").find("option[value='${goodsType.typeId}']").attr("selected", "selected");--%>


    var typeId = '${goodsType.typeId}';
    var attrName = '${attribute.attrName}';
    $(document).ready(function () {
        var $select = $("#select-goods-type");
        ajax("goods/type/list", null, function (result) {
            $select.updateChosen(result, "typeName", "typeId", typeId);
        }, "json");

        if (typeId) {
            selectType();
        }
    });

    function selectType() {
        var attrId = $("#select-attribute").val();
        ajax("goods/attribute/list", { typeId: typeId }, function (result) {
            $("#select-attribute").updateChosen(result, "attrName", "attrId", attrId);
        }, "json");
    }
</script>
</body>
</html>
