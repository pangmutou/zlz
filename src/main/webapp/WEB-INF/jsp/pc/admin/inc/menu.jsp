<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--_menu 作为公共模版分离出去-->
<aside class="Hui-aside">

    <div class="menu_dropdown bk_2">
    <c:forEach var="item" items="${sessionScope.leftUrls}">
        <dl id="menu-article">
            <dt><i class="Hui-iconfont">&#xe616;</i> ${item.authName}<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <c:forEach var="childItem" items="${item.childAuthority}">
                        <li><a href="${childItem.authUrl}" title="${childItem.authName}">${childItem.authName}</a></li>
                    </c:forEach>
                </ul>
            </dd>
        </dl>
    </c:forEach>
    </div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
<!--/_menu 作为公共模版分离出去-->
