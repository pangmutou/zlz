<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/WEB-INF/jsp/inc/base.jsp"%>
</head>
<style>
	@media (min-width: 1200px) {
		.logo{
			width:540px;
			height:160px;
		}
		.logo-mar{
			margin-bottom:100px;
		}
	}
	@media (min-width: 768px) and (max-width: 979px) {
		.logo{
			width:300px;
			height:90px;
		}
		.logo-mar{
			margin-bottom:30px;
		}
	}
	@media (max-width: 767px) {
		.logo{
			width:255px;
			height:75px;
		}
		.logo-mar{
			margin-bottom:5px;
		}
	}
</style>
<body>
<div class="container-fluid">
		<div class="row-fluid">
		
			<div class="row-fluid logo-mar">
				<div class="span12 center login-header">
					<img alt="Charisma Logo" class="logo" src="img/logo.png"/>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<form class="form-horizontal" action="login" method="post">
						<fieldset>
							<div class="input-prepend" data-rel="tooltip" data-original-title="用户名">
								<span class="add-on"><i class="icon-user"></i></span><input autofocus="" class="input-large span10" name="loginName" id="username" type="text" value="admin">
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" data-rel="tooltip" data-original-title="密码">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" value="admin">
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" >
							<label class="remember" for="remember" style="display: inline;margin-right: 20px;" ><div class="checker" id="uniform-remember"><span class="checked"><input type="checkbox" id="remember" style="opacity: 0;"></span></div>记住</label>
							<a href="javascript:void(0);" onclick="$('#myModal').modal();" >找回密码</a>
							</div>
							<div class="clearfix"></div>

							<p class="center span5">
							<button type="submit" class="btn btn-primary">登录</button>
							</p>
						</fieldset>
					</form>
				</div><!--/span-->
			</div><!--/row-->
				</div><!--/fluid-row-->
	</div>
	<div class="modal hide fade" id="myModal" >
		<button type="button" class="close" data-dismiss="modal">×</button>
	    <div class="modal-header">
	        <h3>找回密码</h3>
	    </div>
	    <div class="modal-body" style="height: 100%;margin-left:20%;">
	    	<table width="282" height="153" border="0" cellspacing="0">
			  <tr>
			    <td width="40"><img src="img/telephone.jpg" width="40px;" height="40px;" /></td>
			    <td width="230">
			        <div style="font-size:14px;margin-left:10px;" >
			        	请您拨打：<span style="color:#1c628b;" >400-830-9995</span> 确认您的帐号信息，我们的客服人员帮您找回密码！
			        </div>
			    </td>
			  </tr>
			</table>
	    </div>		
		<div class="modal-footer">
		    <a href="#" class="btn btn-primary" data-dismiss="modal" >确定</a>
		</div>
	</div>
	<%@include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
</html>