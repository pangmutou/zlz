<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/14 0014
  Time: 上午 9:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <style>
        .ellipsis {
            text-overflow: ellipsis;
            white-space: nowrap;
            /*display: block;*/
            overflow: hidden;
        }

        .table th,
        .table td {
            text-align: center;
            vertical-align: middle;
        }
    </style>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-th-list"></i>朋友圈举报列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="${url}" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="title">朋友圈标题:</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="title" name="title" placeholder="请输入文章标题" value="${circle.title}">
                                        </div>
                                    </div>
                                    <label class="control-label" for="username" style="margin-right: 15px;">用户名:</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="username" name="username" placeholder="请输入用户名" value="${user.username}">
                                        </div>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <button type="submit" class="btn btn-primary"><i class="icon-search"></i>查询</button>
                                        <%--<a href="article/add" class="btn btn-success">添加</a>--%>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>文章标题</th>
                                        <th>举报者</th>
                                        <th>举报报告</th>
                                        <th>举报时间</th>
                                        <%--<th>操作</th>--%>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${circleReportList.result}" varStatus="vs">
                                        <tr>
                                            <td>${vs.count}</td>
                                            <td>${item.title}</td>
                                            <td>${item.username}</td>
                                            <td>${item.reportContent}</td>
                                            <td>
                                                <ry:formatDate date="${item.reportTime}" toFmt="yyyy-MM-dd"></ry:formatDate>
                                            </td>
                                            <%--<td>--%>
                                                <%--<span class="label ${item.reportStatus == 0? 'label-success':'label-important'}">--%>
                                                    <%--${item.reportStatus == 1? '被举报':'未被举报'}--%>
                                                <%--</span>--%>
                                            <%--</td>--%>
                                            <%--<td>--%>
                                                <%--<span class="label ${item.circleStatus == 0? 'label-success':'label-important'}">--%>
                                                    <%--${item.circleStatus == 0? '显示':'隐藏'}--%>
                                                <%--</span>--%>
                                            <%--</td>--%>
                                            <%--<td>--%>
                                                <%--<c:if test="${item.checkStatus == 0}">--%>
                                                    <%--<c:if test="${item.reportStatus == 0}">--%>
                                                        <%--<span class="label label-success">未被举报不需处理</span>--%>
                                                    <%--</c:if>--%>
                                                    <%--<c:if test="${item.reportStatus == 1}">--%>
                                                        <%--<span class="label label-important">被举报未处理</span>--%>
                                                    <%--</c:if>--%>
                                                <%--</c:if>--%>
                                                <%--<c:if test="${item.checkStatus == 1}">--%>
                                                    <%--<span class="label label-warning">处理通过</span>--%>
                                                <%--</c:if>--%>
                                                <%--<c:if test="${item.checkStatus == 2}">--%>
                                                    <%--<span class="label label-info">处理未通过</span>--%>
                                                <%--</c:if>--%>
                                            <%--</td>--%>
                                            <%--<td>--%>
                                                <%--<ry:formatDate date="${item.checkTime}" toFmt="yyyy-MM-dd"></ry:formatDate>--%>
                                            <%--</td>--%>
                                            <%--<td>--%>
                                                <%--<a class="btn btn-primary" href="/admin/circle/detail?circleId=${item.circleId}">详情</a>--%>
                                                <%--<a class="btn btn-danger" href="javascript:showModal('删除','您确定删除吗？','del(${item.circleId})')">删除</a>--%>
                                            <%--</td>--%>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                            <input type="hidden" name="circleId" value="${param.circleId}">
                            <input type="hidden" name="circleId" value="${param.circleReportId}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function del(id) {
        window.location.href = "admin/circle/delete?circleId=" + id;
//        urlUtils("admin/circle/delete?circleId=" + id);
    }
</script>
</html>