<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/WEB-INF/jsp/inc/base.jsp" %>
    <%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
    <script type="text/javascript">
        $(function () {
            if('${param.isIframe}') {
                $("body").html($(".box-content"));
                $(".center,.filter").hide();
                $(".iframe").show();
            }else{
                $(".iframe").hide();
            }
        })

        function goPage(event, obj, type, page) {
            if (obj.currentTarget.parentElement.className == "active")
                return;
            $("#currentPageNo").val(page);
            document.forms[0].submit();
        }

        function delOrg(id) {
            urlUtils("org/delOrg?orgId=" + id);
        }
    </script>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>${title}列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="${empty url ? 'org/page' : url}" method="get">
                            <fieldset>
                                <div class="control-group filter">
                                    <label class="control-label" for="orgName">${title}名称</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="orgName" name="orgName" value="${param.orgName}"/>
                                        </div>
                                        <div class="span3" style="width: auto;">
                                            <button type="submit" class="btn btn-primary"><i class="icon-search"></i>查询</button>
                                            <a href="org/add" class="btn btn-success">新增</a>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>${title}名称</th>
                                        <th>地址</th>
                                        <th>创建时间</th>
                                        <th>联系人</th>
                                        <th>联系人座机</th>
                                        <th>联系人电话</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.orgName}</td>
                                            <td>${item.orgAddress}</td>
                                            <td><fmt:formatDate value="${item.createTime}" type="date" dateStyle="long"/></td>
                                            <td>${item.orgLinkmanName}</td>
                                            <td>${item.orgLinkmanTel}</td>
                                            <td>${item.orgLinkmanPhone}</td>
                                            <td class="center">
                                                <div class="btn-group group" style="float:left;margin-right:4px;">
                                                    <button class="btn dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-wrench"></i>操作<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <%--<li><a href="product/page?orgId=${item.org_id}">进入商铺</a></li>--%>
                                                        <%--<li><a href="product/save?orgId=${item.org_id}">添加菜单</a></li>--%>
                                                        <li><a href="org/update?orgId=${item.orgId}">编辑</a></li>
                                                        <li><a href="javascript:showModal('停用','您确定停用吗？','delOrg(${item.orgId})');">停用</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td class="iframe">
                                                <button class="btn" type="button" onclick="window.parent.selectOrg('${item.orgId}','${item.orgName}')">选择</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}"
                                     totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                                <input name="isIframe" value="${param.isIframe}" type="hidden">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
