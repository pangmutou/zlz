<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}广告信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="advert/${object.advertId > 0 ? 'edit' : 'add'}" class="form-horizontal" method="post">
                            <%--<div class="control-group">--%>
                                <%--<label class="control-label">选择菜单类型</label>--%>
                                <%--<div class="controls row-fluid" style="width: auto;">--%>
                                    <%--<!-- 选择产品 -->--%>
                                    <%--<div class="span4" style="width: auto;">--%>
                                        <%--<select name="advertLayout" data-placeholder="选择菜单类型">--%>
                                            <%--<option value="tongcheng">同城</option>--%>
                                        <%--</select>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                            <div class="control-group">
                                <label class="control-label">上传图片</label>
                                <div class="controls">
                                    <div id="img-uploader"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="button" class="btn btn-default" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="advertId" value="${object.advertId}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
<script src="js/fine-uploader-master/all.fine-uploader.js"></script>
<script type="text/template" id="img-template">
    <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="拖拽文件到此即可上传">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector">拖拽文件到此即可上传</span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div>请点击上传</div>
        </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals" style="overflow-x: hidden;">
            <li>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <div class="qq-thumbnail-wrapper">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </div>
                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                    <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                    Retry
                </button>

                <div class="qq-file-info">
                    <%--<div class="qq-file-name">--%>
                    <%--<span class="qq-upload-file-selector qq-upload-file"></span>--%>
                    <%--<span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>--%>
                    <%--</div>--%>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                    </button>
                </div>
                <input type="hidden" name="advertImg">
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-ok-button-selector">是</button>
                <button type="button" class="qq-cancel-button-selector">否</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
<script>
    $(function () {
        var element = $("#img-uploader")[0];
        var imageUploader = uploader(element, "img-template", 1, updateFileInputValue,
                {
                    endpoint: "advert/img",
                    params: {'advertId': '${object.advertId}'}
                }
        );
    });

    function updateFileInputValue(fineUploaderObject, id, value) {
        var element = fineUploaderObject._options.element;
        var $li = $(element).find(".qq-file-id-" + id);
        try {
            if (fineUploaderObject._options.validation.acceptFiles[0] == 'image/*') {
                $li.find('.qq-thumbnail-selector').attr('src', value)
            }
        } catch (e) {
            console.log("获取控件相关参数失败，" + e);
        }
        $li.find('input[name]').val(value)
    }
</script>
</body>
</html>