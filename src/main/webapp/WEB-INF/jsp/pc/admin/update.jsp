<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
<div class="row-fluid">
<%@include file="/WEB-INF/jsp/inc/left.jsp" %>

<noscript>
    <div class="alert alert-block span10">
        <h4 class="alert-heading">Warning!</h4>

        <p>You need to have
            <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript
            </a>
            enabled to use this site.
        </p>
    </div>
</noscript>

<div id="content" class="span10" ><!-- content starts -->

<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">更新缓存</a>
        </li>
    </ul>
</div>
<div class="row-fluid" id="div_content" style="text-align:center;" >
	<a href='<c:url value="/cacheOperate/updateCache" />' class="btn btn-danger" style="width: 150px;height: 40px;font-size:15px;line-height:40px;" >更新缓存</a>
	<span style="color: red;" >${message}</span>
</div>
</div>
<!--/.fluid-container-->
</div>
</div>
<!-- 判断用户默认密码的信息 -->
<input id="telephone" type="hidden" value="${userinfo.userTelephone}" >
<input id="pwd" type="hidden" value="${userinfo.TUser.password}" >
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
</html>