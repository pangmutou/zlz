<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <style>
        [class*="label-"] {
            font-size: 1em;
        }
    </style>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>开店申请管理</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="org/request/page" method="get">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="request_phone">申请人手机</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="request_phone"
                                                   name="requestPhone"/>
                                        </div>
                                        <div id="dvCBs" class="span3" style="width: auto;">
                                            <input type="checkbox" name="handleResult" value="-1" ${tOrgRequest.handleResult == -1 ? 'checked' : ''}>
                                            已处理
                                            <button type="submit" class="btn"><i class="icon-search"></i>查询</button>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>商户名称</th>
                                        <th>手机</th>
                                        <th>申请人</th>
                                        <th>申请时间</th>
                                        <th class="userName">处理人</th>
                                        <th class="handleTime">处理时间</th>
                                        <th class="check">审核</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.orgName}</td>
                                            <td>${item.requestPhone}</td>
                                            <td>${item.requestUserName}</td>
                                            <td>
                                                <fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                                            <td class="userName">${item.userName}</td>
                                            <td class="handleTime">
                                                <fmt:formatDate value="${item.handleTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                                            <td class="center check">
                                                <c:choose>
                                                    <c:when test="${item.handleResult ==-1}"><a class="btn btn-primary" href="org/request/check?requestId=${item.requestId}">审核</a></c:when>
                                                    <c:when test="${item.handleResult == 0}"><span class="label label-important">审核不通过</span></c:when>
                                                    <c:when test="${item.handleResult == 1}"><span class="label label-success">审核通过</span></c:when>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <!-- content ends -->
        </div>
        <!--/#content.span10-->
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function check(id, handleResult) {
        urlUtils("org/request/check?requestId=" + id & "handleResult=" + handleResult);
    }
</script>
</html>
