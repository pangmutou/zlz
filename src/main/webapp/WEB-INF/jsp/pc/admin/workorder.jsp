<%@ page import="com.ahnimeng.system.model.TWorkorder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${title}</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label class="control-label">标题</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" valitype="require" placeholder="请输入工单标题" name="title" value="${object.title}">
                                    <span class="help-inline">工单标题不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">类型</label>
                                <div class="controls">
                                    <div class="span4" style="width: auto;">
                                        <select data-placeholder="工单类型" name="type">
                                            <option value="0">公共设施报修</option>
                                            <option value="1">业主报修</option>
                                            <option value="2">投诉</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">内容</label>
                                <div class="controls">
                                    <%--<script id="container" name="content" type="text/plain">${object.content}</script>--%>
                                    <textarea name="content" cols="30" rows="10" class="cleditor">${object.content}</textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">图片</label>
                                <div class="controls">
                                    <%
                                        TWorkorder workorder = (TWorkorder) request.getAttribute("object");
                                        if(workorder == null) return;
                                        String[] galleryArray = (workorder.getGallery() == null ? "" : workorder.getGallery()).split(",");
                                        for (String gallery : galleryArray) {
                                            out.println(String.format("<img src=\"%s\">", gallery));
                                        }
                                    %>
                                </div>
                            </div>
                            <%--<div class="control-group">--%>
                            <%--<label class="control-label">上传图片</label>--%>
                            <%--<div class="controls">--%>
                            <%--<div id="img-uploader"></div>--%>
                            <%--</div>--%>
                            <%--</div>--%>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="reset" class="btn btn-default">重置</button>
                                    <button type="button" class="btn" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="articleId" value="${object.workorderId}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script>
    categoryInit : $(function () {
        var parentId = 0, orderArray = [], categoryList = eval('${categoryList}');
        var rootNode = {"catId": 0, "level": 0, "catName": "顶级分类"};

        findChildren(rootNode);
        function findChildren(parentNode) {
            $(categoryList).each(function (i, json) {
                if (json.parentId == parentNode.catId) {
                    json.level = json.parentId == 0 ? 0 : parentNode.level + 1;
                    var indent = "";
                    for (var i = 0; i < json.level; i++) {
                        indent += "&nbsp;&nbsp;&nbsp;&nbsp;";
                    }
                    json.catName = indent + json.catName;
                    orderArray.push(json);
                    findChildren(json);
                }
            })
        }

        var $categorySelect = $("#parentId");
        $(orderArray).each(function (i, json) {
            var option = document.createElement("option");
            option.value = json.catId, option.innerHTML = json.catName;
            $categorySelect.append(option);
        });

        var categoryParentId = eval(${goods.catId});
        $categorySelect.val(categoryParentId);
        $categorySelect.trigger("liszt:updated");
    })
</script>
</body>
</html>
