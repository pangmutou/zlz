<%--
  Created by IntelliJ IDEA.
  User: zhangxiansen
  Date: 2016/4/12
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp"%>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}优惠信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="control-group">
                                <label for="productName" class="control-label">商品分类名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" id="productName" placeholder="请输入商品名称" name="goodsName" value="${goods.goodsName}">
                                    <span class="help-inline">商品分类名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <c:choose>
                                        <c:when test="${methodName=='查看'}">
                                        </c:when>
                                        <c:otherwise>
                                            <button type="submit" class="btn btn-default">提交</button>
                                            <button type="reset" class="btn btn-default">重置</button>
                                        </c:otherwise>
                                    </c:choose>
                                    <button type="button" class="btn btn-primary"
                                            onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回
                                    </button>
                                </div>
                            </div>
                            <div id="img">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
</html>
