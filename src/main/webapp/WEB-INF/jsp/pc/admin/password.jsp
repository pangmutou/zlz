<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <ry:binding parentCode="USERSEX" bingdingName="usersexs"></ry:binding>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>

        <div id="content" class="span10">
            <!-- content starts -->

             <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well data-original-title">
						<h2><i class="icon-edit"></i>个人密码修改</h2>
					</div>
					<div class="box-content">
						<form id="userform" class="form-horizontal" action="user/info/updatePwd" method="post">
						    <fieldset>
							  <div id="oldPwdDiv" class="control-group" >
								<label class="control-label">原密码</label>
								<div class="controls">
								 <input class="input-xlarge focused" id="oldInputPwd" name="oldPassword" valitype="require" type="password"  >
								<span class="help-inline">原始密码不正确！</span>
								</div>
							  </div>
							  <div id="firstPass" class="control-group" >
								<label class="control-label">新密码</label>
								<div class="controls">
								 <input class="input-xlarge focused" id="newpasswod" name="password" valitype="password" type="password"  >
								<span class="help-inline">请正确输入密码</span>
								<p style="padding:5px 0 0 0;font-size:12px;color:red;" >密码格式为：数字和字母组合为6-20位，以字母开头</p>
								</div>
							  </div>
							  <div id="secondPass" class="control-group" >
								<label class="control-label">确认新密码</label>
								<div class="controls">
								 <input class="input-xlarge focused" id="renewpass" valitype="equal:newpasswod,require" type="password"  >
								 <span class="help-inline">确认密码不正确！</span>
								</div>
							  </div>
							
							  <div class="form-actions">
							    <button type="button" class="btn btn-primary" onclick="checkSubmit();" >保存</button>
								<button type="button" class="btn btn-primary" onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回</button>
							  </div>
							</fieldset>
							<input type="hidden" name="userinfoId" value="${userinfo.userInfoId}"/>
							<input type="hidden" name="userId" value="${user.userId}"/>
							<input type="hidden" id="oldPwd" value="${user.password}" />
						  </form>
					</div>
				</div><!--/span-->
			</div><!--/row-->
         </div>
         <!--/span-->
     </div>
     <!--/row-->
     <!-- content ends -->
 </div>
        <!--/#content.span10-->
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript" src="js/md5.js" ></script>
<script type="text/javascript">
function checkSubmit(){
	if($("#userform").validate()){
		var pwd = $("#oldPwd").val();
		//获得原密码
		var oldPwd=$("#oldInputPwd").val();
			oldPwd = hex_md5(oldPwd);
		var newpasswod = $("#newpasswod").val();
			newpasswod = hex_md5(newpasswod);
		if(pwd!=oldPwd){
			$("#oldInputPwd").lightHelp(0,1);
			return false;
		}
		if(newpasswod==oldPwd){
			$("#newpasswod").lightHelp(0,1);
			$("#newpasswod").eachSelect(".controls").find(".help-inline").html("您填入的新密码跟原密码一样，无需修改");
			return false;
		}
		if(confirm("密码修改成功后，自动进入登陆页，请使用新密码登陆。是否确认修改？")){
			$("form").submit();
   			return true;
   		}
   		return false;
		
	}
}
</script>
</body>
</html>