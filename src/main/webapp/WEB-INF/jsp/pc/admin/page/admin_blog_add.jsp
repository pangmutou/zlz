<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--_meta 作为公共模版分离出去-->
<%@include file="/WEB-INF/jsp/pc/admin/inc/base.jsp" %>
    <title>胖师兄-添加博客</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

<%--<link href="plugins/fileUpload/css/iconfont.css" rel="stylesheet" type="text/css"/>--%>
<%--<link href="plugins/fileUpload/css/fileUpload.css" rel="stylesheet" type="text/css">--%>
</head>
<body>
<article class="page-container">
    <form action="admin/blog/add" method="post" class="form form-horizontal" id="blogForm" enctype="multipart/form-data">
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">博客标题：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="请输入博客标题" id="blogTitle" name="blogTitle">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>分类栏目：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <span class="select-box">
				<select id="blogCateId" name="blogCateId" class="select">
					<option value="">请选择栏目</option>
					<option value="1">博客</option>
					<option value="2">工作</option>
					<option value="3">生活</option>
					<option value="3">梦想</option>
					<option value="4">简历</option>
				</select>
				</span>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">文章摘要：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <textarea id="blogAbstract" name="blogAbstract" cols="" rows="" class="textarea"  placeholder="说点什么...最少输入10个字符" datatype="*10-100" dragonfly="true" nullmsg="备注不能为空！" onKeyUp="textarealength(this,200)"></textarea>
                <p class="textarea-numberbar"><em class="textarea-length">0</em>/200</p>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">缩略图：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <div id="uploader-demo">
                    <!--用来存放item-->
                    <div id="fileList" class="uploader-list"></div>
                    <div id="filePicker">选择图片</div>
                </div>
                <input name="blogThumbnail" id="blog_thumbnail" type="hidden" value=""/>
            </div>
        </div>
        <link rel="stylesheet" type="text/css" href="plugins/webuploader/webuploader.css">

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-2">文章内容：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <div id="editor" style="width:100%;height:400px;"></div>
                <textarea style="display: none;" name="blogContent" id="blogContent"></textarea>
            </div>
        </div>
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                <button onClick="article_save_submit();" class="btn btn-primary radius" type="button"><i class="Hui-iconfont">&#xe632;</i> 保存并提交审核</button>
                <%--<button onClick="article_save();" class="btn btn-secondary radius" type="button"><i class="Hui-iconfont">&#xe632;</i> 保存草稿</button>--%>
                <button onClick="removeIframe();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
            </div>
        </div>
    </form>
</article>

<%@include file="/WEB-INF/jsp/pc/admin/inc/footer.jsp" %>
<%--wangEditor--%>
<script src="js/wangeditor_qiniu/qiniu.js"></script>
<script src="js/wangeditor_qiniu/plupload/moxie.js"></script>
<script src="js/wangeditor_qiniu/plupload/plupload.dev.js"></script>
<script src="js/wangeditor_qiniu/plupload/plupload.full.min.js"></script>
<script src="js/wangeditor_qiniu/plupload/i18n/zh_CN.js"></script>
<script src="https://unpkg.com/wangeditor/release/wangEditor.min.js"></script>
<%--单张图片上传--%>
<script type="text/javascript" src="plugins/webuploader/webuploader.js"></script>
<script type="text/javascript" src="plugins/webuploader/getting-started-qiniu.js"></script>
<script type="text/javascript">
    var E = window.wangEditor
    var editor = new E('#editor');
    editor.customConfig.onchange = function (html) {
        // 监控变化，同步更新到 textarea
        $("#blogContent").val(html);
    }
    // 允许上传到七牛云存储
    editor.customConfig.qiniu = true
    editor.create()

    // 初始化七牛上传
    uploadInit();

    // 初始化七牛上传的方法
    function uploadInit() {
        // 获取相关 DOM 节点的 ID
        var btnId = editor.imgMenuId;
        var containerId = editor.toolbarElemId;
        var textElemId = editor.textElemId;

        // 创建上传对象
        var uploader = Qiniu.uploader({
            runtimes: 'html5,flash,html4',    //上传模式,依次退化
            browse_button: btnId,       //上传选择的点选按钮，**必需**
            uptoken_url: 'file/qiniu/uptoken',
            domain: 'http://on202kn1z.bkt.clouddn.com/',
            container: containerId,           //上传区域DOM ID，默认是browser_button的父元素，
            max_file_size: '100mb',           //最大文件体积限制
            flash_swf_url: 'js/wangeditor_qiniu/plupload/Moxie.swf',  //引入flash,相对路径
            filters: {
                mime_types: [
                    //只允许上传图片文件 （注意，extensions中，逗号后面不要加空格）
                    { title: "图片文件", extensions: "jpg,gif,png,bmp" }
                ]
            },
            max_retries: 3,                   //上传失败最大重试次数
            dragdrop: true,                   //开启可拖曳上传
            drop_element: textElemId,        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
            chunk_size: '4mb',                //分块上传时，每片的体积
            auto_start: true,                 //选择文件后自动上传，若关闭需要自己绑定事件触发上传
            init: {
                'FilesAdded': function(up, files) {
                    plupload.each(files, function(file) {
                        // 文件添加进队列后,处理相关的事情
                        printLog('on FilesAdded');
                    });
                },
                'BeforeUpload': function(up, file) {
                    // 每个文件上传前,处理相关的事情
                    printLog('on BeforeUpload');
                },
                'UploadProgress': function(up, file) {
                    // 显示进度
                    printLog('进度 ' + file.percent)
                },
                'FileUploaded': function(up, file, info) {
                    // 每个文件上传成功后,处理相关的事情
                    // 其中 info 是文件上传成功后，服务端返回的json，形式如
                    // {
                    //    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
                    //    "key": "gogopher.jpg"
                    //  }
                    printLog(info);
                    // 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html

                    var domain = up.getOption('domain');
                    var res = $.parseJSON(info);
                    var sourceLink = domain + res.key; //获取上传成功后的文件的Url

                    printLog(sourceLink);

                    // 插入图片到editor
                    editor.cmd.do('insertHtml', '<img src="' + sourceLink + '" style="max-width:100%;"/>')
                },
                'Error': function(up, err, errTip) {
                    //上传出错时,处理相关的事情
                    printLog('on Error');
                },
                'UploadComplete': function() {
                    //队列文件处理完毕后,处理相关的事情
                    printLog('on UploadComplete');
                }
                // Key 函数如果有需要自行配置，无特殊需要请注释
                //,
                // 'Key': function(up, file) {
                //     // 若想在前端对每个文件的key进行个性化处理，可以配置该函数
                //     // 该配置必须要在 unique_names: false , save_key: false 时才生效
                //     var key = "";
                //     // do something with key here
                //     return key
                // }
            }
            // domain 为七牛空间（bucket)对应的域名，选择某个空间后，可通过"空间设置->基本设置->域名设置"查看获取
            // uploader 为一个plupload对象，继承了所有plupload的方法，参考http://plupload.com/docs
        });
    }

    // 封装 console.log 函数
    function printLog(title, info) {
        window.console && console.log(title, info);
    }
    
    function article_save_submit() {
        var isOk = false;
        if(!$("#blogTitle").val()){
            isOk = false;
            alert("标题不允许为空");
            return false;
        }else {
            isOk = true;
        }
        if(!$("#blogCateId").val()){
            isOk = false;
            alert("栏目不允许为空");
            return false;

        }else {
            isOk = true;
        }
        if(!$("#blogAbstract").val()){
            isOk = false;
            alert("摘要不允许为空");
            return false;

        }else {
            isOk = true;
        }
        if(!$("#blogContent").val()){
            isOk = false;
            alert("正文不允许为空");
            return false;

        }else {
            isOk = true;
        }
        if(isOk){
            $("#blogForm").submit();
        }else {
            return false;
        }
    }
</script>
</body>
</html>
