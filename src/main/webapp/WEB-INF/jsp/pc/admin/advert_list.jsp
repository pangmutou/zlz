<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>广告管理</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="advert/page" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="productName">广告名称</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="productName"
                                                   name="productName" placeholder="请输入菜单名"
                                                   value="${product.productName}">
                                        </div>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <button type="submit" class="btn"><i class="icon-search"></i>查询</button>
                                        <a href="advert/add" class="btn btn-success">新增</a>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>图片</th>
                                        <th>创建时间</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.advertId}</td>
                                            <td><img src="${item.advertImg}" style="height: 45px;"/></td>
                                            <td>${item.createTime}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="urlUtils('advert/edit?advertId=${item.advertId}')" class="btn btn-primary">编辑</a>
                                                <a href="javascript:showModal('删除','您确定删除吗？','del(${item.advertId})')" class="btn btn-danger">删除</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                                <input name="advertLayout" type="hidden" value="${object.advertLayout}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function del(id) {
        urlUtils("advert/delete?advertId=" + id);
    }
</script>
</html>