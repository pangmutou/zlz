<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>${methodName}</h2>
                    </div>
                    <div class="box-content">
                        <div class="tab-content">
                            <div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <td>申请人</td>
                                        <td>店铺名</td>
                                        <td>申请时间</td>
                                        <td>申请状态</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${myRequestList.result}">
                                    <tr>
                                        <td>${item.request_user_name}</td>
                                        <td>${item.org_name}</td>
                                        <td>${item.create_time}</td>
                                        <td><c:if test="${item.handle_result==1}">审核通过</c:if>
                                            <c:if test="${item.handle_result==-1}">未审核</c:if>
                                            <c:if test="${item.handle_result==0}">审核未通过</c:if>
                                        </td>
                                    </tr>
                                    </tbody>


                                    </c:forEach>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <!--/span-->

            </div>
            <!--/row-->

            <!-- content ends -->
        </div>
        <!--/#content.span10-->

    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goHandlePage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#handleCurrentPageNo").val(page);
        document.forms[0].submit();
    }
    function goUnHandlePage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#unHandleCurrentPageNo").val(page);
        document.forms[0].submit();
    }
    function approve(id) {
        urlUtils("org/request/approve?requestId=" + id);
    }
    function reject(id) {
        urlUtils("org/request/reject?requestId=" + id);
    }
    function unHandle() {
        $("#append1").show();
        $("#append2").hide();
    }
    function handle() {
        $("#append2").show();
        $("#append1").hide();
    }
    var dv = document.getElementById('dvCBs'), cbs = dv.getElementsByTagName('input');
    dv.onclick = function (e) {
        e = e || window.event;
        var o = e.target || e.srcElement;
        if (o.type == 'radio') {
            var vs = '';
            for (var i = 0; i < cbs.length; i++)
                if (cbs[i].checked) vs += ',' + cbs[i].value;
            document.cookie = 'vs=' + vs.substring(1);//存储选中的checkbook的值
        }
    }
    var m = /(^| |;)vs=([^;]+)/.exec(document.cookie);
    if (m) {//cookie中有值，初始化勾选状态
        var arr = m[2].split(',');
        for (var j = 0; j < arr.length; j++)
            for (var i = 0; i < cbs.length; i++)
                if (cbs[i].value == arr[j]) {
                    cbs[i].checked = true;
                    if (cbs[i].value == "handle") {
                        handle();
                    } else {
                        unHandle();
                    }
                    break;
                }
    }
</script>
</html>

