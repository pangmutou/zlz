<%@ page import="com.ahnimeng.system.model.TWorkorder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${title}</h2>
                    </div>
                    <div class="box-content">
                        <form action="workorder/handle" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label class="control-label">标题</label>
                                <div class="controls">
                                    <input readonly type="text" class="input-xlarge focused" valitype="require" placeholder="请输入工单标题" name="title" value="${object.title}">
                                    <span class="help-inline">工单标题不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">类型</label>
                                <div class="controls">
                                    <div class="span4" style="width: auto;">
                                        <select disabled data-placeholder="工单类型" id="type-select" name="type">
                                            <option value="0">公共设施报修</option>
                                            <option value="1">业主报修</option>
                                            <option value="2">投诉</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">内容</label>
                                <div class="controls">
                                    <textarea readonly name="content" cols="30" rows="5">${object.content}</textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">图片</label>
                                <div class="controls">
                                    <%
                                        TWorkorder workorder = (TWorkorder) request.getAttribute("object");
                                        String[] galleryArray = (workorder.getGallery() == null ? "" : workorder.getGallery()).split(",");
                                        for (String gallery : galleryArray) {
                                            out.println(String.format("<img src=\"%s\">", gallery));
                                        }
                                    %>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">处理结果</label>
                                <div class="controls">
                                    <textarea name="handleResult" cols="30" rows="5" ${object.status!=1?'disabled':''}>${object.handleResult}</textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <c:if test="${object.status == 1}">
                                        <button type="submit" class="btn btn-primary">提交</button>
                                        <button type="reset" class="btn btn-default">重置</button>
                                    </c:if>
                                    <button type="button" class="btn" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="workorderId" value="${object.workorderId}">
                            <input type="hidden" name="type" value="${object.type}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script>
    $(function () {
        $("#type-select").val(${object.type})
    })
</script>

</body>
</html>
