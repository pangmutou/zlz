<%@ page import="com.ahnimeng.common.util.SpringContextUtil" %>
<%@ page import="com.ahnimeng.system.service.AreaService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <%
        AreaService areaService = SpringContextUtil.getBean(AreaService.class);
        request.setAttribute("areaList", areaService.page(null, null));
    %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}机构信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="org/${method}" method="post" class="form-horizontal"
                              onsubmit="return $(this).validate()">
                            <div class="control-group">
                                <label for="orgName" class="control-label">名称</label>
                                <div class="controls">
                                    <input name="orgName" value="${org.orgName}" type="text" class="form-control"
                                           id="orgName" valitype="require">
                                    <span class="help-inline">请输入名称</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="orgLinkmanName" class="control-label">联系人</label>
                                <div class="controls">
                                    <input name="orgLinkmanName" value="${org.orgLinkmanName}" type="text"
                                           class="form-control" id="orgLinkmanName" valitype="require">
                                    <span class="help-inline">请输入联系人名称</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="orgLinkmanTel" class="control-label">电话</label>
                                <div class="controls">
                                    <input name="orgLinkmanTel" value="${org.orgLinkmanTel}" type="text"
                                           class="form-control" id="orgLinkmanTel">
                                    <span class="help-inline">请输入电话</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="orgLinkmanPhone" class="control-label">手机</label>
                                <div class="controls">
                                    <input name="orgLinkmanPhone" value="${org.orgLinkmanPhone}" type="text"
                                           class="form-control" id="orgLinkmanPhone" valitype="require">
                                    <span class="help-inline">请输入手机</span>
                                </div>
                            </div>
                            <%--<div class="control-group">--%>
                            <%--<label for="areaCode" class="control-label">地区</label>--%>
                            <%--<div class="controls">--%>
                            <%--<select name="areaCode" id="areaCode" data-rel="chosen">--%>
                            <%--<c:forEach items="${areaList}" var="item">--%>
                            <%--<option value="${item.areaCode}">${item.areaName}</option>--%>
                            <%--</c:forEach>--%>
                            <%--</select>--%>
                            <%--<span class="help-inline">请选择地区</span>--%>
                            <%--</div>--%>
                            <%--</div>--%>
                            <div class="control-group">
                                <label class="control-label">所在小区</label>
                                <div class="controls">
                                    <div class="span4" style="width: auto;">
                                        <select data-placeholder="选择所在小区" class="userHandle"
                                                name="communityId" data-rel="chosen"
                                                valitype="require" ${method == 'check' ? 'readonly' : ''}>
                                            <c:forEach items="${communityList}" var="item">
                                                <option value="${item.communityId}">${item.communityName}</option>
                                            </c:forEach>
                                        </select>
                                        <span class="help-inline">必选</span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="orgAddress" class="control-label">地址</label>
                                <div class="controls">
                                    <input name="orgAddress" value="${org.orgAddress}" type="text" class="form-control"
                                           id="orgAddress" valitype="require">
                                    <span class="help-inline">请输入地址</span>
                                </div>
                            </div>
                            <div class="control-group hide">
                                <label class="control-label">经度</label>
                                <div class="controls">
                                    <input name="longitude" value="${org.longitude}" type="text" class="input-xlarge"
                                           id="longitude" valitype="require" readonly>
                                    <a class="btn btn-small btn-success" data-toggle="modal"
                                       data-target="#myModal">地图选点</a>
                                    <span class="help-inline">请选择经度！</span>
                                </div>
                            </div>
                            <div class="control-group hide">
                                <label class="control-label">纬度</label>
                                <div class="controls">
                                    <input name="latitude" value="${org.latitude}" type="text" class="input-xlarge"
                                           id="latitude" valitype="require" readonly>
                                    <span class="help-inline">请选择纬度！</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <c:if test="${not empty method}">
                                        <button type="submit" class="btn btn-primary">提交</button>
                                        <button type="reset" class="btn btn-default">重置</button>
                                    </c:if>
                                    <button type="button" class="btn" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input name="orgId" value="${org.orgId}" type="hidden"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal hide fade" id="myModal" style="width: 80%;margin-left: -40%;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>选择地址信息</h3>
    </div>
    <div class="modal-body">
        <iframe id="map_iframe" src="map/pick"
                style="border: 0; min-height: 390px; height: 100%; width: 100%; display: block;"></iframe>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary" onclick="choseRepairStation()">确定</a>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script>
    $(function () {
        $("#areaCode option[value='${org.areaCode}']").attr("selected", "selected");
        $("#areaCode").val('${org.areaCode}');
        $('#areaCode').trigger("liszt:updated");
    })

    function choseRepairStation() {
        var iframeDocument = window.frames['map_iframe'].document || window.frames['map_iframe'].contentDocument;
        var longitude = $(iframeDocument).find("#longitude").val();
        var latitude = $(iframeDocument).find("#latitude").val();
        $("#longitude").val(longitude);
        $("#latitude").val(latitude);

        $("#myModal").modal('hide');
    }
</script>
</body>
</html>