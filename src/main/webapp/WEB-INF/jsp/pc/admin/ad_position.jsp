<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}广告信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="ad/position/${method}" class="form-horizontal" method="post" onsubmit="return $(this).validate()">
                            <div class="control-group">
                                <label class="control-label">广告名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="positionName" value="${object.positionName}" valitype="require">
                                    <span class="help-inline">广告名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">广告宽度</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="adWidth" value="${object.adWidth}" valitype="require">
                                    <span class="help-inline">广告宽度不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">广告高度</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="adHeight" value="${object.adHeight}" valitype="require">
                                    <span class="help-inline">广告高度不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">广告描述</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="positionDesc" value="${object.positionDesc}" valitype="require">
                                    <span class="help-inline">广告描述不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">广告模板</label>
                                <div class="controls">
                                    <textarea name="positionStyle" cols="30" rows="10" name="positionStyle" valitype="require">${object.positionStyle}</textarea>
                                    <span class="help-inline">广告模板不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="button" class="btn btn-default" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="positionId" value="${object.positionId}"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script>
    var img = document.getElementById('imgSource');
    img.onload = function () {
        //生成比例
        var width = img.width,
                height = img.height,
                scale = width / height;
        if (scale > 1) {
            width = parseInt(500);
            height = parseInt(width / scale);
        } else {
            height = parseInt(500);
            width = parseInt(height * scale);
        }

        var $canvas = $('canvas');
        var ctx = $canvas[0].getContext('2d');
        $canvas.attr({width: width, height: height});
        ctx.drawImage(img, 0, 0, width, height);
        var base64 = $canvas[0].toDataURL('image/jpeg', 0.5);
        $("#imgValue").text(base64);
    }
    $('#fileupload').change(function () {
        var $file = $(this);
        var fileObj = $file[0];
        var windowURL = window.URL || window.webkitURL;
        var dataURL;
        var $img = $(img);

        if (fileObj && fileObj.files && fileObj.files[0]) {
            dataURL = windowURL.createObjectURL(fileObj.files[0]);
            $img.attr('src', dataURL);
        } else {
            dataURL = $file.val();
            var imgObj = document.getElementById("preview");
            // 两个坑:
            // 1、在设置filter属性时，元素必须已经存在在DOM树中，动态创建的Node，也需要在设置属性前加入到DOM中，先设置属性在加入，无效；
            // 2、src属性需要像下面的方式添加，上面的两种方式添加，无效；
            imgObj.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
            imgObj.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = dataURL;
        }
    })
</script>
</body>
</html>