<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid sortable">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i> 操作日志</h2>

                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <br>

                    <div class="box-content">
                        <form id="myform" class="form-horizontal" action="operationLog/list" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="operationUserName">操作人</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="operationUserName"
                                                   name="operationUserName" placeholder="请输入操作人名称" value="${operationlog.operationUserName}">
                                        </div>
                                    </div>

                                    <div class="controls">
                                        <label class="control-label" for="operationUserName">操作时间</label>
                                        <div class="span4" style="width: auto;">
                                            <input class="input-small" type="text" id="createdate" name="createdate" placeholder="开始时间" value="<ry:formatDate date="${param.createTime}" toFmt="yyyy-MM-dd"></ry:formatDate>">
                                            ~
                                            <input class="input-small" type="text" id="createdateEnd" name="createdateEnd" placeholder="结束时间" value="<ry:formatDate date="${param.createTimeEnd}" toFmt="yyyy-MM-dd"></ry:formatDate>">
                                        </div>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <button onclick="checkForm('operationLog/list')" class="btn">
                                            <i class="icon-search"></i>查询
                                        </button>
                                        <ry:authorize ifAllGranted="${authMap['EXPORT_EXCEL_AUTH']}">
                                            <button onclick="checkForm('operationLog/exportExcel')" class="btn">
                                                <i class="icon-share-alt"></i>导出
                                            </button>
                                        </ry:authorize>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>操作名</th>
                                        <th>操作内容</th>
                                        <th>操作人</th>
                                        <th>操作类型</th>
                                        <th>时间</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${pageList.result}" varStatus="row">
                                        <tr>
                                            <td>${(pageList.cur-1)*page.pageSize+row.count}</td>
                                            <td class="center">${item.operationName}</td>
                                            <td>${item.operationContent}</td>
                                            <td>${item.operationUserName}</td>
                                            <td>
                                                <ry:show parentCode="OPERA_TYPE" itemCode="${item.operationType}"></ry:show></td>
                                            <td><ry:formatDate date="${item.createTime}"></ry:formatDate></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${pageList.cur}" totalPages="${pageList.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${pageList.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <!-- content ends -->
        </div>
        <!--/#content.span10-->

    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function checkForm(url) {
        $("#myform").attr("action", url).submit();
    }
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    $(function () {
        $("#createdate").datepicker({
            onClose: function (selectedDate) {
                $("#createdateEnd").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#createdateEnd").datepicker({
            onClose: function (selectedDate) {
                $("#createdate").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>
</html>