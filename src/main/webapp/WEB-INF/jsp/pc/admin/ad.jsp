<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}广告信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="ad/${method}" class="form-horizontal" method="post" onsubmit="return $(this).validate()">
                            <div class="control-group">
                                <label class="control-label">广告名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="adName" value="${object.adName}" valitype="require">
                                    <span class="help-inline">广告名称不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">媒介类型</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="mediaType" value="${object.mediaType}" valitype="require">
                                    <span class="help-inline">媒介类型不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">广告位置</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="positionId" value="${object.positionId}" valitype="require">
                                    <span class="help-inline">广告位置不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">广告链接</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="adLink" value="${object.adLink}" valitype="require">
                                    <span class="help-inline">广告链接不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">上传商品图片</label>
                                <div class="controls">
                                    <div id="img-uploader" style="width: 250px;"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">广告联系人</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="linkMan" value="${object.linkMan}">
                                    <span class="help-inline">广告联系人不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">联系人Email</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="linkEmail" value="${object.linkEmail}">
                                    <span class="help-inline">联系人Email不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">联系电话</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" name="linkPhone" value="${object.linkPhone}">
                                    <span class="help-inline">联系电话不能为空</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="button" class="btn btn-default" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="adId" value="${object.adId}"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script type="text/template" id="img-template">
    <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="拖拽文件到此即可上传">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector">拖拽文件到此即可上传</span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div>请点击上传</div>
        </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals" style="overflow-x: hidden;">
            <li>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <div class="qq-thumbnail-wrapper">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </div>
                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                    <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                    Retry
                </button>

                <div class="qq-file-info">
                    <%--<div class="qq-file-name">--%>
                    <%--<span class="qq-upload-file-selector qq-upload-file"></span>--%>
                    <%--<span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>--%>
                    <%--</div>--%>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                    </button>
                </div>
                <input type="hidden" name="adCode">
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-ok-button-selector">是</button>
                <button type="button" class="qq-cancel-button-selector">否</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
<script src="js/fine-uploader-master/all.fine-uploader.js"></script>
<script>
    var imgUploader;
    $(function () {
        "use strict";

        imgUploader = new qq.FineUploader({
            element: document.getElementById("img-uploader"),
            template: 'img-template',
            autoUpload: true,
            debug: false,
            uploadButtonText: "Select Files",
            editFilename: {
                enable: false
            },
            messages: {
                typeError: "{file} has an invalid extension. Valid extension(s): {extensions}.",
                sizeError: "{file} 太大了, 文件大小不能超过 {sizeLimit}.",
                minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                emptyError: "{file} is empty, please select files again without it.",
                noFilesError: "No files to upload.",
                tooManyItemsError: "最多上传{itemLimit}张图片.",
                maxHeightImageError: "Image is too tall.",
                maxWidthImageError: "Image is too wide.",
                minHeightImageError: "Image is not tall enough.",
                minWidthImageError: "Image is not wide enough.",
                retryFailTooManyItems: "Retry failed - you have reached your file limit.",
                onLeave: "The files are being uploaded, if you leave now the upload will be canceled.",
                unsupportedBrowserIos8Safari: "Unrecoverable error - this browser does not permit file uploading of any kind due to serious bugs in iOS8 Safari.  Please use iOS8 Chrome until Apple fixes these issues."
            },
            display: {
                fileSizeOnSubmit: true
            },
            request: {
                endpoint: "file/upload",
                inputName: "file"
            },
            deleteFile: {
                enabled: true,
                endpoint: "#",
                confirmMessage: "确定要删除 {filename} 吗?",
                forceConfirm: false,
                params: {
                    foo: "bar"
                }
            },
            chunking: {
                enabled: false,
                concurrent: {
                    enabled: false
                },
                success: {
                    endpoint: "#"
                }
            },
            resume: {
                enabled: false
            },
            retry: {
                enableAuto: false
            },
            thumbnails: {
                placeholders: {
                    waitingPath: "js/fine-uploader-master/waiting-generic.png",
                    notAvailablePath: "js/fine-uploader-master/not_available-generic.png"
                }
            },
            <c:if test="${method == 'edit'}">
            session: {
                endpoint: "ad/gallery/list",
                params: {'adId': '${object.adId}'}
            },
            </c:if>
            validation: {
                allowedExtensions: ['jpg', 'png', 'bmp'],
                sizeLimit: 5000000,
                itemLimit: 1,
                acceptFiles: ['image/*']
            },
            callbacks: {
                onError: function (id, fileName, reason) {
                    if (id)
                        alert("上传失败");
                    return qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
                },
                onComplete: function (id, name, data) {
                    updateFileInputValue(imgUploader, id, data.obj);
                },
                onSessionRequestComplete: function (data) {
                    $(data).each(function (i, e) {
                        updateFileInputValue(imgUploader, i, e.name)
                    })
                }
            }
        });

    });
    function formatTemplate(dta, tmpl) {
        var format = {
            name: function (x) {
                return x
            }
        };
        return tmpl.replace(/{(\w+)}/g, function (m1, m2) {
            if (!m2)
                return "";
            return (format && format[m2]) ? format[m2](dta[m2]) : dta[m2];
        });
    }
    function updateFileInputValue(fineUploaderObject, id, value) {
        var element = fineUploaderObject._options.element;
        var $li = $(element).find(".qq-file-id-" + id);
        if (fineUploaderObject._options.validation.acceptFiles[0] == 'image/*') {
            $li.find('.qq-thumbnail-selector').attr('src', value)
        }
        $li.find('input[name]').val(value)
    }

</script>
</body>
</html>