<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <style>
        .orderTable th {
            text-align: center;
            background-color: #d9edf7;
        }

        .orderTable .textRightAlign {
            text-align: right;
        }

        .td-title {
            width: 5em;
        }
    </style>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>发货详情</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" class="form-horizontal" method="get">
                            <table class="table table-bordered orderTable">
                                <tr class="success">
                                    <th colspan="4">基本信息</th>
                                </tr>
                                <tr>
                                    <td>订单号：</td>
                                    <td id="orderSn">${order.orderSn}</td>
                                    <td>订单状态：</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${order.orderStatus == 0}">待付款</c:when>
                                            <c:when test="${order.orderStatus == 1}">待发货</c:when>
                                            <c:when test="${order.orderStatus == 2}">待收货</c:when>
                                            <c:when test="${order.orderStatus == 10}">交易成功</c:when>
                                            <c:when test="${order.orderStatus == 20}">退货</c:when>
                                            <c:when test="${order.orderStatus == 31}">退款成功</c:when>
                                            <c:otherwise>未知的订单状态</c:otherwise>
                                        </c:choose>
                                        <%--,--%>
                                        <%--<c:choose>--%>
                                        <%--<c:when test="${order.payStatus == 0}">未付款</c:when>--%>
                                        <%--<c:when test="${order.payStatus == 1}">付款中</c:when>--%>
                                        <%--<c:when test="${order.payStatus == 2}">已付款</c:when>--%>
                                        <%--<c:otherwise>未知的付款状态</c:otherwise>--%>
                                        <%--</c:choose>,--%>
                                        <%--<c:choose>--%>
                                        <%--<c:when test="${order.shippingStatus == 0}">未发货</c:when>--%>
                                        <%--<c:when test="${order.shippingStatus == 1}">已发货</c:when>--%>
                                        <%--<c:when test="${order.shippingStatus == 2}">配送中</c:when>--%>
                                        <%--<c:when test="${order.shippingStatus == 3}">已收货</c:when>--%>
                                        <%--<c:when test="${order.shippingStatus == 4}">退货</c:when>--%>
                                        <%--<c:otherwise>未知的付款状态</c:otherwise>--%>
                                        <%--</c:choose>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>订单标题：</td>
                                    <td>${order.orderSubject}</td>
                                    <td>下单时间：</td>
                                    <td><ry:formatDate date="${order.addTime}"/></td>
                                </tr>
                                <tr>
                                    <td>
                                        <%--支付方式：--%>
                                    </td>
                                    <td>
                                        <%--${order.payName}--%>
                                    </td>
                                    <td>付款时间：</td>
                                    <td><ry:formatDate date="${order.payTime}"/></td>
                                </tr>
                                <tr>
                                    <td>
                                        发货信息：
                                    </td>
                                    <td>
                                        ${order.shippingName}
                                        <%--- ${order.shippingId}--%>
                                        <c:if test="${order.orderStatus==1}">
                                            <a data-toggle="modal" data-target="#myModal">编辑</a>
                                        </c:if>
                                    </td>
                                    <td>发货时间：</td>
                                    <td><ry:formatDate date="${order.shippingTime}"/></td>
                                </tr>
                            </table>
                            <table class="table table-bordered orderTable">
                                <tr>
                                    <th colspan="4">买家信息</th>
                                </tr>
                                <tr>
                                    <td class="td-title">收货人：</td>
                                    <td colspan="3">${order.consignee}</td>
                                    <%--<td style="width: 5em;">邮编：</td>--%>
                                    <%--<td>${order.zipcode}</td>--%>
                                </tr>
                                <tr>
                                    <td>地址：</td>
                                    <td colspan="3">
                                        ${order.address}
                                    </td>
                                </tr>
                                <tr>
                                    <td>手机：</td>
                                    <td colspan="3">${order.mobile}</td>
                                    <%--<td>电话：</td>--%>
                                    <%--<td>${order.tel}</td>--%>
                                </tr>
                            </table>
                            <table class="table table-bordered orderTable">
                                <tr>
                                    <th colspan="4">卖家信息</th>
                                </tr>
                                <tr>
                                    <td class="td-title">卖家名称：</td>
                                    <td>${order.sellerOrgName}</td>
                                    <td class="td-title">电话：</td>
                                    <td>${order.sellerOrgPhone}</td>
                                    <%--<td>卖家id：</td>--%>
                                    <%--<td>${order.sellerUserId}</td>--%>
                                </tr>
                                <%--<tr>--%>
                                <%--<td>地址：</td>--%>
                                <%--<td colspan="3">{地址}</td>--%>
                                <%--</tr>--%>
                                <%--<tr>--%>
                                <%--<td>手机：</td>--%>
                                <%--<td>{手机}</td>--%>
                                <%--<td>电话：</td>--%>
                                <%--<td>{电话}</td>--%>
                                <%--</tr>--%>
                            </table>
                            <table class="table table-bordered orderTable">
                                <tr>
                                    <th colspan="6">商品信息</th>
                                </tr>
                                <tr>
                                    <th>商品名称</th>
                                    <th>货号</th>
                                    <th>属性</th>
                                    <th>价格</th>
                                    <th>数量</th>
                                    <th>小计</th>
                                </tr>
                                <c:forEach items="${orderGoods}" var="item">
                                    <tr>
                                        <td>${item.goodsName}</td>
                                        <td>${item.goodsSn}</td>
                                        <td>${item.goodsAttr}</td>
                                        <td class="textRightAlign">${item.goodsPrice}</td>
                                        <td class="textRightAlign">${item.goodsNumber}</td>
                                        <td class="textRightAlign xiaoji">${item.goodsPrice*item.goodsNumber}</td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="textRightAlign">合计</td>
                                    <td class="textRightAlign"><span class="heji"></span>元</td>
                                </tr>
                            </table>
                            <table class="table table-bordered orderTable">
                                <tr>
                                    <th colspan="2">费用信息</th>
                                </tr>
                                <tr>
                                    <td>
                                        商品总金额：￥${order.goodsAmount}元
                                        <%--+ 调整费用：￥0--%>
                                    </td>
                                    <td class="textRightAlign">￥${order.goodsAmount}元</td>
                                </tr>
                                <%--<tr>--%>
                                <%--<td>优惠券：￥0元</td>--%>
                                <%--<td class="textRightAlign"> - ￥0元</td>--%>
                                <%--</tr>--%>
                                <tr>
                                    <td></td>
                                    <td class="textRightAlign">应付金额：￥${order.orderAmount}元</td>
                                </tr>
                            </table>
                            <div class="control-group">
                                <div class="controls">
                                    <%--<button type="submit" class="btn btn-default">提交</button>--%>
                                    <%--<button type="reset" class="btn btn-default">重置</button>--%>
                                    <%--<button type="button" class="btn btn-primary" onclick="showModal('返回','您确定要离开此页面吗？','back()');">--%>
                                    <%--返回--%>
                                    <%--</button>--%>
                                    <a href="javascript:history.back(-1)" class="btn btn-primary">返回</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal hide fade" id="myModal" style="width: 50%;margin-left: -25%;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>发货信息</h3>
    </div>
    <div class="modal-body">
        <iframe src="order/deliver" style="border: 0; min-height: 100px; height: 100%; width: 100%; display: block;"></iframe>
    </div>
    <%--<div class="modal-footer">--%>
    <%--<a href="#" class="btn btn-primary" onclick="choseCommunity()">确定</a>--%>
    <%--</div>--%>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script>
    function deliver(expressOrderSn) {
        var data = {
            'shippingName': expressOrderSn,
            'orderSn': $('#orderSn').html(),
            'ajax': 1,
        };
        ajax('order/deliver', data, function (response) {
            $.noty.closeAll();
            var options = {"text": response.msg, "layout": "top", "type": response.result == 1 ? "success" : 'error'};
            noty(options);
            if (response.result == 1) {
                setTimeout(function () {
                    location.reload()
                }, 2000);
            }
        }, 'json');
    }
    $(function () {
        {
            //算合计
            var total = 0;
            $('.xiaoji').each(function (index, elmt) {
                total += $(elmt).html();
            })
            $('.heji').html(total);
        }
    })
</script>
</body>
</html>
