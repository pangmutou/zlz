<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <link href='charisma/css/login.css' rel='stylesheet'>
</head>
<body>
<center>
    <div class="top">
        <!-- <img src="img/yantiaxia.jpg" /> -->
        <div style="height: 175px;"></div>
        <img src="img/admin_logo.jpg" style="width: 461px;"/>
    </div>
    <div class="mid_box_t"></div>
    <div class="mid_box">
        <div class="mid_right">
            <div class="mid_right_b">
                <form class="form-horizontal" action="login" method="post">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="38%" class="mid_text">用户名：</td>
                            <td width="62%" align="left">
                                <input name="username" id="loginName" type="text" class="from"/></td>
                        </tr>
                        <tr>
                            <td class="mid_text">密码：</td>
                            <td align="left"><input name="password" id="password" type="password" class="from"/></td>
                        </tr>
                        <tr>
                            <td class="mid_text"></td>
                            <td align="left">
                                <div style="display:inline-block;">
                                    <div style="float:left;margin-top:1px;">
                                        <label class="remember" for="remember" style="width:80px;"><input type="checkbox" name="checkbox" id="remember" checked="checked" style="opacity: 0;"/>记住密码</label>
                                    </div>
                                    <%--<span style="margin-left:10px;" ><a href="javascript:void(0);" onclick="$('#myModal').modal();" class="ls_text">找回密码</a></span>--%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="left">
                                <label>
                                    <input type="button" onclick="loginsumbit();" name="button" id="button" value="登录" class="btn btn-primary"/>
                                </label>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</center>
<div id="footer" style="margin-top:100px;">
    <div class="container" style="width:100%;text-align:center;color:white;">
        <%--<span>--%>
        <%--<a href="http://www.miitbeian.gov.cn" target="_blank">粤ICP备14008059号-1</a>--%>
        <%--</span><br/>--%>
        <%--<span>			--%>
        <%--建议使用1024*768以上的屏幕分辨率来访问本站--%>
        <%--</span>--%>
        <%--<br/>--%>
        <%--<span id="ieBrower" style="display:none;color: red;" >--%>
        <%--<span>检测到您使用的是低版本的IE浏览器，IE8.0以上版本的效果更好</span>--%>
        <%--<br/>--%>
        <%--<a href="http://dlc2.pconline.com.cn/filedown_49581_6850107/UnfM5Sv4/IE8-WindowsXP-x86-CHS.exe" >--%>
        <%--IE8.0下载--%>
        <%--</a>--%>
        <%--</span>--%>
        <%--<span id="browser" style="display:none;color: red;" >--%>
        <%--<span>检测到您使用的不是IE浏览器，IE8.0以上版本的效果更好</span>--%>
        <%--<br/>--%>
        <%--<a href="http://dlc2.pconline.com.cn/filedown_49581_6850107/UnfM5Sv4/IE8-WindowsXP-x86-CHS.exe" >--%>
        <%--IE8.0下载--%>
        <%--</a>--%>
        <%--</span>--%>
    </div>
</div> <!-- /footer -->
<div class="modal hide fade" id="myModal">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <div class="modal-header">
        <h3>找回密码</h3>
    </div>
    <div class="modal-body" style="height: 100%;margin-left:20%;">
        <table width="282" height="153" border="0" cellspacing="0">
            <tr>
                <td width="40"><img src="img/telephone.jpg" width="40px;" height="40px;"/></td>
                <td width="230">
                    <div style="font-size:14px;margin-left:10px;">
                        请您拨打：<span style="color:#1c628b;">400-830-9995</span> 确认您的帐号信息，我们的客服人员帮您找回密码！
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal">确定</a>
    </div>
</div>
</body>
<%@include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script type="text/javascript">
    <c:if test="${empty userinfo}">
    init();
    </c:if>
    $("form").bind('keyup', function (event) {
        if ($('#loginName').val() && $('#password').val()) {
            if ($("#password").is(":focus")) {
                if (event.keyCode == 13) {
                    loginsumbit();
                }
            }
        }
    });
    function loginsumbit() {
        var remember = document.getElementById("remember");
        if (document.getElementById("remember").checked) {
            SetCookie("loginName", $('#loginName').val());
            SetCookie("password", $('#password').val());
        }
        $("form").submit();
    }
    function init() {
        $('#loginName').val(getCookie("loginName") || "");
        $('#password').val(getCookie("password") || "");
    }

    function SetCookie(name, value) {
        var Days = 30; //此 cookie 将被保存 30 天
        var exp = new Date();    //new Date("December 31, 9998");
        exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
        document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
    }

    function getCookie(name) {
        var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
        if (arr != null) return unescape(arr[2]);
        return null;
    }

    function delCookie(name) {
        var exp = new Date();
        exp.setTime(exp.getTime() - 1);
        var cval = getCookie(name);
        if (cval != null) document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
    }
    //检测IE9以下的浏览器版本
    var _IE = (function () {
        var v = 3, div = document.createElement('div'), all = div.getElementsByTagName('i');
        while (
                div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
                        all[0]
                );
        return v > 4 ? v : false;
    }());
    //检测IE10以上的浏览器版本
    var isIE = (function () {
        var flag = false;
        try {
            if (window.matchMedia("screen and (-ms-high-contrast: active), (-ms-high-contrast: none)").matches) {
                flag = true;
            }
        } catch (e) {
        }

        return flag;
    }());

    $(document).ready(function () {
        if (_IE) {
            if (_IE < 8) {
                $("#ieBrower").show();
            }
        } else if (isIE) {
            $("#ieBrower").hide();
            $("#browser").hide();
        } else {
            $("#browser").show();
        }
    });
</script>
</html>