<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<link href="FlexPaper/css/flexpaper.css" rel="stylesheet">
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
		<ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12" style="text-align:center;" >
                    <div class="box-header well data-original-title">
                        <h2><i class="icon-user"></i> 用户手册</h2>
                    </div>
                    <div class="box-content">
                        <div style="padding-left: 15px;">
	                       <a id="pdf_btn" class="btn btn-primary" href="user/manual" style="width: 150px;height: 40px;font-size:15px;line-height:40px;" >下载《用户手册》</a>
	                    </div>
	                    <br/><br/>
                        <div id="pdf" style="height: 800px;" >
                        	请安装Flash插件
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <!-- content ends -->
        </div>
        <!--/#content.span10-->
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript" src="FlexPaper/js/flexpaper.js" ></script>
<script type="text/javascript" src="FlexPaper/js/flexpaper_handlers.js" ></script>
<script type="text/javascript">
var params = {
		SwfFile : "FlexPaper/docs/maunal.swf",
		Scale : 1.3
		}
		swfobject.embedSWF("FlexPaper/FlexPaperViewer.swf","pdf","800","600","9.0.0","FlexPaper/docs/Paper.swf", params);
 </script>
</html>