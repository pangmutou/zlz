<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <style>
        .dataTable th {
            text-align: center;
            background-color: #d9edf7;
        }

        .dataTable td {
            vertical-align: middle;
        }

        .goodsCheckBox {
            cursor: pointer;
        }
    </style>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>商品列表</h2>
                    </div>
                    <br>
                    <div class="box-content">
                        <form class="form-horizontal" action="goods/page" method="get">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="productName">商品名称</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="productName" name="goodsName" placeholder="请输入商品名" value="${param.goodsName}">
                                        </div>
                                    </div>
                                    <div class="span3" style="width: auto;">
                                        <button type="submit" class="btn btn-primary"><i class="icon-search"></i>查询
                                        </button>
                                        <a href="goods/add" class="btn btn-success">添加</a>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable dataTable">
                                    <thead>
                                    <tr>
                                        <th>商品名称</th>
                                        <th>商品点击量</th>
                                        <th>商品库存数量</th>
                                        <th>本店价格</th>
                                        <th>相册</th>
                                        <th>上架</th>
                                        <%--<th>特价</th>--%>
                                        <%--<th>团购</th>--%>
                                        <th>创建时间</th>
                                        <th>创建人</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.goodsName}</td>
                                            <td>${item.clickCount}</td>
                                            <td>${item.goodsNumber}</td>
                                            <td>${item.shopPrice}</td>
                                            <td style="text-align: center;">
                                                <a href="${item.goodsThumb}" target="_blank"><img src="${item.goodsThumb}" height="50px" width="50px"></a>
                                            </td>
                                            <td style="text-align: center;">
                                                <img class="goodsCheckBox" data-goods-id="${item.goodsId}" data-value="${item.isOnSale}" data-status-type="sale" <c:if test="${item.isGroupPurchase != 1}">onclick="changeStatus(this)"</c:if> src="${item.isOnSale == 1 ? "img/yes.gif" : "img/no.gif"}">
                                            </td>
                                            <%--<td style="text-align: center;">--%>
                                                <%--<img class="goodsCheckBox" data-goods-id="${item.goodsId}" data-value="${item.isPromote}" data-status-type="promote" <c:if test="${item.isGroupPurchase != 1}">onclick="changeStatus(this)"</c:if> src="${item.isPromote == 1  ? "img/yes.gif" : "img/no.gif"}">--%>
                                            <%--</td>--%>
                                            <%--<td style="text-align: center;">--%>
                                                <%--<img class="goodsCheckBox" data-goods-id="${item.goodsId}" data-value="${item.isGroupPurchase}" data-status-type="" src="${item.isGroupPurchase == 1  ? "img/yes.gif" : "img/no.gif"}">--%>
                                            <%--</td>--%>
                                            <td><ry:formatDate date="${item.addTime}"></ry:formatDate></td>
                                            <td>${item.userName}</td>
                                            <td class="center">
                                                <c:if test="${item.isGroupPurchase != 1}">
                                                <a class="btn btn-primary" href="goods/update?goodsId=${item.goodsId}">编辑菜单</a>
                                                </c:if>
                                                <a class="btn btn-danger" href="javascript:showModal('删除','您确定删除吗？','delGoods(${item.goodsId})');">删除</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}" totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function delGoods(id) {
        urlUtils("goods/delete?goodsId=" + id);
    }
    function changeStatus(img) {
        var data = $(img).data();
        data.value = data.value ^ 1;
        $.getJSON("goods/changeStatus", data, function (json) {
            if (!json.result) {
                data.value = data.value ^ 1;
                alert('修改商品属性失败，请稍后再试')
            }
            $(img).attr("src", data.value ? "img/yes.gif" : "img/no.gif");
        })
    }
</script>
</html>