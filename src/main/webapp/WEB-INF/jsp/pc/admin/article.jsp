<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" class="form-horizontal" method="post">
                            <input type="hidden" name="catId" value="1">
                            <div class="control-group">
                                <label class="control-label">标题</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" placeholder="请输入标题" name="title" value="${article.title}">
                                    <span class="help-inline">标题不能为空</span>
                                </div>
                            </div>
                            <div class="control-group hide">
                                <label class="control-label">作者</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" placeholder="请输入作者" name="author" value="${article.author}">
                                    <span class="help-inline">作者不能为空</span>
                                </div>
                            </div>
                            <div class="control-group hide">
                                <label class="control-label">email</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" placeholder="email" name="authorEmail" value="${article.authorEmail}">
                                    <span class="help-inline">email不能为空</span>
                                </div>
                            </div>
                            <div class="control-group hide">
                                <label class="control-label">关键字</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" placeholder="关键字" name="keywords" value="${article.keywords}">
                                </div>
                            </div>
                            <div class="control-group hide">
                                <label class="control-label">类型</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" placeholder="请输入文章标题" name="articleType" value="${article.articleType}">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">内容</label>
                                <div class="controls">
                                    <textarea name="content" cols="30" rows="10">${article.content}</textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-success">提交</button>
                                    <button type="reset" class="btn btn-default">重置</button>
                                    <button type="button" class="btn" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="articleId" value="${article.articleId}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal hide fade" id="myModal" style="width: 80%;margin-left: -40%;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>选择小区</h3>
    </div>
    <div class="modal-body">
        <iframe src="community/list?isIframe=1" style="border: 0; min-height: 390px; height: 100%; width: 100%; display: block;"></iframe>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary" onclick="choseCommunity()">确定</a>
    </div>
</div>

<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script>
    function selectOrg(id, name) {
        $("#communityName").val(name);
        $("#communityId").val(id);

        $("#myModal").modal('hide');
    }
</script>
</body>
</html>
