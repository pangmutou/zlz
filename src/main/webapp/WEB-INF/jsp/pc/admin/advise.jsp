<%@ page import="com.ahnimeng.system.model.TWorkorder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>投诉与建议</h2>
                    </div>
                    <div class="box-content">
                        <form action="#" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label class="control-label">内容</label>
                                <div class="controls">
                                    <textarea name="content" cols="30" rows="10" readonly>${object.content}</textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="button" class="btn" onclick="showModal('返回','您确定要离开此页面吗？','back()');">返回</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
</html>
