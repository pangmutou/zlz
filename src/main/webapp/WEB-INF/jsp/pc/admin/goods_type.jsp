<%@ page import="com.ahnimeng.common.util.SpringContextUtil" %>
<%@ page import="com.ahnimeng.system.model.TCategory" %>
<%@ page import="com.ahnimeng.system.service.CategoryService" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
    <%
        ApplicationContext applicationContext = SpringContextUtil.getApplicationContext();
        CategoryService categoryService = applicationContext.getBean(CategoryService.class);
        //商品分类
        String categoryList = JSONArray.fromObject(categoryService.list(new TCategory())).toString();
        request.setAttribute("categoryList", categoryList);
    %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}商品属性</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" id="form" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label for="typeName" class="control-label">商品属性名称</label>
                                <div class="controls">
                                    <input name="typeName" id="typeName" type="text" class="input-xlarge focused" valitype="require" placeholder="请输入商品名称" value="${object.typeName}">
                                    <span class="help-inline">请输入商品属性名称</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="button" class="btn" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="goodsId" value="${object.typeId}" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
</html>