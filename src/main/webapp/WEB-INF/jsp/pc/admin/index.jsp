<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>

        <noscript>
            <div class="alert alert-block span10">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have
                    <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.
                </p>
            </div>
        </noscript>

        <div id="content" class="span10">
            <%--<c:if test="${systemUser.requestType==1}">--%>
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="#">待办事项</a>
                    </li>
                </ul>
            </div>
            <%--</c:if>--%>
            <div class="row-fluid hide" id="div_content">
                <%--<ry:authorize ifAnyGranted="A_1142">--%>
                <a data-rel="tooltip" title="" class="well span3 top-block" href="workorder/list?type=0">
                    <span class="icon32 icon-red icon-star-on"></span>
                    <div>公共设施工单</div>
                    <%--<span class="notification red REPAIR">1</span>--%>
                </a>
                <%--</ry:authorize>--%>
                <a data-rel="tooltip" title="" class="well span3 top-block" href="workorder/list?type=1">
                    <span class="icon32 icon-color icon-star-on"></span>
                    <div>业主报修</div>
                    <%--<span class="notification yellow FIRST_INSPECTION">2</span>--%>
                </a>
                <a data-rel="tooltip" title="" class="well span3 top-block" href="workorder/list?type=2">
                    <span class="icon32 icon-color icon-star-on"></span>
                    <div>投诉</div>
                    <%--<span class="notification yellow FIRST_INSPECTION">2</span>--%>
                </a>
                <a data-rel="tooltip" title="" class="well span3 top-block" href="expressOrder/page">
                    <span class="icon32 icon-color icon-star-on"></span>
                    <div>快递订单</div>
                    <%--<span class="notification yellow FIRST_INSPECTION">2</span>--%>
                </a>
                <a data-rel="tooltip" title="" class="well span3 top-block" href="order/list">
                    <span class="icon32 icon-color icon-star-on"></span>
                    <div>团购订单</div>
                    <%--<span class="notification yellow FIRST_INSPECTION">2</span>--%>
                </a>

            </div>
        </div>
        <!--/.fluid-container-->
    </div>
</div>

<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<%--<script type="text/javascript" src="js/md5.js"></script>--%>
<script type="text/javascript">
    var pendingOrderCount = eval('${pendingOrderCount}');
    for (var i in pendingOrderCount) {
        $(".notification." + pendingOrderCount[i].orderType).html(pendingOrderCount[i].pendingCount);
    }
    $(document).ready(function () {
        //调整了下标签的排版间距
        //不知道是谁写的，但用JS调整肯定是不对的
        var $_aArray = $("#div_content").find("a");
        $_aArray.each(function (index, element) {
            if (index % 4 == 0) {
                $(element).css("margin-left", "0px");
            }
        });
    });
</script>

</body>
</html>