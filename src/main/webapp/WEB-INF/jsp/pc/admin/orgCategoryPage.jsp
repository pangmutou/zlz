<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <ry:binding type="3"></ry:binding>
        <div id="content" class="span10">
            <!-- content starts -->
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-user"></i>商铺类目</h2>
                    </div>

                    <div class="box-content">
                        <form class="form-horizontal" action="org/category/page" method="post">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="itemName">类目名称</label>
                                    <div class="controls">
                                        <div class="span4" style="width: auto;">
                                            <input class="input-medium focused" type="text" id="itemName"
                                                   name="itemName"/>
                                        </div>
                                        <div class="span3" style="width: auto;">
                                            <button type="submit" class="btn"><i class="icon-search"></i>查询</button>
                                            <a href="org/category/save" class="btn btn-primary"><i class="icon-plus icon-white"></i>添加</a>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-bordered bootstrap-datatable">
                                    <thead>
                                    <tr>
                                        <th>类目名称</th>
                                        <th>类目值</th>
                                        <th>顺序</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${page.result}">
                                        <tr>
                                            <td>${item.itemName}</td>
                                            <td>${item.itemCode}</td>
                                            <td>${item.orderby}</td>
                                            <td class="center">
                                                <div class="btn-group group" style="float:left;margin-right:4px;">
                                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><i
                                                            class="icon-wrench"></i>操作<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="javascript:void(0);" onclick="urlUtils('org/category/edit?id=${item.id}')">编辑</a></li>
                                                        <li><a href="javascript:showModal('删除','您确定删除吗？','del(${item.id})');">删除</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="paginator" id="page" currentPage="${page.cur}"
                                     totalPages="${page.pageCount}" numberOfPages="5" onPageClicked="goPage"></div>
                                <input id="currentPageNo" type="hidden" name="cur" value="${page.cur}"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <!-- content ends -->
        </div>
        <!--/#content.span10-->
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
</body>
<script type="text/javascript">
    function goPage(event, obj, type, page) {
        if (obj.currentTarget.parentElement.className == "active")
            return;
        $("#currentPageNo").val(page);
        document.forms[0].submit();
    }
    function del(id) {
        urlUtils("org/category/delete?id=" + id);
    }
</script>
</html>
