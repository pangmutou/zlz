<%@ page import="com.ahnimeng.common.util.SpringContextUtil" %>
<%@ page import="com.ahnimeng.system.model.TCategory" %>
<%@ page import="com.ahnimeng.system.service.CategoryService" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/jsp/inc/base.jsp" %>
    <link href="js/fine-uploader-master/fine-uploader-gallery.css" rel="stylesheet" type="text/css"/>
    <%--<%--%>
        <%--ApplicationContext applicationContext = SpringContextUtil.getApplicationContext();--%>
        <%--CategoryService categoryService = applicationContext.getBean(CategoryService.class);--%>
        <%--//商品分类--%>
        <%--String categoryList = JSONArray.fromObject(categoryService.list(new TCategory())).toString();--%>
        <%--request.setAttribute("categoryList", categoryList);--%>
    <%--%>--%>
</head>
<body>
<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
<div class="container-fluid">
    <div class="row-fluid">
        <%@include file="/WEB-INF/jsp/inc/left.jsp" %>
        <div id="content" class="span10">
            <div class="row-fluid">
                <div class="box span12">
                    <div class="box-header well" data-original-title>
                        <h2><i class="icon-edit"></i>${methodName}商品信息</h2>
                    </div>
                    <div class="box-content">
                        <form action="${url}" id="form" class="form-horizontal" method="post" onsubmit="return $(this).validate()">
                            <div class="control-group hide">
                                <label for="goodsNumber" class="control-label">商品类型</label>
                                <div class="controls">
                                    <label style="margin-right: 1em; display: inline;">
                                        <input type="radio" class="uneditable-input" name="typeChosen" value="1" data-no-uniform="true">
                                        普通商品
                                    </label>
                                    <label style="margin-right: 1em; display: inline;">
                                        <input type="radio" class="uneditable-input" name="typeChosen" value="2" data-no-uniform="true">
                                        团购
                                    </label>
                                    <span style="padding: 5px 0 0 5px;display: inline-block;color: red;">注意：在选择为团购类型后将无法修改商品信息</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="goodsName" class="control-label">名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" valitype="require" id="goodsName" placeholder="请输入商品名称" name="goodsName" value="${goods.goodsName}">
                                    <span class="help-inline">请输入商品名称</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">市场价格</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" valitype="require,Currency" id="marketPrice" placeholder="请输入市场价格" name="marketPrice" value="${goods.marketPrice}">
                                    <span>该价格只用于比较显示，会覆盖上删除线</span>
                                    <span class="help-inline">请输入市场价格</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">售价</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" valitype="require,Currency" placeholder="请输入商品价格" name="shopPrice" value="${goods.shopPrice}">
                                    <span class="help-inline">请输入商品价格</span>
                                </div>
                            </div>
                            <div class="control-group hide">
                                <label class="control-label">运费</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" valitype="require,Currency" placeholder="请输入运费" name="expressFee" value="${goods.expressFee}">
                                    <span class="help-inline">请输入运费价格</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="goodsNumber" class="control-label">库存</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" valitype="require,int" id="goodsNumber" placeholder="请输入库存" name="goodsNumber" value="${goods.goodsNumber}">
                                    <span class="help-inline">请输入商品库存</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="goodsNumber" class="control-label">是否上架</label>
                                <div class="controls">
                                    <label>
                                        <input name="isOnSale" type="checkbox" value="1" ${goods.isOnSale == 1 ? "checked" : "" } data-no-uniform="true">
                                        上架
                                    </label>
                                </div>
                            </div>
                            <div class="control-group promote-div hide">
                                <label for="goodsNumber" class="control-label" style="display: inline-block;">是否特价</label>
                                <div class="controls">
                                    <label>
                                        <input type="checkbox" value="3" ${goods.isPromote == 1 ? "checked" : "" } data-no-uniform="true">
                                        特价
                                    </label>
                                    <span style="color: red;">提示：需要尽快销售商品时，可以勾选特价按钮</span>
                                </div>
                            </div>
                            <div class="control-group promote-div hide">
                                <label class="control-label">特价价格</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge focused" valitype="require,Currency" placeholder="请输入商品价格" name="promotePrice" value="${goods.promotePrice}">
                                    <span class="help-inline">请输入促销价格</span>
                                </div>
                            </div>
                            <div class="control-group groupPurchaseTime-div hide">
                                <label class="control-label">团购时间</label>
                                <div class="controls">
                                    <div style="display: inline;">
                                        <div class="input-append date form_datetime">
                                            <input name="groupPurchaseBeginTime" value="<ry:formatDate date="${goods.groupPurchaseBeginTime}"/>" data-format="yyyy-MM-dd hh:mm:ss" type="text" valitype="require" class="datetimepicker"><span class="add-on"><i></i></span>
                                        </div>
                                        至
                                        <div class="input-append date form_datetime">
                                            <input name="groupPurchaseEndTime" value="<ry:formatDate date="${goods.groupPurchaseEndTime}"/>" data-format="yyyy-MM-dd hh:mm:ss" type="text" valitype="require" class="datetimepicker"><span class="add-on"><i></i></span>
                                        </div>
                                    </div>
                                    <span class="help-inline">此项必填</span>
                                </div>
                            </div>
                            <%--<div class="control-group promote-price promote-type-div">--%>
                            <%--<label class="control-label">促销特价</label>--%>
                            <%--<div class="controls">--%>
                            <%--<input type="text" class="input-xlarge focused" valitype="require,Currency" placeholder="请输入商品价格" name="promotePrice" value="${goods.promotePrice}">--%>
                            <%--<span>需要尽快销售商品时，可以勾选特价按钮</span>--%>
                            <%--<span class="help-inline">此项必填</span>--%>
                            <%--</div>--%>
                            <%--</div>--%>
                            <%--<div class="control-group">--%>
                                <%--<label for="parentId" class="control-label">商品分类</label>--%>
                                <%--<div class="controls">--%>
                                    <%--<div class="span4" style="width: auto;">--%>
                                        <%--<select name="catId" id="parentId" data-rel="chosen">--%>
                                        <%--</select>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                            <%--<div class="control-group">--%>
                                <%--<label class="control-label">上传商品视频</label>--%>
                                <%--<div class="controls">--%>
                                    <%--<div id="video-uploader" style="width: 250px;"></div>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                            <div class="control-group">
                                <label class="control-label">上传商品图片</label>
                                <div class="controls">
                                    <div id="img-uploader"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">商品简介</label>
                                <div class="controls row-fluid" style="width: auto;">
                                    <div class="span4" style="width: auto;">
                                        <textarea name="goodsBrief" rows="6" valitype="require" style="resize: none;width: 200%;">${goods.goodsBrief}</textarea>
                                        <span class="help-inline">请输入商品简介</span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">商品详述</label>
                                <div class="controls row-fluid" style="width: auto;">
                                    <div class="span4" style="width: auto;">
                                        <textarea name="goodsDesc" valitype="require" class="cleditor">${goods.goodsDesc}</textarea>
                                        <span class="help-inline">请输入商品详述</span>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="reset" class="btn">重置</button>
                                    <button type="button" class="btn" onclick="showModal('返回','您确定要离开此页面吗？','back()');">
                                        返回
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="goodsId" value="${goods.goodsId}"/>
                            <input type="hidden" name="isGroupPurchase" value="${goods.isGroupPurchase}">
                            <input type="hidden" name="isPromote" value="${goods.isPromote}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp" %>
<script type="text/template" id="img-template">
    <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="拖拽文件到此即可上传">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector">拖拽文件到此即可上传</span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div>请点击上传</div>
        </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals" style="overflow-x: hidden;">
            <li>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <div class="qq-thumbnail-wrapper">
                    <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                </div>
                <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                    <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                    Retry
                </button>

                <div class="qq-file-info">
                    <%--<div class="qq-file-name">--%>
                    <%--<span class="qq-upload-file-selector qq-upload-file"></span>--%>
                    <%--<span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>--%>
                    <%--</div>--%>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                    </button>
                    <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                    </button>
                </div>
                <input type="hidden" name="imgUrl">
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-ok-button-selector">是</button>
                <button type="button" class="qq-cancel-button-selector">否</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
<script src="js/fine-uploader-master/all.fine-uploader.js"></script>
<script>

    $("[name='typeChosen']").click(function (event) {
        var selectValue = event.target.value;

        $("[name='isPromote']").val(selectValue == 1 ? 1 : 0);
        $("[name='isGroupPurchase']").val(selectValue == 2 ? 1 : 0);

    });

    var imgUploader;
    $(function () {
        "use strict";

        //判断商品是不是团购和还是促销
        var typeChosen = 1;
        var isPromote = "${goods.isPromote}";
        var isGroupPurchase = "${goods.isGroupPurchase}";
        if (isPromote == 1) {
            typeChosen = 1;
        }
        if (isGroupPurchase == 1) {
            typeChosen = 2;
        }
        $("[name='typeChosen'][value=" + typeChosen + "]").click();

        $(".form_datetime").datetimepicker({language: "zh-CN"});

        imgUploader = uploader(
                $("#img-uploader")[0],
                "img-template",
                5,
                updateFileInputValue,
                {
                    endpoint: "goods/gallery/list",
                    params: {'goodsId': '${goods.goodsId}'}
                }
        );
    });

    <%--categoryInit : $(function () {--%>
        <%--var parentId = 0, orderArray = [], categoryList = eval('${categoryList}');--%>
        <%--var rootNode = {"catId": 0, "level": 0, "catName": "顶级分类"};--%>

        <%--findChildren(rootNode);--%>
        <%--function findChildren(parentNode) {--%>
            <%--$(categoryList).each(function (i, json) {--%>
                <%--if (json.parentId == parentNode.catId) {--%>
                    <%--json.level = json.parentId == 0 ? 0 : parentNode.level + 1;--%>
                    <%--var indent = "";--%>
                    <%--for (var i = 0; i < json.level; i++) {--%>
                        <%--indent += "&nbsp;&nbsp;&nbsp;&nbsp;";--%>
                    <%--}--%>
                    <%--json.catName = indent + json.catName;--%>
                    <%--orderArray.push(json);--%>
                    <%--findChildren(json);--%>
                <%--}--%>
            <%--})--%>
        <%--}--%>

        <%--var $categorySelect = $("#parentId");--%>
        <%--$(orderArray).each(function (i, json) {--%>
            <%--var option = document.createElement("option");--%>
            <%--option.value = json.catId, option.innerHTML = json.catName;--%>
            <%--$categorySelect.append(option);--%>
        <%--});--%>

        <%--var categoryParentId = eval(${goods.catId});--%>
        <%--$categorySelect.val(categoryParentId);--%>
        <%--$categorySelect.trigger("liszt:updated");--%>
    <%--})--%>
</script>
</body>
</html>