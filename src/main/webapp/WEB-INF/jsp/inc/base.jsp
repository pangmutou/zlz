<%@ page import="com.ahnimeng.common.util.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<base href="<%=basePath%>"/>
<%-- <%=basePath%> --%>
<meta charset="utf-8">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<title><%=Constants.PROJECT_NAME%></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="https://cdn.bootcss.com/bootstrap/2.0.4/css/bootstrap.css" rel="stylesheet">
<link id="bs-css" href="charisma/css/bootstrap-cerulean.css" rel="stylesheet">
<link href="charisma/css/bootstrap-responsive.css" rel="stylesheet">
<link href="charisma/css/charisma-app.css" rel="stylesheet">
<link href="charisma/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
<link href='charisma/css/fullcalendar.css' rel='stylesheet'>
<link href='charisma/css/fullcalendar.print.css' rel='stylesheet' media='print'>
<link href='charisma/css/chosen.css' rel='stylesheet'>
<link href='charisma/css/uniform.default.css' rel='stylesheet'>
<link href='charisma/css/colorbox.css' rel='stylesheet'>
<link href='charisma/css/jquery.cleditor.css' rel='stylesheet'>
<link href='charisma/css/jquery.noty.css' rel='stylesheet'>
<link href='charisma/css/noty_theme_default.css' rel='stylesheet'>
<link href='charisma/css/elfinder.min.css' rel='stylesheet'>
<link href='charisma/css/elfinder.theme.css' rel='stylesheet'>
<link href='charisma/css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='charisma/css/opa-icons.css' rel='stylesheet'>
<link href='charisma/css/uploadify.css' rel='stylesheet'>
<link href='charisma/css/documentation.css' rel='stylesheet'>
<link href='charisma/css/bootstrap-datetimepicker.min.css' rel='stylesheet'>
<style type="text/css">
    body {
        padding-bottom: 40px;
    }
    .sidebar-nav {
        padding: 9px 0;
    }
    @media (max-width: 480px) {
        .modal.fade.in {
            top: 50%;
            margin-top: -80px
        }
        .sidebar-nav {
            padding: 0;
        }
    }

    input[disabled], select[disabled], textarea[disabled], input[readonly], select[readonly], textarea[readonly] {
        /*background-color: #ddd;*/
    }

    .controls-row * {
        margin-left: 20px;
    }

    .controls-row input {
        margin-left: 0px;
    }

    .controls-row *:first-child {
        margin-left: 0px;
    }
</style>
<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<!-- The fav icon -->
<%--<link rel="shortcut icon" href="img/favicon.ico">--%>
<script type="text/javascript" >
var basePath = document.getElementsByTagName("base")[0].getAttribute("href");
function loginout(){
	var url="loginout";
	window.location.href=basePath+url;
}
</script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ry" uri="http://www.ruanyun.com/taglib/ry" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nm" uri="http://www.ahnimeng.com/taglib/nm" %>
