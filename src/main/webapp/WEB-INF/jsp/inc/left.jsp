<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- left menu starts -->
<c:if test="${not empty systemUser }">
<div class="span2 main-menu-span" id="leftUrl">
    <div class="well nav-collapse sidebar-nav">
    <%--<c:if test="${systemUser.orgType!=2}">--%>
    <%--<ul class="nav nav-tabs nav-stacked main-menu" style="margin-bottom: 0px;text-align:center; ">--%>
    <%--<li class="nav-header hidden-tablet"><span--%>
                    <%--class="hidden-tablet"><font style="font-size:16px;color: #3B96CC;font-family: 微软雅黑;">智慧生活管理平台欢迎您！</font></span></li>--%>
    <%--</ul>--%>
    <%--</c:if>--%>
    <%--<c:if test="${systemUser.orgType==2 }">--%>
        <ul class="nav nav-tabs nav-stacked main-menu">
            <c:forEach var="item" items="${sessionScope.leftUrls}">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href=""><i
                        class="icon-th"></i><span
                        class="hidden-tablet">${item.authName}</span><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <c:forEach var="childItem" items="${item.childAuthority}">
                            <li><a class="ajax-link" href="${childItem.authUrl}">${childItem.authName}</a></li>
                        </c:forEach>
                    </ul>
                </li>
            </c:forEach>
        </ul>
	<%--</c:if>--%>
	<%--<c:if test="${systemUser.orgType!=2 }">--%>
	 <%--<ul class="nav nav-tabs nav-stacked main-menu">--%>
        	<%--<c:forEach var="item" items="${sessionScope.leftUrls}">--%>
        	 <%--<li>--%>
        	 	<%--<li class="nav-header hidden-tablet"><i class="icon-th-list"></i><span--%>
                    <%--class="hidden-tablet">${item.authName}</span></li>--%>
               	<%--<c:forEach var="childItem" items="${item.childAuthority}">--%>
               		<%--<li><a class="ajax-link" href="${childItem.authUrl}"><i class="icon"></i>${childItem.authName }</a></li>--%>
               	<%--</c:forEach>--%>
            <%--</li>  --%>
          <%--</c:forEach>        --%>
        <%--</ul>--%>
	        <%--<ul class="nav nav-tabs nav-stacked main-menu" style="margin-bottom: 0px;text-align:center; ">--%>
		    <%--&lt;%&ndash;<li class="nav-header hidden-tablet">&ndash;%&gt;--%>
		    	<%--&lt;%&ndash;<span class="hidden-tablet">&ndash;%&gt;--%>
		    		<%--&lt;%&ndash;<font style="font-size:11px;">服务热线：4008-4008-58</font>&ndash;%&gt;--%>
		    	<%--&lt;%&ndash;</span>&ndash;%&gt;--%>
		    <%--&lt;%&ndash;</li>&ndash;%&gt;--%>
		    <%--<li id="service_qq" class="nav-header hidden-tablet">--%>
		    	<%--<span class="hidden-tablet">--%>
		    		<%--<font style="font-size:13px;">服务QQ：<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=2562292248&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:800003250:51" alt="点击这里给我发消息" title="点击这里给我发消息"/></a></font>--%>
		    	<%--</span>--%>
		    <%--</li>--%>
	    <%--</ul>--%>
	<%--</c:if>--%>
        <%--<label id="for-is-ajax" class="hidden-tablet" for="is-ajax">
            <input id="is-ajax" type="checkbox"> Ajax 加载菜单</label>--%>
    </div>
	
    <!--/.well -->
</div>
</c:if>
<!--/span-->
<!-- left menu ends -->