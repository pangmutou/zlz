<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- jQuery -->
<script src="charisma/js/jquery-1.7.2.min.js"></script>
<!-- jQuery UI -->
<script src="charisma/js/jquery-ui-1.8.21.custom.min.js"></script>
<!-- transition / effect library -->
<script src="charisma/js/bootstrap-transition.js"></script>
<!-- alert enhancer library -->
<script src="charisma/js/bootstrap-alert.js"></script>
<!-- modal / dialog library -->
<script src="charisma/js/bootstrap-modal.js"></script>
<!-- custom dropdown library -->
<script src="charisma/js/bootstrap-dropdown.js"></script>
<!-- scrolspy library -->
<script src="charisma/js/bootstrap-scrollspy.js"></script>
<!-- library for creating tabs -->
<script src="charisma/js/bootstrap-tab.js"></script>
<!-- library for advanced tooltip -->
<script src="charisma/js/bootstrap-tooltip.js"></script>
<!-- popover effect library -->
<script src="charisma/js/bootstrap-popover.js"></script>
<!-- button enhancer library -->
<script src="charisma/js/bootstrap-button.js"></script>
<!-- accordion library (optional, not used in demo) -->
<script src="charisma/js/bootstrap-collapse.js"></script>
<!-- carousel slideshow library (optional, not used in demo) -->
<script src="charisma/js/bootstrap-carousel.js"></script>
<!-- autocomplete library -->
<script src="charisma/js/bootstrap-typeahead.js"></script>
<!-- tour library -->
<script src="charisma/js/bootstrap-tour.js"></script>
<!-- library for cookie management -->
<script src="charisma/js/jquery.cookie.js"></script>
<!-- calander plugin -->
<script src='charisma/js/fullcalendar.min.js'></script>
<!-- data table plugin -->
<%--<script src='charisma/js/jquery.dataTables.min.js'></script>--%>
<script src='charisma/js/jquery.dataTables.js'></script>
<!-- paginator plugin -->
<%--<script src='charisma/js/bootstrap-paginator.min.js'></script>--%>
<script src='charisma/js/bootstrap-paginator.js'></script>

<!-- chart libraries start -->
<script src="charisma/js/excanvas.js"></script>
<script src="charisma/js/jquery.flot.min.js"></script>
<script src="charisma/js/jquery.flot.pie.min.js"></script>
<script src="charisma/js/jquery.flot.stack.js"></script>
<script src="charisma/js/jquery.flot.resize.min.js"></script>
<!-- chart libraries end -->

<!-- select or dropdown enhancer -->
<script src="charisma/js/jquery.chosen.min.js"></script>
<%--<script src="charisma/js/chosen.jquery.js"></script>--%>
<!-- checkbox, radio, and file input styler -->
<script src="charisma/js/jquery.uniform.min.js"></script>
<!-- plugin for gallery image view -->
<script src="charisma/js/jquery.colorbox.min.js"></script>
<!-- rich text editor library -->
<script src="charisma/js/jquery.cleditor.min.js"></script>
<!-- notification plugin -->
<script src="charisma/js/jquery.noty.js"></script>
<!-- file manager library -->
<script src="charisma/js/jquery.elfinder.min.js"></script>
<!-- star rating plugin -->
<script src="charisma/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="charisma/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="charisma/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="charisma/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="charisma/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="charisma/js/charisma.js"></script>
<!-- 添了一个日历时间控件 -->
<script src="charisma/js/bootstrap-datetimepicker.js"></script>
<!-- 自己加的JS -->
<script src="js/custome.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/ajax2.js"></script>
<script type="text/javascript" src="js/showModal.js"></script>
<script type="text/javascript" src="js/jquery.messager.js"></script>
<script type="text/javascript" src="js/unicode.js"></script>
<script type="text/javascript" src="js/urlUtil.js"></script>
<%-- <noscript>
    <div class="alert alert-block span10">
        <h4 class="alert-heading">警告!</h4>
        <p>你需要启用<a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>.</p>
    </div>
</noscript> --%>
<style>
    @media (min-width: 1200px) {
        .message {
            width: 200px;
            height: 180px;
        }

        .message_info {
            width: 187px;
            height: 130px;
        }

        #demo1Btn {
            display: none;
        }
    }

    @media (min-width: 768px) and (max-width: 979px) {
        .message {
            width: 200px;
            height: 180px;
        }

        .message_info {
            width: 183px;
            height: 130px;
        }

        #demo1Btn {
            display: none;
        }
    }

    @media (max-width: 767px) {
        .message {
            width: 170px;
            height: 130px;
        }

        .message_info {
            width: 153px;
            height: 80px;
        }

        #demo1Btn {
            display: none;
        }
    }

    .demo1Btn {
        width: 50px;
        height: 50px;
        line-height: 50px;
        text-align: center;
        -webkit-border-radius: 50px;
        -moz-border-radius: 50px;
        border-radius: 50px;
        right: 50px;
        bottom: 50px;
        z-index: 99;
        position: fixed;
        font-weight: 400;
        display: none;
        color: #fff;
        cursor: pointer;
        overflow: hidden;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .demo1Btn:hover {
        border: 2px solid rgba(255, 255, 255, 1);
    }

    .demo1Btn:hover .first-link {
        margin-top: -50px;
    }

    .demo1Btn {
        width: 40px;
        height: 45px;
        border-radius: 30px;
        line-height: 40px;
        right: 15px;
        bottom: 30px;
    }

    .demo1Btn:hover .first-link {
        margin-top: -30px;
    }

    @-webkit-keyframes upMotion {
        0% {
            opacity: 0;
            -webkit-transform: translateX(500px);
        }
        50% {
            opacity: 0;
            -webkit-transform: translateX(200px);
        }
        100% {
            opacity: 1;
            -webkit-transform: translateX(0);
        }
    }

    @-moz-keyframes upMotion {
        0% {
            opacity: 0;
            -moz-transform: translateX(500px);
        }
        50% {
            opacity: 0;
            -moz-transform: translateX(200px);
        }
        100% {
            opacity: 1;
            -moz-transform: translateX(0);
        }
    }

    @-o-keyframes upMotion {
        0% {
            opacity: 0;
            -o-transform: translateX(500px);
        }
        50% {
            opacity: 0;
            -o-transform: translateX(200px);
        }
        100% {
            opacity: 1;
            -o-transform: translateX(0);
        }
    }

    @keyframes upMotion {
        0% {
            opacity: 0;
            transform: translateX(500px);
        }
        50% {
            opacity: 0;
            transform: translateX(200px);
        }
        100% {
            opacity: 1;
            transform: translateX(0);
        }
    }
</style>
<ry:pageMessage result="${msg}" replaceStr="message">
    <script>$(document).ready(function () {
        var options = {"text": "message", "layout": "top", "type": "success"};
        noty(options);
    });</script>
</ry:pageMessage>

<script>
    var msg = '${param.msg}' || '${msg}';
    if (msg && isNaN(msg)) {
        $(document).ready(function () {
            var options = {"text": msg, "layout": "top", "type": "success"};
            noty(options);
        });
    }
</script>
<%--<c:if test="${not empty systemUser}" >--%>
<%--<div id="demo1Btn" class="demo1Btn upMotion" >--%>
<%--<a class="first-link" href="javascript:void(0);"></a>--%>
<%--<a id="message_link" href="javascript:void(0);">--%>
<%--<img src="img/message.png" />--%>
<%--</a>--%>
<%--</div>--%>
<%--<script type="text/javascript">--%>
<%--var screenWidth = window.screen.width;--%>
<%--var message = null;--%>
<%--function serviceCenterOrderCount(){--%>
<%--$.ajax({--%>
<%--type:"get",--%>
<%--url:basePath+"order/serviceCenterOrderCount",--%>
<%--data:"",--%>
<%--cache:false,--%>
<%--global: false,--%>
<%--dataType:"html",--%>
<%--success:function(jsonResult){--%>
<%--var json = eval(jsonResult);--%>
<%--var message = null;--%>
<%--if(json[0].count!=0){--%>
<%--message = ('<a href="<c:url value="/order/queryServiceOne" />">您有<font style="color:red;">'+json[0].count+'</font>条待初审的工单</a><br/><br/>');--%>
<%--}--%>
<%--if(json[0].review!=0){--%>
<%--if(message!=null){--%>
<%--message = message + ('<a href="<c:url value="/order/queryServiceTwo"/>">您有<font style="color:red;">'+json[0].review+'</font>条复审的工单</a><br/><br/>');--%>
<%--}else{--%>
<%--message = ('<a href="<c:url value="/order/queryServiceTwo"/>">您有<font style="color:red;">'+json[0].review+'</font>条复审的工单</a><br/><br/>');--%>
<%--}--%>
<%--}--%>
<%--if(json[0].factoryCount!=0){--%>
<%--if(message!=null){--%>
<%--message = message + ('<a href="<c:url value="/order/queryFactoryOne" />">您有<font style="color:red;">'+json[0].factoryCount+'</font>条待审核的工单</a><br/><br/>');--%>
<%--}else{--%>
<%--message = ('<a href="<c:url value="/order/queryFactoryOne" />">您有<font style="color:red;">'+json[0].factoryCount+'</font>条待审核的工单</a><br/><br/>');--%>
<%--}--%>
<%--}--%>
<%--if(json[0].repairStationFir!=0){--%>
<%--if(message!=null){--%>
<%--message = message+('<a href="<c:url value="/order/queryRepairStationOne" />">您有<font style="color:red;">'+json[0].repairStationFir+'</font>条待检测的工单</a><br/><br/>');--%>
<%--}else{--%>
<%--message = ('<a href="<c:url value="/order/queryRepairStationOne" />">您有<font style="color:red;">'+json[0].repairStationFir+'</font>条待检测的工单</a><br/><br/>');--%>
<%--}--%>
<%--}--%>
<%--if(json[0].repairStationSec!=0){--%>
<%--if(message!=null){--%>
<%--message = message + ('<a href="<c:url value="/order/queryRepairStationTwo" />">您有<font style="color:red;">'+json[0].repairStationSec+'</font>条待维修的工单</a><br/><br/>');--%>
<%--}else{--%>
<%--message = ('<a href="<c:url value="/order/queryRepairStationTwo" />">您有<font style="color:red;">'+json[0].repairStationSec+'</font>条待维修的工单</a><br/><br/>');--%>
<%--}  --%>
<%--}--%>
<%--if(message!=null){--%>
<%--if(screenWidth>=1200){--%>
<%--$.messager.lays(200, 180);--%>
<%--$.messager.show(0,message,5000);--%>
<%--}else{--%>
<%--$("#demo1Btn").show();--%>
<%--$("#message_link").click(function(){--%>
<%--$.messager.lays(200, 180);--%>
<%--$.messager.show(0,message,5000);--%>
<%--});--%>
<%--}--%>
<%--}else{--%>
<%--$("#demo1Btn").hide();--%>
<%--}--%>
<%--}--%>
<%--});--%>
<%--}--%>
<%--//		if(screenWidth>=1200){--%>
<%--//			$(document).ready(function(){--%>
<%--//				$("#service_qq").show();--%>
<%--//				serviceCenterOrderCount();--%>
<%--//				window.setInterval("serviceCenterOrderCount()",60000);//1分钟刷新1次--%>
<%--//			});--%>
<%--//		}else{--%>
<%--//			$("#service_qq").hide();--%>
<%--//			serviceCenterOrderCount();--%>
<%--//		}--%>
<%--</script>--%>
<%--</c:if>--%>