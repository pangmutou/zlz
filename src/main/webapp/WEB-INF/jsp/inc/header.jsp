<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
	@media (min-width: 1200px) {
		.header-top{
			font-size: 30px;
		}
		.brand-width{
			width:80%;
		}
		#pc_title{
			display:block;
		}
		#wap_title{
			display:none;
		}
		.form-horizontal .control-group {
  			margin-bottom: 20px;
  			*zoom: 1;
		}
		 body{
        	font-size: 14px;
        }
	}
	@media (min-width: 768px) and (max-width: 1199px) {
		.header-top{
			font-size: 20px;
		}
		.brand-width{
			width:200px;
		}
		#pc_title{
			display:none;
		}
		#wap_title{
			display:block;
		}
		.form-horizontal .control-group {
  			margin-bottom: 5px;
  			*zoom: 1;
		}
		 body{
        	font-size: 14px;
        }
	}
	@media (max-width: 767px) {
		.header-top{
			font-size: 15px;
		}
		.brand-width{
			width:200px;
		}
		#pc_title{
			display:none;
		}
		#wap_title{
			display:block;
		}
		.form-horizontal .control-group {
  			margin-bottom: 5px;
  			*zoom: 1;
		}
		 body{
        	font-size: 14px;
        }
	}

</style>
<!-- topbar starts -->
<div id="a1" class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
                	<a class="brand brand-width" href="index">
                	<c:if test="${systemUser.orgType!=2}">
	                	<span class="header-top">
	                		<font id="pc_title" face="微软雅黑"><%=Constants.PROJECT_NAME%></font>
	                		<font id="wap_title" face="微软雅黑"><%=Constants.PROJECT_NAME%></font>
	               		</span>
               		 </c:if>
               		 <c:if test="${systemUser.orgType==2}">
               		 	<span class="header-top">
	                		<font face="微软雅黑"><%=Constants.PROJECT_NAME%></font>
	               		</span>
               		 </c:if>
            </a>
               <div class="btn-group pull-right"><a class="btn dropdown-toggle"
                                                 data-toggle="dropdown" href="#"> <i class="icon-user"></i><span
                    class="hidden-phone">${systemUser.username}</span> <span class="caret"></span> </a>
                <ul class="dropdown-menu">
                    <li><a href="user/info/updatePwd">修改密码</a></li>
                    <li class="divider"></li>
                    <li><a href="javascript:showModal('退出','您确定要离开当前系统吗？','loginout()');">退出系统</a></li>
                </ul>
            </div>
             <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse" style="color: black;font-family: 微软雅黑;">菜单</a> 
            <!-- theme selector starts
            <div class="btn-group pull-right theme-container"><a
                    class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <i
                    class="icon-tint"></i><span class="hidden-phone">换个样子？</span> <span
                    class="caret"></span> </a>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="icon-blank"></i>
                        Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="icon-blank"></i>
                        Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="icon-blank"></i>
                        Cyborg</a></li>
                    <li><a data-value="redy" href="#"><i class="icon-blank"></i>
                        Redy</a></li>
                    <li><a data-value="journal" href="#"><i class="icon-blank"></i>
                        Journal</a></li>
                    <li><a data-value="simplex" href="#"><i class="icon-blank"></i>
                        Simplex</a></li>
                    <li><a data-value="slate" href="#"><i class="icon-blank"></i>
                        Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="icon-blank"></i>
                        Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="icon-blank"></i>
                        United</a></li>
                </ul>
            </div> -->
            <!-- theme selector ends --> <!-- user dropdown starts -->
         
            <!-- user dropdown ends -->
        </div>
    </div>
</div>
<!-- topbar ends -->