function showStep(orderNumber){
		$("#sonNode_div").hide();
		$("#grandsonNode_div").hide();
		$("#stepRemark").val("");
		$("#stepOrder").val(orderNumber);
		$("#modalCopy").hide();
		$("#parent_node").find("option[value='0']").attr("selected",true);
		$("#step_content").empty();
		searchStepContent(orderNumber,5,stepdisplay);
		$("#modal_Step").modal({
			keyboard:false,
			backdrop: 'static'
		});
}

function querySonNode(){
	$("#sonNode_div").hide();
	$("#grandsonNode_div").hide();
	var id = $("#parent_node").val();
	//alert('querySonNode='+id);
	ajax("step/querySon", { parentCode: id }, function (result) {
        $("#son_node").updateSelect(result, "itemName", "itemCode","");
        var son_id = $("#son_node").val();
        if(id != 0){
        	if(result!=null){
        		$("#sonNode_div").show();
        		if(son_id=="请选择")
        			$("#grandsonNode_div").hide();
        	}else{
        		$("#sonNode_div").hide();
        	}
        }else{
        	$("#sonNode_div").hide();
        	$("#grandsonNode_div").hide();
        }
    }, "json");
}

function queryGrandSonNode(){
	var id = $("#son_node").val();
	//alert(id);
	ajax("step/querySon", { parentCode: id }, function (result) {
        $("#grandson_node").updateSelect(result, "itemName", "itemCode","");
        if(id=="请选择" || id == 0){
        	$("#grandsonNode_div").hide();
        }else{
        	if(result!=null){
	        	$("#grandsonNode_div").show();
	        }else{
	        	$("#grandsonNode_div").hide();
	        }
        }
    }, "json");
}

function queryStepInfo(parentType,sonType,orderNumber){
	var answer = true;
	var url = "step/getStep";
	$.ajax({
	      type: "POST",
	      url: url,
	      async:false,
	      cache: false,
	      data: {parentType:parentType,sonType:sonType,orderNumber:orderNumber},
	      success: function(str) {
	    	if(str=='false'){
	    		answer = false;
	    	}
	      }
	});
	return answer;
}

function stepsubmit(){
	var str = $("#fromstep").serialize();
	var orderNumber = $("#stepOrder").val();
	var parent_node = $("#parent_node").val();
	var son_node = $("#son_node").val();
	var grandson_node = $("#grandson_node").val();
	if(parent_node == 0){
		alert("请选择响应的流程步骤");
		return false;
	}
	if($("#son_node").is(":visible")){
		if(son_node=='请选择'){
			alert("请选择响应的流程步骤");
			return false;
		}
	}
	if($("#grandson_node").is(":visible")){
		if(grandson_node=='请选择'){
			alert("请选择响应的流程步骤");
			return false;
		}
	}
	var stepInfo = queryStepInfo(parent_node,son_node,orderNumber);
	var r = null;
		if(!stepInfo){
			r = confirm("此流程已有记录，是否需要再次保存？");
		}
		if(r==null){
			r="123";
		}
		if(r){
			$.ajax({
			      type: "POST",
			      url: "step/saveOrderStep",
			      cache: false,
			      data: str,
			      success: function(result) {
			    	if(result=='false'){
			    		alert("保存失败");
			    		return false;
			    	}else{
			    		$("#parent_node").find("option[value='0']").attr("selected",true);
			    		$("#sonNode_div").hide();
			    		$("#grandsonNode_div").hide();
			    		$("#stepRemark").val("");
			    		searchStepContent(orderNumber,5,stepdisplay);
			    	}
			      }
			});
		}
		return false;
};

function stepdisplay(res){
	$("#step_content").html(res);
}
function searchStepContent(orderNumber,limit,fun){
	var url = "step/getList?orderNumber="+orderNumber;
	if(limit){
		url += "&limit="+limit;
	}
	var steplist = new MyJqueryAjax(url,null,fun,"text");
	steplist.request();
}
function stepCopydisplay(res){
	$("#modalCopy").html(res);
	$("#modalCopy").fadeIn(1000);
}
function getAllStep(){
	var orderNumber = $("#stepOrder").val();
	$("#save_btn").hide();
	searchStepContent(orderNumber,null,stepCopydisplay);
}
function closeModalCopy(){
	$("#save_btn").show();
	$("#modalCopy").fadeOut(1000);
}
