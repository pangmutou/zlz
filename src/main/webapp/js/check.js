function addrow() {
    var faultName = $("#order_fault").find('option:selected').text(),
        faultNumber = $("#order_fault").val(),
        maintenancePointPrice = $("#order_fault").find('option:selected').attr("price"),
        count = $("#fault_count").val();

    if (faultNumber && count) {
        var divclone = $("#fault_template").clone();
        divclone.css("display", "block");
        divclone.find("input[name='faultName']").val(faultName);
        divclone.find("input[name='faultNumber']").val(faultNumber);
        divclone.find("input[name='maintenancePointPrice']").val(maintenancePointPrice);
        divclone.find("input[name='faultCount']").val(count);
        $("#fault_template").after(divclone);
        updateZJ();
        $("#order_fault").formatChosen();
        $("#fault_count").val("");
    }
}

var imageHiddenCount = 1;
function addfilerow(elmt) {
    var group_div = $("#" + elmt).eachSelect(".control-group");
    var group_clone = group_div.clone();
    group_clone.find(".control-label").html('');
    group_clone.find("span").html('');
    //group_clone.find(".filename").html("请上传您的附件");
    group_clone.find("input:file").uniform();
    group_clone.find("input[type='hidden']").attr("id", "imageValue_" + imageHiddenCount);
    var button = group_clone.find("button");
    group_clone.find("br").remove();
    button.html("删除");
    button.show();
    button[0].onclick = null;
    button.click(function () {
        $(this).eachSelect(".control-group").remove();
    });
    $(".input-file.uniform_on").last().eachSelect(".control-group").after(group_clone);
//        group_div.after(group_clone);

    //alert(group_clone);
    imageHiddenCount = imageHiddenCount + 1;
}

/*
 *验证选择故障
 */
/**function checkFault(){
		var faultSize=$("#order_fault").length;
		if(faultSize>0){
			var chooseFaultSize=$("input[name=faultName]").length;
			if(chooseFaultSize==1){
				$("#order_fault").lightHelp(0,1);
				return false;
			}
		}
		return true;;
	}**/

function fsubmit() {
    var selectValue = $("#remarkTitle").val();
    var result = $("input[name=result]").val();
    var status = $("#status").val();
    if ((result == 1 && status == 2) || (result == -2)) {
        $("select[name='score']").attr("valitype", "require");
        $("textarea[name='repairScoreRemark']").attr("valitype", "require");
    } else {
        $("select[name='score']").removeAttr("valitype");
        $("textarea[name='repairScoreRemark']").removeAttr("valitype");
    }
    var isTrue = $(".form-horizontal").validate();
    if (!selectValue) {
        $("#remarkTitle").lightHelp(0, 1);
        //$("#remarkTitle").eachSelect(".controls").find(".help-inline").html("请选择相应的操作信息");
        return false;
    }
    if (result == 1 && isTrue) {
        if (!checkFault()) {//||!checkFile()
            return false;
        }
        $("#fault_template").remove();
        if (status == 2) {
            // getAllOtherPrice();
        }
    } else if (result != 1) {
        if ($("#isSubmit").val() == "true")
            return true;
        return false;
    } else
        return false;
}
function checkFault() {
    var $faultCount = $("#fault_count");
    if ($faultCount.length > 0) {
        var faultCount = $("input[name='faultNumber']").length;
        if (faultCount > 1) {
            return true;
        }
        showModal('提示', '工单信息不完整，请检查故障产品信息是否保存');
        //alert("工单信息不完整，请检查故障产品信息是否保存");
        return false;
    }
    return true;
}

//验证附件是否有
function checkFile() {
    var files = $("input[type='file']"),
        hasFile = true;
    if (files.length > 0) {
        $.each(files, function (i, obj) {
            var filePath = obj.value;
            if (!filePath) {
                $("#uploadFileInput").lightHelp(0, 1);
                hasFile = false;
            }
        });
    }
    return hasFile;
}

function setLabel() {
    var vStatus = $("#status").val();
    switch (vStatus) {
        case "0":
            $("#audit-title").text('审核工单');
            $("#lblRemark").text("初审意见");
            $("#remarkContent").text("请选择初审意见");
            $("#btnOk").text("派发工单");
            $("#btnNo").hide();
            $("#btnOver").hide();
            $("#btnNo").click(function () {
                if (confirm("发回重填后，工单会重新编辑，是否确认发回？")) {
                    $("#isSubmit").val("true");
                } else {
                    $("#isSubmit").val("false");
                }
                //showModal('删除','您确定删除吗？','$("#isSubmit").val("true")','$("#isSubmit").val("false")');
            });
            break;
        case "1":
            $("#qqInfo").hide();
            $("#door").text("是否上门服务");
            $("#audit-title").text('维修站维修');
            $("#lblRemark").text("维修结果");
            $("#remarkContent").text("请选择检测结果");
            $("#btnOk").text("上传报告");
            $("#btnNo").text("退回工单");
            $("#btnOver").hide();
            break;
        case "2":
            $("#audit-title").text('审核工单');
            $("#door").text("上门费");
            $("#lblRemark").text("复审意见");
            $("#remarkContent").text("请选择复审意见");
            $("#btnOk").text("转发工厂审核");
            $("#btnNo").text("发回维修站重新检测");
            $("#btnOver").hide();
            $("#btnStop").hide();
            break;
        case "3":
            $("#audit-title").text('审核工单');
            $("#lblRemark").text("备注说明");
            $("#door").text("上门费");
            $("#remarkContent").text("请选择备注说明");
            $("#btnOk").text("提交审核意见");
            $("#btnNo").text("不同意维修");
            $("#btnOver").text("取消维修");
            $("#btnStop").hide();
            $("#btnNo").hide();
            $("#btnOver").hide();
            $("#btnNo").click(function () {
                var remark = $("#remark").val();
                if (!remark) {
                    showModal('提示', '请在备注栏填写不同意维修的原因');
                    $("#isSubmit").val("false");
                } else {
                    $("#isSubmit").val("true");
                }
            });
            $("#btnOver").click(function () {
                var remark = $("#remark").val();
                if (!remark) {
                    showModal('提示', '请在备注栏填写取消维修的原因');
                    $("#isSubmit").val("false");
                } else {
                    $("#isSubmit").val("true");
                }
            });
            break;
        case "4":
            $("#qqInfo").hide();
            $("#audit-title").text('维修站维修');
            $("#lblRemark").text("维修结果");
            $("#door").text("上门费");
            $("#remarkContent").text("请选择维修结果");
            $("#lblFile").text("上传维修单据");
            $("#btnOk").text("上传报告");
            $("#btnNo").hide();
            $("#btnOver").hide();
            $("#spanFile").text("(选择的照片在上传报告时自动提交)");
            break;
        default:
            break;
    }
    /**if(vStatus==1){
			$("#lblRemark").text("检测结果");
			$("#lblFile").text("上传故障照片");
		}else if(vStatus==4){
			$("#lblRemark").text("维修结果");
			$("#lblFile").text("上传维修单据");
			$("#spanFile").text("(选择的照片在同意时自动提交的)");
		}**/
}

$(function () {
    $("button[type='submit']").click(function () {
        $("input[name='nextStep']").val($(this).val());
    });
    //updateZJ();
    //setTimeout($('#myModal').css("display", "none"), 1000);
    setLabel();
});