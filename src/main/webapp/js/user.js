function upPass(){
	$('#upBtn').hide();
	$('#firstPass').show();
	$('#secondPass').show();
	$("#oldPwdDiv").show();
}
function upAuthPass(){
	$('#upAuthBtn').hide();
	$("#authPassDiv").show();
	$('#firstAuthPass').show();
	$('#secondAuthPass').show();
}

/**
 * 删除用户
 * @return
 */
function delOk(id){
	 // var url = urlUtils('user/del?userId='+id);
	window.location.href = basePath + 'user/info/del?userId=' + id;
}

/**
 * 管理员修改用户重置密码
 * @return
 */
function resetPassword(){
	$('#password').val('');
	$('#resetBtn').text('密码已重置，请保存...');
	document.forms[0].submit();
}
/**
 * 修改用户审核密码
 * @return
 */
function resetAuthPassword(id){
	location.href = "user/password/reset?userId=" + id;
}

/**
 * 返回到用户列表页
 * @return
 */
function backBtn(){
	var url='user/users';
	window.location.href=basePath+url;
}