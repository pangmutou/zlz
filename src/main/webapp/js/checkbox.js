
//选中全选按钮，下面的checkbox全部选中 
function check_All() {
	var checkObj = document.getElementsByName("checkone");
	if (document.getElementById("checkall").checked == false) {
		for ( var i = 0; i < checkObj.length; i++) {
			checkObj[i].checked = false;
			checkObj[i].parentNode.className = "";
		}
	} else {
		for ( var i = 0; i < checkObj.length; i++) {
			checkObj[i].checked = true;
			checkObj[i].parentNode.className = "checked";
		}
	}

}

// 当选中所有的时候，全选按钮会勾上
function setSelectAll() {
	var obj = document.getElementsByName("checkone");
	var count = obj.length;
	var selectCount = 0;
	for ( var i = 0; i < count; i++) {
		if (obj[i].checked == true) {
			selectCount++;
		}
	}
	if (count == selectCount) {
		document.getElementById("checkall").checked = true;
		$("#checkall").parent().addClass("checked");
	} else {
		document.getElementById("checkall").checked = false;
		$("#checkall").parent().removeClass("checked");
	}
}

// 反选按钮
function inverse() {
	var checkboxs = document.getElementsByName("checkAll");
	for ( var i = 0; i < checkboxs.length; i++) {
		var e = checkboxs[i];
		e.checked = !e.checked;
		setSelectAll();
	}
}
