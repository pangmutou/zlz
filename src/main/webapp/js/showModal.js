/**
 * 提示框
 * @param title 标题
 * @param content 内容
 * @param fun   方法名
 * @param isTrue 是否随着滚动条滚动
 * @return
 */


function showModal(title,content,fun,closefun,isTrue){
	$('#myModals').remove();
	var closeClick="";
	if(closefun){
		closeClick="onclick=\""+closefun+"\" ";
	}
	var divHtml = "<div class='modal hide fade'  id='myModals'>"
				 +"	<div class='modal-header'>"
				 +"	<button type='button' class='close' data-dismiss='modal'>×</button>"
				 +"	<h3>"+title+"： </h3>"
				 +"</div>"
				 +"<div class='modal-body'><p>"+content+"</p></div>"
				 +"<div class='modal-footer'>"
				 +"	<a href='#' class='btn' data-dismiss='modal' "+ closeClick+">关闭</a>"
				 +"	<a href='#' id='aok' class='btn btn-primary'  onclick=\""+fun+";return false;\">确定</a>"
				 +"</div>"
				 +"</div>";
    $('#content1').after(divHtml);
	$('#content').after(divHtml);
	if(fun == null || fun==''){
		$('#aok').hide();
	}
	if(isTrue){
		$('#myModals').modal('show');
	}else{
		$('#myModals').modal('show').css({
	        'margin-top': function () {
			var heightSize=$("#myModals").height/2;
			return heightSize-($(this).height() / 2);
	    } ,
	    'position':'fixed'
		});
	}
	
}

function showBig(x){
	var elem = document.getElementById("focusphoto");
	$("#modalBody").css("max-height",$("#modalBody").css("height"));
	$(elem).css("max-width","none");
	$(elem).css("max-height","none");
	var aImg=$("#aImg").text();
	if("放大"==aImg){
		$("#aImg").text("缩小");
		x=1;
	}else{
		$("#aImg").text("放大");
		x=-1;
	}
	if(x>0){
		elem.style.width=parseInt($(elem).css("width")*1.2)+"px";
		//elem.scrollWidth=parseInt(elem.style.width*1.2)+200+"px";
		elem.style.height=parseInt(elem.scrollHeight*1.2)+"px";
	}else{
		elem.style.width=parseInt($(elem).css("width"))+"px";
		//elem.scrollWidth=parseInt(elem.style.width*1.2)+200+"px";
		elem.style.height=parseInt(elem.scrollHeight)+"px";
		$(elem).css("max-width","100%");
		$(elem).css("max-height",$("#modalBody").css("height"));
	}
	return;
}

function showModalImg(title,img){
	var divContent = "<div class='modal hide fade'  id='myModals'>"
		 +"<div class='modal-header'>"
		 +"<button type='button' class='close' data-dismiss='modal'>×</button>"
		 +"<h3>"+title+"： </h3>"
		 +"</div>"
		 +"<div id=\"modalBody\" class='modal-body' style='height: 100%;text-align:center;' ><img id=\"focusphoto\" alt='' src='"+img+"'></div>"
		 +"<div class='modal-footer'>"
		 +"	<a id=\"aImg\" href='javascript:showBig(1);' class='btn btn-primary'>放大</a>"
		 +"<a href='#' class='btn btn-primary' data-dismiss='modal' >关闭</a>"
		 +"</div>"
		 +"</div>";
	$('#content').after(divContent);
	$('#myModals').modal('show').css({
        'margin-top': function () {
		var heightSize=$("#myModals").height;
		return heightSize-($(this).height() / 2);
    },
    	'position':'fixed'
	}).css({'margin-top':-200});
}


