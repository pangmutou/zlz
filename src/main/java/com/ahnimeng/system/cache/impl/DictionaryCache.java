package com.ahnimeng.system.cache.impl;

import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.common.util.TimeUtil;
import com.ahnimeng.system.cache.SystemCacheService;
import com.ahnimeng.system.model.TDictionary;
import com.ahnimeng.system.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DictionaryCache implements SystemCacheService {


    @Autowired
    DictionaryService dictionaryService;

    private static Map<String, TDictionary> dictionaryMap = new HashMap();

    @Override
    public String getCacheName() {
        return "加载字典完毕 结束时间为:" + TimeUtil.getCurrentDay(SysCode.DATE_FORMAT_NUM_L);
    }

    @Override
    public void run() {
        update();
    }

    @Override
    public void update() {
        List<TDictionary> dictionaryList = dictionaryService.getAvailable();
        dictionaryMap.clear();
        for (TDictionary dictionary : dictionaryList) {
            dictionaryMap.put(dictionary.getParentName() + "-" + dictionary.getItemName(), dictionary);
        }
    }

    public static Map<String, TDictionary> getDictionaryMap() {
        return dictionaryMap;
    }

    public static TDictionary getDictionary(String key) {
        return dictionaryMap.get(key);
    }

    public static String getDictionaryCode(String key) {
        return dictionaryMap.get(key).getItemCode();
    }

    public static List<TDictionary> listByParentCode(String parentCode) {
        List<TDictionary> list = new ArrayList<TDictionary>();
        for (TDictionary dictionary : dictionaryMap.values()) {
            if (dictionary.getParentCode().equals(parentCode)) {
                list.add(dictionary);
            }
        }
        return list;
    }
}
