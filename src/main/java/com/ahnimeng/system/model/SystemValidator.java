package com.ahnimeng.system.model;

/**
 * Created by funcong on 2017-02-17.
 */
public interface SystemValidator {
    public interface UserCreate {}
    public interface UserUpdate {}
}
