package com.ahnimeng.system.model;
// Generated 2017-1-12 17:21:18 by Hibernate Tools 3.2.2.GA


import javax.persistence.*;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * TBusinessOrderGoods generated by hbm2java
 */
@Entity
@Table(name="t_business_order_goods"

)
public class TBusinessOrderGoods  implements java.io.Serializable {


     private Integer recId;
     private String orderSn;
     private int goodsId;
     private String goodsName;
     private String goodsSn;
     private int goodsNumber;
     private BigDecimal marketPrice;
     private BigDecimal goodsPrice;
     private String goodsAttr;
     private String goodsImg;
     private Integer sendNumber;
     private Integer isReal;
     private String extensionCode;
     private Integer parentId;
     private Integer isGift;
     private String goodsAttrId;
     private Integer goodsCreateUserId;

    public TBusinessOrderGoods() {
    }

	
    public TBusinessOrderGoods(String orderSn, int goodsId, int goodsNumber) {
        this.orderSn = orderSn;
        this.goodsId = goodsId;
        this.goodsNumber = goodsNumber;
    }
    public TBusinessOrderGoods(String orderSn, int goodsId, String goodsName, String goodsSn, int goodsNumber, BigDecimal marketPrice, BigDecimal goodsPrice, String goodsAttr, String goodsImg, Integer sendNumber, Integer isReal, String extensionCode, Integer parentId, Integer isGift, String goodsAttrId, Integer goodsCreateUserId) {
       this.orderSn = orderSn;
       this.goodsId = goodsId;
       this.goodsName = goodsName;
       this.goodsSn = goodsSn;
       this.goodsNumber = goodsNumber;
       this.marketPrice = marketPrice;
       this.goodsPrice = goodsPrice;
       this.goodsAttr = goodsAttr;
       this.goodsImg = goodsImg;
       this.sendNumber = sendNumber;
       this.isReal = isReal;
       this.extensionCode = extensionCode;
       this.parentId = parentId;
       this.isGift = isGift;
       this.goodsAttrId = goodsAttrId;
       this.goodsCreateUserId = goodsCreateUserId;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="rec_id", unique=true, nullable=false)
    public Integer getRecId() {
        return this.recId;
    }
    
    public void setRecId(Integer recId) {
        this.recId = recId;
    }
    
    @Column(name="order_sn", nullable=false, length=64)
    public String getOrderSn() {
        return this.orderSn;
    }
    
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    
    @Column(name="goods_id", nullable=false)
    public int getGoodsId() {
        return this.goodsId;
    }
    
    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }
    
    @Column(name="goods_name", length=120)
    public String getGoodsName() {
        return this.goodsName;
    }
    
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    
    @Column(name="goods_sn", length=60)
    public String getGoodsSn() {
        return this.goodsSn;
    }
    
    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }
    
    @Column(name="goods_number", nullable=false)
    public int getGoodsNumber() {
        return this.goodsNumber;
    }
    
    public void setGoodsNumber(int goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
    
    @Column(name="market_price", precision=10)
    public BigDecimal getMarketPrice() {
        return this.marketPrice;
    }
    
    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }
    
    @Column(name="goods_price", precision=10)
    public BigDecimal getGoodsPrice() {
        return this.goodsPrice;
    }
    
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
    
    @Column(name="goods_attr", length=65535)
    public String getGoodsAttr() {
        return this.goodsAttr;
    }
    
    public void setGoodsAttr(String goodsAttr) {
        this.goodsAttr = goodsAttr;
    }
    
    @Column(name="goods_img", length=65535)
    public String getGoodsImg() {
        return this.goodsImg;
    }
    
    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    
    @Column(name="send_number")
    public Integer getSendNumber() {
        return this.sendNumber;
    }
    
    public void setSendNumber(Integer sendNumber) {
        this.sendNumber = sendNumber;
    }
    
    @Column(name="is_real")
    public Integer getIsReal() {
        return this.isReal;
    }
    
    public void setIsReal(Integer isReal) {
        this.isReal = isReal;
    }
    
    @Column(name="extension_code", length=30)
    public String getExtensionCode() {
        return this.extensionCode;
    }
    
    public void setExtensionCode(String extensionCode) {
        this.extensionCode = extensionCode;
    }
    
    @Column(name="parent_id")
    public Integer getParentId() {
        return this.parentId;
    }
    
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
    
    @Column(name="is_gift")
    public Integer getIsGift() {
        return this.isGift;
    }
    
    public void setIsGift(Integer isGift) {
        this.isGift = isGift;
    }
    
    @Column(name="goods_attr_id")
    public String getGoodsAttrId() {
        return this.goodsAttrId;
    }
    
    public void setGoodsAttrId(String goodsAttrId) {
        this.goodsAttrId = goodsAttrId;
    }
    
    @Column(name="goods_create_user_id")
    public Integer getGoodsCreateUserId() {
        return this.goodsCreateUserId;
    }
    
    public void setGoodsCreateUserId(Integer goodsCreateUserId) {
        this.goodsCreateUserId = goodsCreateUserId;
    }




}


