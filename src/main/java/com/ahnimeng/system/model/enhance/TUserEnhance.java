package com.ahnimeng.system.model.enhance;

import com.ahnimeng.system.model.TAuthority;
import com.ahnimeng.system.model.TRole;
import com.ahnimeng.system.model.TUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TUserEnhance extends TUser {

    //判断是否为IOS访问
    private boolean IOS = false;
    private String requestType;
    private TRole role;
    private String realName;
    private String orgName;
    private Integer orgType;
    private List<TAuthority> auths = new ArrayList<TAuthority>(0);
    private List<TAuthority> urls = new ArrayList<TAuthority>(0);

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getOrgType() {
        return orgType;
    }

    public void setOrgType(Integer orgType) {
        this.orgType = orgType;
    }

    public List<TAuthority> getAuths() {
        return auths;
    }

    public void setAuths(List<TAuthority> auths) {
        this.auths = auths;
    }

    public TRole getRole() {
        return role;
    }

    public void setRole(TRole role) {
        this.role = role;
    }

    public List<TAuthority> getUrls() {
        return urls;
    }

    public void setUrls(List<TAuthority> urls) {
        this.urls = urls;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public boolean isIOS() {
        return IOS;
    }

    public void setIOS(boolean IOS) {
        this.IOS = IOS;
    }

    @Override
    public Date getCreateTime() {
        return super.getCreateTime() == null ? new Date() : super.getCreateTime();
    }
}
