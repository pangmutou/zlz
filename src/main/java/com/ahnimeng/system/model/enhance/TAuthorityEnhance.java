package com.ahnimeng.system.model.enhance;


import com.ahnimeng.system.model.TAuthority;

import java.util.ArrayList;
import java.util.List;

public class TAuthorityEnhance extends TAuthority {

    private Integer roleId;
    private List<TAuthority> childAuthority = new ArrayList<TAuthority>(0);

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public List<TAuthority> getChildAuthority() {
        return childAuthority;
    }

    public void setChildAuthority(List<TAuthority> childAuthority) {
        this.childAuthority = childAuthority;
    }
}


