package com.ahnimeng.system.interceptor;

import com.ahnimeng.common.util.*;
import com.ahnimeng.system.cache.impl.AuthCache;
import com.ahnimeng.system.model.enhance.TUserEnhance;
import com.ahnimeng.system.util.HttpSessionUtils;
import com.ahnimeng.system.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class WebCommonInterceptor extends HandlerInterceptorAdapter {

    protected static final Logger LOGGER = LoggerFactory.getLogger(WebCommonInterceptor.class);
    //用来分割参数的符号，有 逗号、空格、分号、换行 这几种
    private final String URL_SPLIT_PATTERN = "[, ;\r\n]";
    //开启是否验证权限 是否修改试图返回页面
    private boolean validationAuthority = false, modifyView = false;
    private String whiteList, blackList, resources, noSession;

    // 用spring的Ant过滤的通配符似乎不太适合自己的写法
    // private final PathMatcher pathMatcher = new AntPathMatcher();
    private List<Pattern> whitePatternList = new ArrayList<Pattern>(), blackPatternList = new ArrayList<Pattern>(), resourcesPatternList = new ArrayList<Pattern>();

    public void setBlackList(String blackList) {
        convertToPatternList(blackList, this.blackPatternList);
    }

    public void setWhiteList(String whiteList) {
        convertToPatternList(whiteList, this.whitePatternList);
    }

    public void setResources(String resources) {
        convertToPatternList(resources, this.resourcesPatternList);
    }

    public String getNoSession() {
        return noSession == null ? "" : noSession;
    }

    public void setNoSession(String noSession) {
        this.noSession = noSession;
    }

    public void setValidationAuthority(boolean validationAuthority) {
        this.validationAuthority = validationAuthority;
    }

    /////////////////////////权限验证开始///////////////////////////////////////

    public void setModifyView(boolean modifyView) {
        this.modifyView = modifyView;
    }

    /**
     * 功能描述:拦截UIL  权限过滤
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     * @author yangliu  2013-8-31 上午10:57:25
     */

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        // TODO Session拦截器
        if (validationAuthority) {
            String currentServlet = request.getServletPath();
            boolean inWhite = false, inBlack = false, isResources = false;

            for (Pattern pattern : resourcesPatternList) {
                isResources = pattern.matcher(currentServlet).matches();
                if (isResources) {
//                    System.out.println(String.format("Servlet:%s %s %s", request.getRequestURL(), request.getMethod(), "catch"));
                    return true;
                }
            }

            //非静态资源记录日志
            LOGGER.debug(String.format("Servlet:%s %s %s", request.getRequestURL(), request.getMethod(), "catch"));

            for (Pattern pattern : whitePatternList) {
                inWhite = pattern.matcher(currentServlet).matches();
                if (inWhite)
                    break;
            }

            //暂时使用白名单机制
//            for (Pattern pattern : blackPatternList) {
//                inBlack = pattern.matcher(currentServlet).matches();
//                if (inBlack) {
//                    break;
//                }
//            }

//            if (!inWhite && inBlack) {
            if (!inWhite) {
                TUserEnhance userObj = HttpSessionUtils.getCurrentUser(request.getSession());
                if (userObj != null) {
                    String url = request.getServletPath();
                    if (!"/".equals(url)) {
                        String authCodesStr = AuthCache.getAuthCodeByUrl(url);
                        if (EmptyUtils.isEmpty(authCodesStr) || SecurityUtils.isAnyGranted(authCodesStr, userObj.getUrls()) || "A_1021".equals(authCodesStr) || "A_1035".equals(authCodesStr)) {
                            return super.preHandle(request, response, handler);
                        } else {
                            LOGGER.debug(String.format("Servlet:%s %s %s", request.getRequestURL(), request.getMethod(), "reLogin : no authority"));
                        }
                    }
                } else {
                    LOGGER.debug(String.format("Servlet:%s %s %s", request.getRequestURL(), request.getMethod(), "reLogin : no session"));
                }
                response.getWriter().print(
                        "<script>top.window.location.href = \""
                                + request.getContextPath() + getNoSession()
                                + "\";</script>");
                return false;
            }
        }
        return super.preHandle(request, response, handler);
    }

    /**
     * 将过滤字符串列表转成pattern
     *
     * @param urlArrayString
     * @param patternList
     * @return
     */
    private List<Pattern> convertToPatternList(String urlArrayString, List<Pattern> patternList) {
        if (urlArrayString != null) {
            String[] urlArray = urlArrayString.split(URL_SPLIT_PATTERN);
            for (String url : urlArray) {
                url = url.trim();
                if (url.length() == 0) {
                    continue;
                }
                patternList.add(ExpressionToRegex.getRegexPattern(url));
            }
        }
        return patternList;
    }
    /////////////////////////权限验证结束///////////////////////////////////////

    /////////////////////////修改页面跳转路径开始///////////////////////////////////////

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            HttpSession session = request.getSession(false);
            TUserEnhance userObj = null;
            if (session != null) {
                userObj = HttpSessionUtils.getCurrentUser(request.getSession());
            }
            String requestType = "";
            if (userObj == null) {
                String agent = request.getHeader("User-Agent") == null ? "" : request.getHeader("User-Agent");
                requestType = RequestUtils.isMobileRequest(agent) ? SysCode.REQUEST_TYPE_MOBILE : SysCode.REQUEST_TYPE_PC;
            } else {
                requestType = userObj.getRequestType();
            }
            String viewName = modelAndView.getViewName();
            String urlPath = "";
            if (modifyView) {
                if (SysCode.REQUEST_TYPE_MOBILE.equals(requestType)) {
                    urlPath = PropertiesUtils.URLPage.getValue(viewName + "_mobile");
                }
                if (EmptyUtils.isEmpty(urlPath)) {
                    urlPath = PropertiesUtils.URLPage.getValue(viewName);
                }
            }
            if (EmptyUtils.isEmpty(urlPath))
                urlPath = viewName;
            modelAndView.setViewName(urlPath);
        }
        super.postHandle(request, response, handler, modelAndView);
    }
    /////////////////////////修改页面跳转路径结束///////////////////////////////////////
}
