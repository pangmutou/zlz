package com.ahnimeng.system.util;

import com.ahnimeng.common.util.Constants;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.model.enhance.TUserEnhance;

import javax.servlet.http.HttpSession;

public class HttpSessionUtils {

    /**
     * 功能描述:把user对象保存在 session中
     *
     * @param session session对象
     * @param user    user对象
     * @author yangliu  2013-9-11 下午03:09:01
     */
    public static void setUserToSession(HttpSession session, TUser user) {
        session.setAttribute(Constants.SESSION_KEY_USERINFO, user);
    }

    /**
     * 功能描述:把对象
     *
     * @param session
     * @param key
     * @param obj
     * @author yangliu  2013-9-14 上午09:01:09
     */
    public static void setObjectToSession(HttpSession session, String key, Object obj) {
        session.setAttribute(key, obj);
    }

    /**
     * 功能描述: 获取当前用户信息
     *
     * @param session session
     * @return
     * @author yangliu  2013-9-11 下午03:13:14
     */
    public static TUserEnhance getCurrentUser(HttpSession session) {
        return (TUserEnhance) session.getAttribute(Constants.SESSION_KEY_USERINFO);
    }

    /**
     * 功能描述:获取session中的值
     *
     * @param session session
     * @param key     关键字
     * @return
     * @author yangliu  2013-9-11 下午03:17:07
     */
    public static TUser getSessionValue(HttpSession session, String key) {
        return (TUser) session.getAttribute(key);
    }

    /**
     * 功能描述: 删除session中的用户信息
     *
     * @param session session
     * @author yangliu  2013-9-11 下午03:15:04
     */
    public static void removeSessionUser(HttpSession session) {
        session.removeAttribute(Constants.SESSION_KEY_USERINFO);
    }

}
