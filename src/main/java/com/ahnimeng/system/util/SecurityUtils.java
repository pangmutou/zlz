package com.ahnimeng.system.util;

import com.ahnimeng.common.util.EmptyUtils;
import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.system.model.TAuthority;
import com.ahnimeng.system.model.enhance.TUserEnhance;

import java.util.Iterator;
import java.util.List;

/**
 * #(c) ahnimeng shouhou <br/>
 * <p>
 * 版本说明: $id:$ <br/>
 * <p>
 * 功能说明: 权限帮助类
 * <p>
 * <br/>创建说明: 2013-9-14 下午02:02:39 yangliu  创建文件<br/>
 * <p>
 * 修改历史:<br/>
 */
public final class SecurityUtils {

    /**
     * 功能描述: 用户是否拥有权限
     *
     * @param authCode 权限Code
     * @param user     用户信息
     * @return
     * @author yangliu  2013-9-14 下午01:52:54
     */
    public static boolean isGranted(String authCode, TUserEnhance user) {
        return isGranted(authCode, user.getAuths());
    }

    /**
     * 功能描述: 某些权限是否都在 权限列表中  全部都包含 才会还回true
     *
     * @param authCodesStr 权限字符串 (A_100,A_1001)
     * @param user         用户信息
     * @return
     * @author yangliu  2013-9-14 下午02:01:23
     */
    public static boolean isAllGranted(String authCodesStr, TUserEnhance user) {
        if (EmptyUtils.isNotEmpty(authCodesStr) && EmptyUtils.isNotEmpty(user)) {
            String[] authCodes = authCodesStr.split(SysCode.COMMA);
            return isAllGranted(authCodes, user.getAuths());
        }
        return false;
    }

    /**
     * 功能描述: 某些权限是否都在 权限列表中 全部都包含 才会还回true
     *
     * @param authCodes 权限数组
     * @param user      用户信息
     * @return
     * @author yangliu  2013-9-14 下午02:04:42
     */
    public static boolean isAllGranted(String[] authCodes, TUserEnhance user) {
        if (EmptyUtils.isNotEmpty(authCodes) && EmptyUtils.isNotEmpty(user)) {
            return isAllGranted(authCodes, user.getAuths());
        }
        return false;
    }

    /**
     * 功能描述:某些权限是否在权限列表中,有一个就会还回true
     *
     * @param authCodes 权限codes 数组
     * @param user      用户信息
     * @return
     * @author yangliu  2013-9-14 下午02:08:38
     */
    public static boolean isAnyGranted(String[] authCodes, TUserEnhance user) {
        if (EmptyUtils.isNotEmpty(authCodes) && EmptyUtils.isNotEmpty(user)) {
            return isAnyGranted(authCodes, user.getAuths());
        }
        return false;
    }

    /**
     * 功能描述:某些权限是否在权限列表中，有一个就会还回true
     *
     * @param authCodesStr 权限codes （A_001,A_002）
     * @param user      用户信息
     * @return
     * @author yangliu  2013-9-14 下午02:08:38
     */
    public static boolean isAnyGranted(String authCodesStr, TUserEnhance user) {
        if (EmptyUtils.isNotEmpty(authCodesStr) && EmptyUtils.isNotEmpty(user)) {
            return isAnyGranted(authCodesStr, user.getAuths());
        }
        return false;
    }

    /**
     * 功能描述: 某个权限是否在权限列表中
     *
     * @param authCode 权限Code
     * @param authList 权限列表
     * @return
     * @author yangliu  2013-9-14 下午01:54:18
     */
    public static boolean isGranted(String authCode, List<TAuthority> authList) {
        if (EmptyUtils.isNotEmpty(authCode) && EmptyUtils.isNotEmpty(authList)) {
            Iterator<TAuthority> it = authList.iterator();
            while (it.hasNext()) {
                if (authCode.equals(it.next().getAuthCode()))
                    return true;
            }
        }
        return false;
    }

    /**
     * 功能描述: 某个权限是否在权限列表中
     *
     * @param authCode 权限Code
     * @param currentUser
     * @return
     * @author yangliu  2013-9-14 下午01:54:18
     */
    public static boolean isNotGranted(String authCode, TUserEnhance currentUser) {
        return isNotGranted(authCode, currentUser.getAuths());
    }

    /**
     * 功能描述: 某个权限是否在权限列表中
     *
     * @param authCode 权限Code
     * @param authList 权限列表
     * @return
     * @author yangliu  2013-9-14 下午01:54:18
     */
    public static boolean isNotGranted(String authCode, List<TAuthority> authList) {
        return !isGranted(authCode, authList);
    }

    /**
     * 功能描述: 某些权限都不在 权限列表中  全部不在列表返回true，有一个在列表权限中还回false
     *
     * @param authCodesStr 权限字符串 (A_100,A_1001)
     * @param authList     权限列表
     * @return
     * @author yangliu  2013-9-14 下午02:01:23
     */
    public static boolean isNotAllGranted(String authCodesStr, List<TAuthority> authList) {
        return !isAnyGranted(authCodesStr, authList);
    }

    /**
     * 功能描述: 某些权限都不在 权限列表中  全部不在列表返回true，有一个在列表权限中还回false
     *
     * @param authCodesStr 权限字符串 (A_100,A_1001)
     * @param currentUser
     * @return
     * @author yangliu  2013-9-14 下午02:01:23
     */
    public static boolean isNotAllGranted(String authCodesStr, TUserEnhance currentUser) {
        return isNotAllGranted(authCodesStr, currentUser.getAuths());
    }

    /**
     * 功能描述: 某些权限是否都在 权限列表中  全部都包含 才会还回true
     *
     * @param authCodesStr 权限字符串 (A_100,A_1001)
     * @param authList     权限列表
     * @return
     * @author yangliu  2013-9-14 下午02:01:23
     */
    public static boolean isAllGranted(String authCodesStr, List<TAuthority> authList) {
        if (EmptyUtils.isNotEmpty(authCodesStr) && EmptyUtils.isNotEmpty(authList)) {
            String[] authCodes = authCodesStr.split(SysCode.COMMA);
            return isAllGranted(authCodes, authList);
        }
        return false;
    }

    /**
     * 功能描述: 某些权限是否都在 权限列表中 全部都包含 才会还回true
     *
     * @param authCodes 权限数组
     * @param authList  权限列表
     * @return
     * @author yangliu  2013-9-14 下午02:04:42
     */
    public static boolean isAllGranted(String[] authCodes, List<TAuthority> authList) {
        if (EmptyUtils.isNotEmpty(authCodes) && EmptyUtils.isNotEmpty(authList)) {
            for (String authCode : authCodes) {
                if (isNotGranted(authCode, authList)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 功能描述:某些权限是否在权限列表中,有一个就会还回true
     *
     * @param authCodes 权限codes 数组
     * @param authList  权限列表
     * @return
     * @author yangliu  2013-9-14 下午02:08:38
     */
    public static boolean isAnyGranted(String[] authCodes, List<TAuthority> authList) {
        if (EmptyUtils.isNotEmpty(authCodes) && EmptyUtils.isNotEmpty(authList)) {
            for (String authCode : authCodes) {
                if (isGranted(authCode, authList)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 功能描述:某些权限是否在权限列表中，有一个就会还回true
     *
     * @param authCodesStr 权限codes （A_001,A_002）
     * @param authList  权限列表
     * @return
     * @author yangliu  2013-9-14 下午02:08:38
     */
    public static boolean isAnyGranted(String authCodesStr, List<TAuthority> authList) {
        if (EmptyUtils.isNotEmpty(authCodesStr) && EmptyUtils.isNotEmpty(authList)) {
            String[] authCodes = authCodesStr.split(SysCode.COMMA);
            return isAnyGranted(authCodes, authList);
        }
        return false;
    }


}
