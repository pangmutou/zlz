package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TUserAddress;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAddressService extends BaseServiceImpl<TUserAddress> {

    public TUserAddress get(TUserAddress userAddress) {
        TUserAddress source = baseDaoPlus.get(userAddress);
        if (source == null) {
            throw new BusinessLogicException("收货地址不正确");
        }
        return source;
    }

    public List list(TUserAddress userAddress, Page page) {
        return baseDaoPlus.list(userAddress, page);
    }

    @Override
    public TUserAddress save(TUserAddress userAddress) {
        TUserAddress target = new TUserAddress();
        target.setUserId(userAddress.getUserId());
        Long existsCount = baseDaoPlus.count(target);

        if (existsCount >= 20) {
            throw new BusinessLogicException("收货地址不能超出20个");
        }

        if (userAddress.getIsDefault() == 1) {
            cancelDefault(userAddress.getUserId());
        }

        return baseDaoPlus.save(userAddress);
    }

    @Override
    public TUserAddress update(TUserAddress userAddress) {
        TUserAddress source = new TUserAddress();
        source.setUserId(userAddress.getUserId());
        source.setAddressId(userAddress.getAddressId());
        source = get(userAddress);

        if (userAddress.getIsDefault() == 1) {
            cancelDefault(userAddress.getUserId());
        }

        BeanUtils.copyProperties(userAddress, source, new String[]{
                "addressId",
                "userId"
        });
        return baseDaoPlus.update(userAddress);
    }

    public void cancelDefault(int userId) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("UPDATE t_user_address ");
        sql.append("SET is_default = 0 ");
        sql.append("WHERE user_id = :userId ", userId);
        baseDaoPlus.saveOrUpdate(sql.toString(), sql.getParam());
    }
}
