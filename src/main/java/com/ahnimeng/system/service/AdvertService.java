package com.ahnimeng.system.service;

import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TAdvert;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by funcong on 2016/2/4.
 */
@Service
public class AdvertService extends BaseServiceImpl<TAdvert> {

    public List page(Page page, TAdvert advert) {
        return baseDaoPlus.list(advert, page);
    }

    @Override
    public TAdvert save(TAdvert x) {
        x.setCreateTime(new Date());
        return super.save(x);
    }
}
