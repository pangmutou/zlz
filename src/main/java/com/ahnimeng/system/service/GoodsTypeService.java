package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TGoodsType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsTypeService extends BaseServiceImpl<TGoodsType> {
    @Autowired
    BaseDaoImpl baseDaoPlus;

    public List list(TGoodsType goodsType, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append(" gt.type_id    AS typeId, ");
        sql.append(" gt.type_name  AS typeName,  ");
        sql.append(" gt.enabled    AS enabled, ");
        sql.append(" gt.attr_group AS attrGroup ");
        sql.append(" FROM t_goods_type gt ");
        sql.append(" WHERE 1=1 ");
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

    public List list(TGoodsType goodsType) {
        List list = baseDaoPlus.list(goodsType);
        return list;
    }

    @Override
    public TGoodsType save(TGoodsType goodsType) {

        return super.save(goodsType);
    }

    @Override
    public TGoodsType update(TGoodsType goodsType) {
        return super.update(goodsType);
    }

    @Override
    public int delete(TGoodsType goodsType) {
        return super.delete(goodsType);
    }
}
