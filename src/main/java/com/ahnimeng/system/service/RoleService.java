package com.ahnimeng.system.service;


import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.EmptyUtils;
import com.ahnimeng.system.model.TRole;
import com.ahnimeng.system.model.TUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("roleService")
public class RoleService extends BaseServiceImpl<TRole> {

    @Autowired
    @Qualifier("roleAuthorityService")
    private RoleAuthorityService roleAuthorityService;
    @Autowired
    @Qualifier("operationLogService")
    private OperationLogService operationLogService;

    public Map get(TRole role, Integer userId) {
        List list = list(role, userId, null);
        if (list.size() == 0) {
            throw new BusinessLogicException("没有获取到该角色");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("意外的获取到多个符合的角色");
        } else {
            return (Map) list.get(0);
        }
    }

    /**
     * 功能描述:获取用户拥有的角色
     *
     * @param userId 用户ID
     * @return 角色
     * @author yangliu  2013-9-12 下午05:45:33
     */
    public TRole getRoleListByUserId(Integer userId) {
        TUser user = new TUser();
        user.setUserId(userId);
        user = baseDaoPlus.get(user);
        TRole role = new TRole();
        role.setRoleId(user.getRoleId());
        return baseDaoPlus.get(role);
    }

    public List list(TRole role, Page page) {
        return baseDaoPlus.list(role, page);
    }

    public List list(TRole role, Integer userId) {
        return list(role, userId, null);
    }

    public List list(TRole role, Integer userId, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tr.role_id   AS roleId, ");
        sql.append("  tr.role_name AS roleName, ");
        sql.append("  tr.orderby   AS orderby, ");
        sql.append("  tr.role_type AS roleType ");
        sql.append("FROM t_role tr ");
        sql.append("  LEFT JOIN t_user tu ON tr.role_id = tu.role_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND tr.role_id = :role_id ", role.getRoleId());
        sql.append("      AND tr.role_type & :role_type ", role.getRoleType());
        sql.append("      AND tu.user_id = :user_id ", userId);
        return baseDaoPlus.list(sql, page);
    }

    /**
     * 功能描述:
     *
     * @param role
     * @param authCodesStr
     * @author yangliu  2013-9-25 下午01:46:37
     */
    public Integer saveOrUpdateRole(TRole role, String authCodesStr, TUser user) {

        if (EmptyUtils.isNotEmpty(role)) {
            super.saveOrUpdate(role);
            roleAuthorityService.save(role.getRoleId().toString(), authCodesStr, user);
            String content = null;
            if (role.getRoleId() == null) {
                content = "添加角色操作";
            } else {
                content = "修改角色操作";
            }
            operationLogService.addOperationLogThead(user, "角色管理", content, Constants.OPERA_TYPE_ROLE);
            return 1;
        }
        return -1;
    }

    public void deleteRole(Integer roleId, TUser user) {
        TRole role = new TRole();
        role.setRoleId(roleId);
        role = baseDaoPlus.get(role);
        baseDaoPlus.delete(role);
        operationLogService.addOperationLogThead(user, "角色管理", "删除角色操作", Constants.OPERA_TYPE_ROLE);
    }

    public List<TRole> getAll(TRole role) {
        return list(role, null, null);
    }

}
