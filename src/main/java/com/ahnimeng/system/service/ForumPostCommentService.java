package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TForumPost;
import com.ahnimeng.system.model.TForumPostComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by funcong on 2016/10/14.
 */
@Service
public class ForumPostCommentService extends BaseServiceImpl<TForumPostComment> {

    @Autowired
    private ForumPostService postService;

    public TForumPostComment save(TForumPostComment comment) {
        TForumPost post = new TForumPost();
        post.setPostId(comment.getPostId());
        post = postService.get(post);

        comment = new TForumPostComment(
                comment.getPostId(),
                comment.getUserId(),
                comment.getContent(),
                comment.getImage(),
                1,
                new Date()
        );
        return baseDaoPlus.save(comment);
    }

    public List<Map> list(TForumPostComment comment, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append(" tfpc.comment_id     AS commentId, ");
        sql.append(" tfpc.post_id        AS postId, ");
        sql.append(" tfpc.user_id        AS userId, ");
        sql.append(" tfpc.content        AS content, ");
        sql.append(" tfpc.image          AS image, ");
        sql.append(" tfpc.is_show        AS isShow, ");
        sql.append(" tfpc.create_time    AS createTime, ");
        sql.append(" tui.real_name       AS realName, ");
        sql.append(" tui.nickname        AS nickname, ");
        sql.append(" tui.mobile          AS mobile, ");
        sql.append(" tui.photo           AS photo ");
        sql.append(" FROM t_forum_post_comment tfpc ");
        sql.append("    LEFT JOIN t_user_info tui ON tui.user_id = tfpc.user_id ");
        sql.append(" WHERE 1 = 1 ");
        sql.append("    AND tfpc.comment_id = :comment_id ", comment.getCommentId());
        sql.append("    AND tfpc.post_id = :post_id ", comment.getPostId());
        sql.append("    AND tfpc.user_id = :user_id ", comment.getUserId());
        sql.append("    AND tfpc.content = :content ", comment.getContent());
        sql.append("    AND tfpc.image = :image ", comment.getIsShow());
        sql.append("    AND tfpc.is_show = :is_show ", comment.getCreateTime());
        sql.append(" ORDER BY tfpc.comment_id ");
        return baseDaoPlus.list(sql, page);
    }

    public TForumPostComment get(TForumPostComment comment) {
        Page page = new Page();
        page.setPageSize(2);
        List list = list(comment, page);
        if (list.size() < 1) {
            throw new BusinessLogicException("没有该条需求记录");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("错误的筛选条件，匹配到多条记录");
        }
        return baseDaoPlus.get(comment);
    }

}
