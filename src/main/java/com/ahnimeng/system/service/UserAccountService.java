package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.model.TUserAccount;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by funcong-tp on 2016-07-19.
 */
@Service
public class UserAccountService extends BaseServiceImpl<TUserAccount> {
    public TUserAccount save(TUser user) {
        TUserAccount userAccount = new TUserAccount(
                user.getUserId(),
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                new Date(),
                new Date()
        );
        return super.save(userAccount);
    }

    /**
     * 获取所有尚有余额的用户
     *
     * @return
     */
    public List<TUserAccount> listByHasMoney() {
        return list(TUserAccount.class, new Criterion[]{Restrictions.gt("money", BigDecimal.ZERO)}, new Order[]{Order.asc("userId")}, null, null);
    }

    public List list(Page page, TUserAccount userAccount) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  uc.user_id           as userId, ");
        sql.append("  uc.money             as money, ");
        sql.append("  uc.freezing_money    as freezingMoney, ");
        sql.append("  uc.create_time       as createTime, ");
        sql.append("  uc.update_time       as updateTime ");
        sql.append("  from t_user_account uc ");
        sql.append("  where 1=1 ");
        sql.append("  and uc.user_id = :userId ", userAccount.getUserId());
        sql.append("  and uc.money = :money ", userAccount.getMoney());
        sql.append("  and uc.freezing_money = :freezingMoney ", userAccount.getFreezingMoney());
        sql.append("  and uc.create_time = :createTime ", userAccount.getCreateTime());
        sql.append("  and uc.update_time = :updateTime ", userAccount.getUpdateTime());
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

    public Map getMap(TUserAccount userAccount) {
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(page, userAccount);
        if (list.size() < 1) {
            throw new BusinessLogicException("没有该条需求记录");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("错误的筛选条件，匹配到多条记录");
        }
        return list.get(0);
    }

    public TUserAccount get(TUserAccount userAccount) {
        Map account = getMap(userAccount);
        TUserAccount source = new TUserAccount();
        source.setUserId(Integer.parseInt(account.get("userId").toString()));
        return baseDaoPlus.get(source);
    }
}
