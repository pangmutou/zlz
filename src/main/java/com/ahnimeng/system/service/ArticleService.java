package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TArticle;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by zhangxiansen on 2016/4/8.
 */
@Service
public class ArticleService extends BaseServiceImpl<TArticle> {
    @Autowired
    BaseDaoImpl baseDaoPlus;

    public List list(Page page, TArticle article) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  ta.article_id     AS articleId, ");
        sql.append("  ta.cat_id         AS catId, ");
        sql.append("  ta.title          AS title, ");
        sql.append("  ta.content        AS content, ");
        sql.append("  ta.author         AS author, ");
        sql.append("  ta.author_email   AS authorEmail, ");
        sql.append("  ta.keywords       AS keywords, ");
        sql.append("  ta.article_type   AS articleType, ");
        sql.append("  ta.is_open        AS isOpen, ");
        sql.append("  ta.add_time       AS addTime, ");
        sql.append("  ta.file_url       AS fileUrl, ");
        sql.append("  ta.open_type      AS openType, ");
        sql.append("  ta.link           AS link, ");
        sql.append("  ta.description    AS description, ");
        sql.append("  ta.create_user_id AS createUserId, ");
        sql.append("  ta.org_id         AS orgId, ");
        sql.append("  ta.community_id   AS communityId, ");
        sql.append("  ta.is_delete      AS isDelete, ");
        //小区
        sql.append("  tc.community_name   AS communityName ");

        sql.append("FROM t_article ta ");
        sql.append(" LEFT JOIN t_community tc ON tc.community_id = ta.community_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND ta.title LIKE :title ", StringUtils.isEmpty(article.getTitle()) ? null : "%" + article.getTitle() + "%");
        sql.append("      AND ta.cat_id = :catId ", article.getCatId());
        sql.append("      AND tc.org_id = :org_id ", article.getOrgId());
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

    public List list(TArticle article) {
        List list = list(null, article);
        return list;
    }

    @Override
    public TArticle save(TArticle article) {
        article.setCreateTime(new Date());
        return super.save(article);
    }

    @Override
    public TArticle update(TArticle article) {
        TArticle source = this.get(TArticle.class, article.getArticleId());
        source.setTitle(article.getTitle());
        source.setContent(article.getContent());
        source.setCatId(article.getCatId());
        source.setArticleType(article.getArticleType());
        return super.update(source);
    }

    @Override
    public int delete(TArticle article) {
        TArticle tArticle = this.get(TArticle.class, article.getArticleId());
        return super.delete(tArticle);
    }
}
