package com.ahnimeng.system.service;

import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.transaction.alipay.AliPayHandler;
import com.ahnimeng.common.transaction.alipay.model.AlipayOrderInfo;
import com.ahnimeng.system.model.TOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Created by funcong-tp on 2016-07-03.
 */
@Service
public class TransactionService extends AliPayHandler {

    @Transactional
    @Override
    public Object paySuccessResponse(Map map) {
        //request获取的parameterMap是Map<String, String[]>类型
        Map<String, String> parameterMap = super.getParameterMap(map);
        AlipayOrderInfo alipayOrderInfo = new AlipayOrderInfo();
        try {
            org.apache.commons.beanutils.BeanUtils.populate(alipayOrderInfo, parameterMap);
        } catch (Exception e) {
            throw new BusinessLogicException("支付宝异步通知参数解析出错。");
        }

//        if (alipayOrderInfo.getOut_trade_no().substring(6, 8).equals("04")) {
//            //04 为水、电、煤、物业缴费订单，在不同的表中
//            TFeeOrder feeOrder = new TFeeOrder();
//            feeOrder.setOrderSn(alipayOrderInfo.getOut_trade_no());
//            feeOrderService.paid(feeOrder);
//        } else {
//            TOrder order = getOrder(map);
//            //进行确认付款操作
//            orderService.paid(order);
//        }
        return super.paySuccessResponse(map);
    }

    @Override
    public Object finishResponse(Map map) {
        TOrder order = getOrder(map);
        //交易成功且结束，即不可再做任何操作。
        return super.finishResponse(map);
    }

    private TOrder getOrder(Map map) {
        //request获取的parameterMap是Map<String, String[]>类型
        Map<String, String> parameterMap = super.getParameterMap(map);
        AlipayOrderInfo alipayOrderInfo = new AlipayOrderInfo();
        try {
            org.apache.commons.beanutils.BeanUtils.populate(alipayOrderInfo, parameterMap);
        } catch (Exception e) {
            throw new BusinessLogicException("支付宝异步通知参数解析出错。");
        }

        //获取关联的订单
        TOrder order = new TOrder();
//        order.setOrderSn(alipayOrderInfo.getOut_trade_no());
//        order = orderService.get(order);
//
//        if (order == null) {
//            throw new BusinessLogicException("支付宝异步通知参数中订单号未能找到对应订单,单号 " + alipayOrderInfo.getOut_trade_no());
//        }
//        if (alipayOrderInfo.getSeller_id().equals(AlipayConfig.seller_id)) {
//            throw new BusinessLogicException("支付宝异步通知参数中的卖家ID与服务器中配置的不同,单号 " + alipayOrderInfo.getOut_trade_no());
//        }
//        if (order.getOrderAmount().compareTo(new BigDecimal(alipayOrderInfo.getTotal_fee())) != 0) {
//            throw new BusinessLogicException("支付宝异步通知参数中的金额与订单实际金额不同,单号 " + alipayOrderInfo.getOut_trade_no());
//        }
        return order;
    }

    @Override
    public Object batchTransNotifyRequest(Map map) {
        return super.batchTransNotifyRequest(map);
    }
}
