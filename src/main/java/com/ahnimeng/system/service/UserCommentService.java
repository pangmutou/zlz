package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TBusinessOrder;
import com.ahnimeng.system.model.TUserComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by funcong on 2017-01-02.
 */
@Service
public class UserCommentService extends BaseServiceImpl<TUserComment> {

    @Autowired
    BusinessOrderService businessOrderService;

    public List<Map> listMap(TUserComment comment, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tuc.user_comment_id AS userCommentId, ");
        sql.append("  tuc.comment_type    AS commentType, ");
        sql.append("  tuc.order_sn        AS orderSn, ");
        sql.append("  tuc.user_id         AS userId, ");
        sql.append("  tuc.target_user_id  AS targetUserId, ");
        sql.append("  tuc.STATUS          AS status, ");
        sql.append("  tuc.total_source    AS totalSource, ");
        sql.append("  tuc.time_source     AS timeSource, ");
        sql.append("  tuc.quality_source  AS qualitySource, ");
        sql.append("  tuc.attitude_source AS attitudeSource, ");
        sql.append("  tuc.create_time     AS createTime, ");
        sql.append("  tuc.content         AS content, ");
        sql.append("  source.real_name    AS realName, ");
        sql.append("  source.photo        AS photo ");
        sql.append("FROM t_user_comment tuc ");
        sql.append("  LEFT JOIN t_user_info source ON source.user_id = tuc.user_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND tuc.user_comment_id = :user_comment_id ", comment.getUserCommentId());
        sql.append("      AND tuc.comment_type = :comment_type ", comment.getCommentType());
        sql.append("      AND tuc.order_sn = :order_sn ", comment.getOrderSn());
        sql.append("      AND tuc.user_id = :user_id ", comment.getUserId());
        sql.append("      AND tuc.target_user_id = :target_user_id ", comment.getTargetUserId());
        sql.append("      AND tuc.STATUS = :STATUS ", comment.getStatus());
        sql.append("      AND tuc.total_source = :total_source ", comment.getTotalSource());
        sql.append("      AND tuc.time_source = :time_source ", comment.getTimeSource());
        sql.append("      AND tuc.quality_source = :quality_source ", comment.getQualitySource());
        sql.append("      AND tuc.attitude_source = :attitude_source ", comment.getAttitudeSource());
        sql.append("      AND tuc.create_time = :create_time ", comment.getCreateTime());
        sql.append("      AND tuc.content = :content ", comment.getContent());
        return baseDaoPlus.list(sql, page);
    }

    @Override
    public TUserComment save(TUserComment userComment) {
        TBusinessOrder businessOrder = new TBusinessOrder();
        businessOrder.setOrderSn(userComment.getOrderSn());
        businessOrder = businessOrderService.get(businessOrder);

        if (businessOrder.getBuyerUserId().compareTo(userComment.getUserId()) != 0) {
            throw new BusinessLogicException("只能评价自己的订单");
        }
        if (businessOrder.getBuyerHasComment().compareTo(0) != 0) {
            throw new BusinessLogicException("该订单已经评价过");
        }
        if (businessOrder.getIsFinish().compareTo(1) != 0) {
            throw new BusinessLogicException("该订单还没有完结，无法评价");
        }

        businessOrder.setBuyerHasComment(1);
        baseDaoPlus.update(businessOrder);
        userComment = new TUserComment(
                0,
                userComment.getOrderSn(),
                userComment.getUserId(),
                businessOrder.getSellerUserId(),
                1,
                0,
                0,
                0,
                0,
                new Date(),
                userComment.getContent()
        );
        return super.save(userComment);
    }
}
