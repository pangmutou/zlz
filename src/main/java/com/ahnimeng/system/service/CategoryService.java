package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品分类表的操作
 * Created by zhangxiansen on 2016/3/26.
 */
@Service
public class CategoryService extends BaseServiceImpl<TCategory> {

    @Autowired
    BaseDaoImpl baseDaoPlus;

    public Page page(Page page, TCategory category) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append(" ca.cat_id as catId, ");
        sql.append(" ca.cat_name as catName ");
        sql.append(" FROM ");
        sql.append(" t_category ca ");
        sql.append(" where 1=1");
        sql.append(" and ca.cat_id=:cat_id ", category.getCatId() == null ? null : category.getCatId());
        List list = baseDaoPlus.list(sql, page);
        if (page == null) {
            page = new Page();
        }
        page.setResult(list);
        return page;
    }

    public List list(TCategory category, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  cat_id        AS catId, ");
        sql.append("  cat_name      AS catName, ");
        sql.append("  keywords      AS keywords, ");
        sql.append("  cat_desc      AS catDesc, ");
        sql.append("  parent_id     AS parentId, ");
        sql.append("  sort_order    AS sortOrder, ");
        sql.append("  template_file AS templateFile, ");
        sql.append("  measure_unit  AS measureUnit, ");
        sql.append("  show_in_nav   AS showInNav, ");
        sql.append("  style         AS style, ");
        sql.append("  is_show       AS isShow, ");
        sql.append("  grade         AS grade, ");
        sql.append("  filter_attr   AS filterAttr ");
        sql.append("FROM t_category ");
        return baseDaoPlus.list(sql, page);
    }

    public List list(TCategory category) {
        return list(category, null);
    }

    public TCategory add(TCategory category) {
        return super.save(category);
    }

    @Override
    public TCategory update(TCategory category) {
        return super.update(category);
    }

}
