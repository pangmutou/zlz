package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.EmptyUtils;
import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.common.util.TimeUtil;
import com.ahnimeng.system.model.TAuthority;
import com.ahnimeng.system.model.TRoleAuthority;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.model.enhance.TAuthorityEnhance;
import org.hibernate.SQLQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AuthorityService extends BaseServiceImpl<TAuthority> {

    @Autowired
    @Qualifier("roleAuthorityService")
    private RoleAuthorityService roleAuthorityService;
    @Autowired
    @Qualifier("operationLogService")
    private OperationLogService operationLogService;

    public static Object mapToObject(Map map, Class<?> beanClass) throws Exception {
        if (map == null)
            return null;

        Object obj = beanClass.newInstance();

        org.apache.commons.beanutils.BeanUtils.populate(obj, map);

        return obj;
    }

    /**
     * 功能描述:修改权限
     *
     * @param auth 权限对象
     * @return
     * @author yangliu  2013-9-24 上午09:52:00
     */
    public TAuthority update(TAuthority auth, TUser user) {
        TAuthority oldAuth = get(TAuthority.class, auth.getAuthId());
        BeanUtils.copyProperties(auth, oldAuth, new String[]{"authId", "authCode", "createdate"});
        operationLogService.addOperationLogThead(user, "权限管理", "修改权限操作", Constants.OPERA_TYPE_AUTH);
        return super.save(oldAuth);
    }

    /**
     * 功能描述:获取所用权限 并把属于角色的权限 绑定权限信息
     *
     * @param roleId 角色ID
     * @return 权限列表
     * @author yangliu  2013-9-25 上午11:16:38
     */
    public List getListAuthBindingRoleId(Integer roleId) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  ta.auth_id          AS authId, ");
        sql.append("  ta.auth_code        AS authCode, ");
        sql.append("  ta.parent_auth_code AS parentAuthCode, ");
        sql.append("  ta.auth_name        AS authName, ");
        sql.append("  ta.auth_url         AS authUrl, ");
        sql.append("  ta.auth_type        AS authType, ");
        sql.append("  ta.auth_orderby     AS authOrderby, ");
        sql.append("  ta.create_time      AS createTime, ");
        sql.append("  ta.request_type     AS requestType, ");
        sql.append("  tra.role_id         AS roleId ");
        sql.append("FROM t_authority ta ");
        sql.append("  LEFT JOIN t_role_authority tra ON ta.auth_code = tra.auth_code ");
        sql.append("AND tra.role_id = :role_id ", roleId);
        sql.append("ORDER BY ta.auth_orderby DESC ");
        return baseDaoPlus.list(sql.toString(), sql.getParam());
    }

    /**
     * 功能描述: 根据user 获取权限
     *
     * @param userid
     * @param type
     * @param requestType 不等于访问的类型
     * @return
     * @author yangliu  2013-9-14 上午09:26:08
     */
    public List<TAuthority> getListTAuthByUser(Integer userid, String type, String requestType) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  ta.* ");
        sql.append("FROM t_role_authority tra ");
        sql.append("  LEFT JOIN t_authority ta ON tra.auth_code = ta.auth_code ");
        sql.append("  LEFT JOIN t_user tu ON tu.role_id = tra.role_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND tu.user_id = :userId ", userid);
        sql.append("      AND ta.request_type & :requestType ", requestType);
        sql.append("      AND ta.auth_type = :authType ", requestType);
        sql.append("ORDER BY ta.auth_orderby DESC ");
        SQLQuery sqlQuery = baseDaoPlus.getSession().createSQLQuery(sql.toString());
        sqlQuery.addEntity(TAuthority.class).setProperties(sql.getParam());
        return sqlQuery.list();
    }

    public List listByUserId(Integer userid, String type, String requestType) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  ta.auth_id          AS authId, ");
        sql.append("  ta.auth_code        AS authCode, ");
        sql.append("  ta.parent_auth_code AS parentAuthCode, ");
        sql.append("  ta.auth_name        AS authName, ");
        sql.append("  ta.auth_url         AS authUrl, ");
        sql.append("  ta.auth_type        AS authType, ");
        sql.append("  ta.auth_orderby     AS authOrderby, ");
        sql.append("  ta.createdate       AS createdate, ");
        sql.append("  ta.request_type     AS requestType ");
        sql.append("FROM t_authority ta ");
        sql.append("  LEFT JOIN t_role_authority tra ");
        sql.append("    ON tra.auth_code = ta.auth_code ");
        sql.append("  LEFT JOIN t_user u ");
        sql.append("    ON u.role_id = tra.role_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND u.user_id = :userId ", userid);
        sql.append("      AND ta.auth_type = :authType ", type);
        sql.append("      AND ta.request_type & :requestType ", requestType);
        sql.append("ORDER BY ta.auth_orderby DESC ");
        List list = baseDaoPlus.list(sql.toString(), sql.getParam());
        List listArray = new ArrayList();
        try {
            for (Object o : list) {
                listArray.add(mapToObject((Map) o, TAuthority.class));
            }
        } catch (Exception e) {
            logger.error(e, e);
        }
        return listArray;
    }

    /**
     * 功能描述:设置菜单的子父级关系
     *
     * @param userAuthList 权限列表
     * @return
     * @author yangliu  2013-9-14 上午09:05:58
     */
    public List<TAuthority> getAuths(List<TAuthorityEnhance> userAuthList) {

        List<TAuthority> parentList = new ArrayList<TAuthority>();
        if (EmptyUtils.isNotEmpty(userAuthList)) {
            for (TAuthorityEnhance parentMenu : userAuthList) {
                if ("A_1000".equals(parentMenu.getParentAuthCode())) {
                    parentList.add(getChildMenu(parentMenu, userAuthList));
                }
            }
        }
        return parentList;
    }

    /**
     * 功能描述: 获取子菜单
     *
     * @param auth 权限
     * @param list 列表
     * @return
     * @author yangliu  2013-9-13 下午06:02:53
     */
    public TAuthority getChildMenu(TAuthorityEnhance auth, List<TAuthorityEnhance> list) {
        List<TAuthority> childAuthList = new ArrayList<TAuthority>();
        for (TAuthorityEnhance childAuth : list) {
            if (auth.getAuthCode().equals(childAuth.getParentAuthCode())) {
                getChildMenu(childAuth, list);
                childAuthList.add(childAuth);
            }
        }
        auth.setChildAuthority(childAuthList);
        return auth;
    }

    /**
     * 功能描述:修改或者保存权限ID
     *
     * @param auth 权限
     * @return
     * @author yangliu  2013-9-24 上午09:56:54
     */

    public Integer saveOrUpdateAuth(TAuthority auth, TUser user) {
        if (auth != null) {
            if (EmptyUtils.isEmpty(auth.getAuthId())) {
                save(auth);
                operationLogService.addOperationLogThead(user, "权限管理", "保存权限操作", Constants.OPERA_TYPE_AUTH);
                auth.setAuthCode(getAuthCode(auth.getAuthId()));
            }
            update(auth);
            operationLogService.addOperationLogThead(user, "权限管理", "修改权限操作", Constants.OPERA_TYPE_AUTH);
            return 1;
        }
        return -1;
    }

    /**
     * 功能描述:根据权限ID 自动生成权限code
     *
     * @param authId 权限ID
     * @return
     * @author yangliu  2013-9-24 上午10:01:43
     */
    private String getAuthCode(Integer authId) {
        if (EmptyUtils.isEmpty(authId))
            return "A_" + TimeUtil.getCurrentDay(SysCode.DATE_FORMAT_NUM_L);
        return "A_" + (1000 + authId);
    }

    /**
     * 功能描述: 删除权限信息
     *
     * @param authCode 权限authCode
     * @return
     * @author yangliu  2013-9-25 下午03:15:55
     */
    public int delete(String authCode, TUser user) {
        if (EmptyUtils.isNotEmpty(authCode)) {
            roleAuthorityService.delete(TRoleAuthority.class, "authCode", authCode);
            super.delete(TAuthority.class, "authCode", authCode);
            TAuthority authority = new TAuthority();
            authority.setParentAuthCode(authCode);
            List<TAuthority> childList = baseDaoPlus.list(authority);
            for (TAuthority auth : childList) {
                delete(auth.getAuthCode(), user);
            }
            operationLogService.addOperationLogThead(user, "权限管理", "删除权限信息", Constants.OPERA_TYPE_AUTH);
            return 1;
        }
        return -1;
    }

    public List list(TAuthority authority, Page page) {
        return baseDaoPlus.list(authority, page);
    }

}
