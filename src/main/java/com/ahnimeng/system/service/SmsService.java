package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.DateUtils;
import com.ahnimeng.common.util.HttpUtil;
import com.ahnimeng.system.model.TSms;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by zhangxiansen on 2016-06-30.
 */
@Service
public class SmsService extends BaseServiceImpl<TSms> {
    @Autowired
    BaseDaoImpl baseDaoPlus;

    public TSms save(TSms sms) {
        TSms tSms = new TSms(
                sms.getTargetNumber(),
                sms.getContent(),
                new Date(),
                0,
                sms.getFlag1(),
                ""
        );
        return super.save(tSms);
    }

    public TSms getLast(String phone) {
        TSms sms = new TSms();
        sms.setTargetNumber(phone);

        Page page = new Page();
        page.setPageSize(1);
        page.setOrderBy("smsId");
        page.setOrder("DESC");

        List list = baseDaoPlus.list(sms, page);
        if (list.size() == 1) {
            TSms lastSms = (TSms) list.get(0);
            return lastSms;
        }
        return null;
    }

    public boolean isValid(String phone, String captcha) {
        TSms sms = getLast(phone);
        if (sms == null || StringUtils.isEmpty(sms.getFlag1())) {
            throw new BusinessLogicException("请先发送验证码");
        }
        if (!sms.getFlag1().equals(captcha)) {
            throw new BusinessLogicException("验证码不正确");
        }
        return true;
    }

    @Transactional
    public TSms generateSmsCode(String phone) {
        TSms lastSms = getLast(phone);

        if (lastSms != null) {
            int limit = 2;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(lastSms.getCreateTime());
            calendar.add(Calendar.MINUTE, limit);
//			calendar.add(Calendar.SECOND, 10);
            if (new Date().before(calendar.getTime())) {
                throw new BusinessLogicException(String.format("%s分钟后才可以再次获取验证码", limit));
            }
        }

        String code = DateUtils.getRandNum(6);
        String msg = String.format("【%s】您的验证码是：%s", Constants.PROJECT_NAME, code);

        TSms tSms = new TSms(
                phone,
                msg,
                new Date(),
                0,
                code,
                ""
        );

        Map<String, String> map = new HashMap<String, String>();
        map.put("un", "N5153860");
        map.put("pw", "Ps799491");
        map.put("da", phone);
        map.put("sm", msg);
        map.put("dc", "15");
        map.put("rd", "1");
        map.put("tf", "3");
        map.put("rf", "2");
        String response = HttpUtil.clientGet("http://222.73.117.138:7891/mt", map);
        logger.debug(String.format("短信发往%s 响应结果%s", phone, response));
        return super.save(tSms);
    }
}
