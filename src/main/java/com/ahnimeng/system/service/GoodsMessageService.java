package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.model.ValidSave;
import com.ahnimeng.common.model.ValidSelect;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TGoods;
import com.ahnimeng.system.model.TGoodsMessage;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.Date;
import java.util.List;

@Service
public class GoodsMessageService extends BaseServiceImpl<TGoodsMessage> {

    @Override
    public TGoodsMessage save(@Validated({ValidSave.class}) TGoodsMessage goodsMessage) {
        goodsMessage.setSendTime(new Date());
        goodsMessage.setDeleted(0);

        TGoods goods = new TGoods();
        goods.setGoodsId(goodsMessage.getGoodsId());
        goods = baseDaoPlus.get(goods);
        goods.setMessageCount(goods.getMessageCount() + 1);
        baseDaoPlus.update(goods);

        return super.save(goodsMessage);
    }

    public List list(@Validated({ValidSelect.class}) TGoodsMessage goodsMessage, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tgm.message_id AS messageId, ");
        sql.append("  tgm.goods_id   AS goodsId, ");
        sql.append("  tgm.sender_id  AS senderId, ");
        sql.append("  tgm.send_time  AS sendTime, ");
        sql.append("  tgm.deleted    AS deleted, ");
        sql.append("  tgm.title      AS title, ");
        sql.append("  tgm.message    AS message, ");
        sql.append("  tui.user_photo AS userPhoto, ");
        sql.append("  tui.user_name  AS userName ");
        sql.append("FROM t_goods_message tgm ");
        sql.append("  LEFT JOIN t_user_info tui ON tui.user_id = tgm.sender_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND tgm.goods_id = :goodsId ", goodsMessage.getGoodsId());
        return baseDaoPlus.list(sql, page);
    }
}
