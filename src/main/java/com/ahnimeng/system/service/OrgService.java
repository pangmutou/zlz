package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.ExcelUtils;
import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.system.model.TOrg;
import com.ahnimeng.system.model.TUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * #(c) ahnimeng shouhou <br/>
 * <p/>
 * 版本说明: $id:$ <br/>
 * <p/>
 * 功能说明: 机构业务处理类
 * <p/>
 * <br/>
 * 创建说明: 2013-9-30 下午01:35:29 yangliu 创建文件<br/>
 * <p/>
 * 修改历史:<br/>
 */
@Service("orgService")
public class OrgService extends BaseServiceImpl<TOrg> {

    @Autowired
    @Qualifier("operationLogService")
    private OperationLogService operationLogService;

    public Page page(TOrg org, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  org_id            AS orgId, ");
        sql.append("  parent_org_id     AS parentOrgId, ");
        sql.append("  org_name          AS orgName, ");
        sql.append("  org_address       AS orgAddress, ");
        sql.append("  create_user_id    AS createUserId, ");
        sql.append("  org_linkman_name  AS orgLinkmanName, ");
        sql.append("  org_linkman_tel   AS orgLinkmanTel, ");
        sql.append("  org_linkman_phone AS orgLinkmanPhone, ");
        sql.append("  org_type          AS orgType, ");
        sql.append("  org_status        AS orgStatus, ");
        sql.append("  remark            AS remark, ");
        sql.append("  deadline          AS deadline, ");
        sql.append("  create_time       AS createTime, ");
        sql.append("  longitude         AS longitude, ");
        sql.append("  latitude          AS latitude, ");
        sql.append("  area_code         AS areaCode, ");
        sql.append("  flag1             AS flag1, ");
        sql.append("  flag2             AS flag2, ");
        sql.append("  community_id      AS communityId ");
        sql.append("FROM t_org ");
        sql.append("WHERE 1=1 ");
        sql.append("      AND org_status = :orgStatus ", org.getOrgStatus());
        sql.append("      AND org_name LIKE :org_name ", org.getOrgName() == null ? null : "%" + org.getOrgName() + "%");
        sql.append("      AND org_type LIKE :org_type ", org.getOrgType());
        sql.append("ORDER BY org_id ASC ");
        List list = baseDaoPlus.list(sql, page);
        page.setResult(list);
        return page;
    }

    /**
     * 修改商铺信息
     *
     * @param org
     * @return
     */
    public TOrg update(TOrg org, TUser user) {
        TOrg source = getOrgById(org.getOrgId());
        String[] ignore = new String[]{
                "orgId",
                "parentOrgId",
                "orgStatus",
                "deadline",
                "createTime",
                "flag1",
                "flag2"
        };
        BeanUtils.copyProperties(org, source, ignore);
        super.update(source);
        operationLogService.addOperationLogThead(user, "组织管理", "更新组织信息", Constants.OPERA_TYPE_ORG);
        return source;
    }

    public TOrg save(TOrg org, TUser user) {
        org = new TOrg(
                0,
                org.getOrgName(),
                org.getOrgAddress(),
                org.getOrgLinkmanName(),
                org.getOrgLinkmanTelephone(),
                org.getOrgLinkmanMobile(),
                org.getOrgType(), //机构类型：0-开发商拟梦 1-平台方运营方 2-水质检测公司 3-商家
                1,
                org.getAreaCode(),
                org.getRemark(),
                user.getUserId(),
                new Date(),
                "",
                "",
                "",
                "",
                null);
        super.save(org);//新增店铺信息
        operationLogService.addOperationLogThead(user, "店铺管理", "添加店铺信息，名称为:" + org.getOrgName(), Constants.OPERA_TYPE_ORG);
        return org;
    }

    /**
     * 根据ID取机构
     *
     * @param orgId
     * @return
     */
    public TOrg getOrgById(Integer orgId) {
        return super.get(TOrg.class, orgId);
    }

    /**
     * 功能描述:删除商铺
     *
     * @param org
     * @param user
     */
    public void deleteOrg(TOrg org, TUser user) {
        org = super.get(TOrg.class, org.getOrgId());
        org.setOrgStatus(0);
        super.update(org);
        operationLogService.addOperationLogThead(user, "商铺管理", "删除商铺信息,名称为:" + org.getOrgName(), Constants.OPERA_TYPE_ORG);
    }


    @Override
    public List<TOrg> getAll(TOrg org) {
        return super.getAll(org);
    }

    /**
     * 导出
     *
     * @param response
     * @param org
     * @throws Exception
     */
    public void exportExcel(HttpServletResponse response, TOrg org) throws Exception {
        List<TOrg> list = this.getAll(org);
        String fileName = "商铺信息";
        String[] headers = {"商铺名称", "地址", "联系人", "联系人电话", "创建时间"};
        String[] columns = {"orgName", "orgAddress", "orgLinkmanName", "orgLinkmanTel", "createTime"};
        ExcelUtils.exportExcel(response, fileName, list, columns, headers, SysCode.DATE_FORMAT_STR_S);
    }

    public Page page(Page page, TOrg org) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("    * ");
        sql.append("FROM ");
        sql.append("    t_org ");
        sql.append("WHERE org_id >= 10000 ");
        sql.append("    AND org_name LIKE :orgName ", org.getOrgName() == null ? null : "%" + org.getOrgName() + "%");
        sql.append("    AND flag1 = :flag1 ", org.getFlag1());
        sql.append("    AND org_type = :orgType ", org.getOrgType());
        sql.append("    AND area_code = :areaCode ", org.getAreaCode());
        //TODO 经纬度查询附近店铺没有加上
        sql.append("    AND org_status = 1 ");
        List result = baseDaoPlus.list(sql, page);
        page.setResult(result);
        return page;
    }

    public TOrg getByUserId(Integer userId) {
        TOrg org = new TOrg();
        org.setCreateUserId(userId);
        return baseDaoPlus.get(org);
    }
}
