package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TGoods;
import com.ahnimeng.system.model.TGoodsCategory;
import com.ahnimeng.system.model.TUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class GoodsService extends BaseServiceImpl<TGoods> {

    @Autowired
    BaseDaoImpl baseDaoPlus;
    @Autowired
    UserInfoService userInfoService;

    public List list(TGoods goods, TGoodsCategory goodsCategory, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tg.goods_id                  AS goodsId, ");
        sql.append("  tg.cat_id                    AS catId, ");
        sql.append("  tg.goods_sn                  AS goodsSn, ");
        sql.append("  tg.goods_name                AS goodsName, ");
        sql.append("  tg.goods_name_style          AS goodsNameStyle, ");
        sql.append("  tg.click_count               AS clickCount, ");
        sql.append("  tg.brand_id                  AS brandId, ");
        sql.append("  tg.provider_name             AS providerName, ");
        sql.append("  tg.goods_number              AS goodsNumber, ");
        sql.append("  tg.goods_weight              AS goodsWeight, ");
        sql.append("  tg.market_price              AS marketPrice, ");
        sql.append("  tg.shop_price                AS shopPrice, ");
        sql.append("  tg.promote_price             AS promotePrice, ");
        sql.append("  tg.promote_start_date        AS promoteStartDate, ");
        sql.append("  tg.promote_end_date          AS promoteEndDate, ");
        sql.append("  tg.warn_number               AS warnNumber, ");
        sql.append("  tg.keywords                  AS keywords, ");
        sql.append("  tg.goods_brief               AS goodsBrief, ");
        sql.append("  tg.goods_desc                AS goodsDesc, ");
        sql.append("  tg.goods_thumb               AS goodsThumb, ");
        sql.append("  tg.goods_img                 AS goodsImg, ");
        sql.append("  tg.original_img              AS originalImg, ");
        sql.append("  tg.is_real                   AS isReal, ");
        sql.append("  tg.extension_code            AS extensionCode, ");
        sql.append("  tg.is_on_sale                AS isOnSale, ");
        sql.append("  tg.is_alone_sale             AS isAloneSale, ");
        sql.append("  tg.is_shipping               AS isShipping, ");
        sql.append("  tg.is_group_purchase         AS isGroupPurchase, ");
        sql.append("  tg.group_purchase_begin_time AS groupPurchaseBeginTime, ");
        sql.append("  tg.group_purchase_end_time   AS groupPurchaseEndTime, ");
        sql.append("  tg.integral                  AS integral, ");
        sql.append("  tg.add_time                  AS addTime, ");
        sql.append("  tg.sort_order                AS sortOrder, ");
        sql.append("  tg.is_delete                 AS isDelete, ");
        sql.append("  tg.is_best                   AS isBest, ");
        sql.append("  tg.is_new                    AS isNew, ");
        sql.append("  tg.is_hot                    AS isHot, ");
        sql.append("  tg.is_promote                AS isPromote, ");
        sql.append("  tg.bonus_type_id             AS bonusTypeId, ");
        sql.append("  tg.last_update               AS lastUpdate, ");
        sql.append("  tg.goods_type                AS goodsType, ");
        sql.append("  tg.seller_note               AS sellerNote, ");
        sql.append("  tg.give_integral             AS giveIntegral, ");
        sql.append("  tg.rank_integral             AS rankIntegral, ");
        sql.append("  tg.suppliers_id              AS suppliersId, ");
        sql.append("  tg.is_check                  AS isCheck, ");
        sql.append("  tg.goods_video               AS goodsVideo, ");
        sql.append("  tg.goods_taste               AS goodsTaste, ");
        sql.append("  tg.user_id                   AS userId, ");
        sql.append("  tg.tag_name                  AS tagName, ");
        sql.append("  tg.area_code                 AS areaCode, ");
        sql.append("  tg.express_fee               AS expressFee, ");
        sql.append("  tg.collect_count             AS collectCount, ");
        sql.append("  tg.message_count             AS messageCount, ");
        sql.append("  tg.lng                       AS lng, ");
        sql.append("  tg.lat                       AS lat, ");
        sql.append("  tg.address                   AS address, ");
        sql.append("  tgc.cat_name                 AS catName, ");
        sql.append("  seller.real_name             AS realName ");
        sql.append("FROM t_goods tg ");
        sql.append("  LEFT JOIN t_goods_category tgc ON tgc.cat_id = tg.cat_id ");
        sql.append("  LEFT JOIN t_user_info seller ON seller.user_id = tg.user_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND tg.goods_id = :goods_id ", goods.getGoodsId());
        sql.append("      AND tg.cat_id = :cat_id ", goods.getCatId());
        sql.append("      AND tg.goods_sn = :goods_sn ", goods.getGoodsSn());
        sql.append("      AND tg.goods_name = :goods_name ", goods.getGoodsName());
        sql.append("      AND tg.goods_name_style = :goods_name_style ", goods.getGoodsNameStyle());
        sql.append("      AND tg.click_count = :click_count ", goods.getClickCount());
        sql.append("      AND tg.brand_id = :brand_id ", goods.getBrandId());
        sql.append("      AND tg.provider_name = :provider_name ", goods.getProviderName());
        sql.append("      AND tg.goods_number = :goods_number ", goods.getGoodsNumber());
        sql.append("      AND tg.goods_weight = :goods_weight ", goods.getGoodsWeight());
        sql.append("      AND tg.market_price = :market_price ", goods.getMarketPrice());
        sql.append("      AND tg.shop_price = :shop_price ", goods.getShopPrice());
        sql.append("      AND tg.promote_price = :promote_price ", goods.getPromotePrice());
        sql.append("      AND tg.promote_start_date = :promote_start_date ", goods.getPromoteStartDate());
        sql.append("      AND tg.promote_end_date = :promote_end_date ", goods.getPromoteEndDate());
        sql.append("      AND tg.warn_number = :warn_number ", goods.getWarnNumber());
        sql.append("      AND tg.keywords = :keywords ", goods.getKeywords());
        sql.append("      AND tg.goods_brief = :goods_brief ", goods.getGoodsBrief());
        sql.append("      AND tg.goods_desc = :goods_desc ", goods.getGoodsDesc());
        sql.append("      AND tg.goods_thumb = :goods_thumb ", goods.getGoodsThumb());
        sql.append("      AND tg.goods_img = :goods_img ", goods.getGoodsImg());
        sql.append("      AND tg.original_img = :original_img ", goods.getOriginalImg());
        sql.append("      AND tg.is_real = :is_real ", goods.getIsReal());
        sql.append("      AND tg.extension_code = :extension_code ", goods.getExtensionCode());
        sql.append("      AND tg.is_on_sale = :is_on_sale ", goods.getIsOnSale());
        sql.append("      AND tg.is_alone_sale = :is_alone_sale ", goods.getIsAloneSale());
        sql.append("      AND tg.is_shipping = :is_shipping ", goods.getIsShipping());
        sql.append("      AND tg.is_group_purchase = :is_group_purchase ", goods.getIsGroupPurchase());
        sql.append("      AND tg.group_purchase_begin_time = :group_purchase_begin_time ", goods.getGroupPurchaseBeginTime());
        sql.append("      AND tg.group_purchase_end_time = :group_purchase_end_time ", goods.getGroupPurchaseEndTime());
        sql.append("      AND tg.integral = :integral ", goods.getIntegral());
        sql.append("      AND tg.add_time = :add_time ", goods.getAddTime());
        sql.append("      AND tg.sort_order = :sort_order ", goods.getSortOrder());
        sql.append("      AND tg.is_delete = :is_delete ", goods.getIsDelete());
        sql.append("      AND tg.is_best = :is_best ", goods.getIsBest());
        sql.append("      AND tg.is_new = :is_new ", goods.getIsNew());
        sql.append("      AND tg.is_hot = :is_hot ", goods.getIsHot());
        sql.append("      AND tg.is_promote = :is_promote ", goods.getIsPromote());
        sql.append("      AND tg.bonus_type_id = :bonus_type_id ", goods.getBonusTypeId());
        sql.append("      AND tg.last_update = :last_update ", goods.getLastUpdate());
        sql.append("      AND tg.goods_type = :goods_type ", goods.getGoodsType());
        sql.append("      AND tg.seller_note = :seller_note ", goods.getSellerNote());
        sql.append("      AND tg.give_integral = :give_integral ", goods.getGiveIntegral());
        sql.append("      AND tg.rank_integral = :rank_integral ", goods.getRankIntegral());
        sql.append("      AND tg.suppliers_id = :suppliers_id ", goods.getSuppliersId());
        sql.append("      AND tg.is_check = :is_check ", goods.getIsCheck());
        sql.append("      AND tg.goods_video = :goods_video ", goods.getGoodsVideo());
        sql.append("      AND tg.goods_taste = :goods_taste ", goods.getGoodsTaste());
        sql.append("      AND tg.user_id = :user_id ", goods.getUserId());
        sql.append("      AND tg.tag_name = :tag_name ", goods.getTagName());
        sql.append("      AND tg.area_code = :area_code ", goods.getAreaCode());
        sql.append("      AND tg.express_fee = :express_fee ", goods.getExpressFee());
        sql.append("      AND tg.collect_count = :collect_count ", goods.getCollectCount());
        sql.append("      AND tg.message_count = :message_count ", goods.getMessageCount());
        sql.append("      AND tg.lng = :lng ", goods.getLng());
        sql.append("      AND tg.lat = :lat ", goods.getLat());
        sql.append("      AND tg.address = :address ", goods.getAddress());
        sql.append("      AND tgc.cat_name = :cat_name ", goodsCategory.getCatName());


//        SqlBuffer sql = new SqlBuffer();
//        sql.append(" SELECT ");
//        sql.append(" SQL_CALC_FOUND_ROWS ");
//        sql.append("  g.goods_id               AS goodsId, ");
//        sql.append("  g.cat_id                 AS catId, ");
//        sql.append("  g.goods_sn               AS goodsSn, ");
//        sql.append("  g.goods_name             AS goodsName, ");
//        sql.append("  g.goods_name_style       AS goodsNameStyle, ");
//        sql.append("  g.click_count            AS clickCount, ");
//        sql.append("  g.brand_id               AS brandId, ");
//        sql.append("  g.provider_name          AS providerName, ");
//        sql.append("  g.goods_number           AS goodsNumber, ");
//        sql.append("  g.goods_weight           AS goodsWeight, ");
//        sql.append("  g.market_price           AS marketPrice, ");
//        sql.append("  g.shop_price             AS shopPrice, ");
//        sql.append("  g.promote_price          AS promotePrice, ");
//        sql.append("  g.promote_start_date     AS promoteStartDate, ");
//        sql.append("  g.promote_end_date       AS promoteEndDate, ");
//        sql.append("  g.warn_number            AS warnNumber, ");
//        sql.append("  g.keywords               AS keywords, ");
//        sql.append("  g.goods_brief            AS goodsBrief, ");
//        sql.append("  g.goods_desc             AS goodsDesc, ");
//        sql.append("  g.goods_thumb            AS goodsThumb, ");
//        sql.append("  g.goods_img              AS goodsImg, ");
//        sql.append("  g.original_img           AS originalImg, ");
//        sql.append("  g.is_real                AS isReal, ");
//        sql.append("  g.extension_code         AS extensionCode, ");
//        sql.append("  g.is_on_sale             AS isOnSale, ");
//        sql.append("  g.is_alone_sale          AS isAloneSale, ");
//        sql.append("  g.is_shipping            AS isShipping, ");
//        sql.append("  g.integral               AS integral, ");
//        sql.append("  g.add_time               AS addTime, ");
//        sql.append("  g.sort_order             AS sortOrder, ");
//        sql.append("  g.is_delete              AS isDelete, ");
//        sql.append("  g.is_best                AS isBest, ");
//        sql.append("  g.is_new                 AS isNew, ");
//        sql.append("  g.is_hot                 AS isHot, ");
//        sql.append("  g.is_promote             AS isPromote, ");
//        sql.append("  g.bonus_type_id          AS bonusTypeId, ");
//        sql.append("  g.last_update            AS lastUpdate, ");
//        sql.append("  g.goods_type             AS goodsType, ");
//        sql.append("  g.seller_note            AS sellerNote, ");
//        sql.append("  g.give_integral          AS giveIntegral, ");
//        sql.append("  g.rank_integral          AS rankIntegral, ");
//        sql.append("  g.suppliers_id           AS suppliersId, ");
//        sql.append("  g.is_check               AS isCheck, ");
//        sql.append("  g.goods_video            AS goodsVideo, ");
//        sql.append("  g.goods_taste            AS goodsTaste, ");
//        sql.append("  g.user_id                AS userId, ");
//        sql.append("  g.tag_name               AS tagName, ");
//        sql.append("  g.area_code              AS areaCode, ");
//        sql.append("  g.is_group_purchase               AS isGroupPurchase, ");
//        sql.append("  g.group_purchase_begin_time       AS groupPurchaseBeginTime, ");
//        sql.append("  g.group_purchase_end_time         AS groupPurchaseEndTime, ");
//        sql.append("  g.express_fee                     AS expressFee, ");
//        sql.append("  g.collect_count                     AS collectCount, ");
//        sql.append("  g.message_count                     AS messageCount, ");
//        sql.append("  g.lng                    AS lng, ");
//        sql.append("  g.lat                    AS lat, ");
//        sql.append("  g.address                AS address, ");
//
//        //商品图片
//        sql.append("  GROUP_CONCAT(gg.img_url) AS imgUrls, ");
//        //商品发布人信息
////        sql.append("  ui.user_id               AS userId, ");
//        sql.append("  ui.real_name             AS realName, ");
//        sql.append("  ui.sex                   AS sex, ");
//        sql.append("  ui.user_telephone        AS userTelephone, ");
//        sql.append("  ui.user_email            AS userEmail, ");
//        sql.append("  ui.qq                    AS qq, ");
//        sql.append("  ui.user_photo            AS userPhoto, ");
//        //商品分类
//        sql.append("  tc.cat_name              AS catName, ");
//        sql.append("  tc.parent_id             AS parentId ");
//        //商品收藏情况
//        sql.append("  ,IF(tcg.user_id, 1, 0) AS isCollect ");
//
//        sql.append("  FROM t_goods g ");
//        sql.append("  LEFT JOIN t_user_info ui ON ui.user_id = g.user_id ");
//        sql.append("  LEFT JOIN t_goods_gallery gg ON gg.goods_id = g.goods_id ");
//        sql.append("  LEFT JOIN t_goods_category tc ON tc.cat_id = g.cat_id ");
//        //商品收藏情况
//        sql.append("  LEFT JOIN t_collect_goods tcg ON tcg.goods_id = g.goods_id and tcg.user_id = :collectUserId ", goods.getCollectCount() == null ? 0 : goods.getCollectCount());
//        sql.append("  WHERE 1 = 1 ");
//        sql.append("  AND g.user_id = :user_id ", goods.getUserId());
//        sql.append("  AND g.goods_name LIKE :goods_name ", goods.getGoodsName() == null ? null : "%" + goods.getGoodsName() + "%");
//        sql.append("  AND (tc.cat_id = :catId  ", goods.getCatId());
//        sql.append("                          || tc.parent_id = :catId) ", goods.getCatId());//商品分类或父商品分类筛选
//        sql.append("  AND g.is_delete = :is_delete ", goods.getIsDelete());//根据商品创建人id查询商品列表
//        sql.append("  AND g.is_hot = :isHot ", goods.getIsHot());//根据商品创建人id查询商品列表
//        sql.append("  AND g.is_group_purchase = :isGroup ", goods.getIsGroupPurchase());//根据商品创建人id查询商品列表
//        sql.append("  AND g.is_promote = :isPromote ", goods.getIsPromote());//根据商品创建人id查询商品列表
//        sql.append("  AND g.area_code = :areaCode ", goods.getAreaCode() != null && !goods.getAreaCode().isEmpty() ? goods.getAreaCode().replace("00", "") + "%" : null);//根据地区筛选商品，但要看见该级别下的所有商品
//
//        if (goods.getExtensionCode() != null && goods.getExtensionCode().equals("relate")) {
//            //获取类似商品时，有特殊的筛选
//            sql.append("  AND g.goods_id != :goodsId ", goods.getGoodsId());//获取类似的商品要排除自身
//        } else {
//            sql.append("  AND g.goods_id = :goods_id ", goods.getGoodsId());
//        }
//
//        sql.append(" GROUP BY g.goods_id ");
//        if (page.getOrderBy() != null && !page.getOrderBy().isEmpty()) {
//            sql.append(" ORDER BY ").append(page.getOrderBy()).append(" ");
//            if (page.getOrder() != null && !page.getOrder().isEmpty()) {
//                sql.append(page.getOrder()).append(" ");
//            }
//        }
//        sql.append(" limit :begin ", (page.getCur() - 1) * page.getPageSize());
//        sql.append(" , :end ", page.getPageSize());
        List list = baseDaoPlus.list(sql, page);

//        Long count = baseDaoPlus.count("SELECT FOUND_ROWS()", null);
//        page.setCount(count.intValue());
//        page.setResult(list);
//        System.out.println("count:" + count);
        return list;
    }

    public List relate(TGoods goods) {
        Page page = new Page();
        page.setPageSize(4);
        page.setOrderBy(" RAND() ");

        goods.setExtensionCode("relate");
        goods.setIsDelete(0);

        List list = list(goods, new TGoodsCategory(), page);

        return list;
    }

    public Map get(TGoods goods) {
        Page page = new Page();
        page.setPageSize(2);
        List list = list(goods, new TGoodsCategory(), page);
        if (list.size() == 0) {
            throw new BusinessLogicException("指定的商品不存在");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("商品信息有误，同时获取到多个商品");
        }
        return (Map) list.get(0);
    }

    public TGoods save(TGoods goods) {
        Date now = new Date();
        {
            if (goods.getGoodsType().compareTo(1) == 0) {
                TUserInfo userInfo = new TUserInfo();
                userInfo.setUserId(goods.getUserId());
                userInfo = userInfoService.get(userInfo);
                if (userInfo.getIsDoctor().compareTo(1) != 0) {
                    throw new BusinessLogicException("非医生用户无法创建咨询信息");
                }
            }
        }

        goods = new TGoods(
                goods.getCatId(),
                "N/A",
                goods.getGoodsName(),
                "N/A",
                0,
                0,
                "N/A",
                goods.getGoodsNumber(),
                BigDecimal.ZERO,
                goods.getMarketPrice(),
                goods.getShopPrice(),
                goods.getPromotePrice(),
                goods.getPromoteStartDate(),
                goods.getPromoteEndDate(),
                0,
                "N/A",
                goods.getGoodsBrief(),
                goods.getGoodsDesc(),
                goods.getGoodsImg(),
                goods.getGoodsImg(),
                goods.getGoodsImg(),
                goods.getItemImg(),
                1,
                "N/A",
                1,
                0,
                0,
                goods.getIsGroupPurchase(),
                goods.getGroupPurchaseBeginTime(),
                goods.getGroupPurchaseEndTime(),
                0,
                now,
                goods.getSortOrder(),
                0,
                0,
                0,
                0,
                0,
                0,
                now,
                goods.getGoodsType(),
                goods.getSellerNote(),
                0,
                0,
                0,
                0,
                "",
                0,
                goods.getUserId(),
                "N/A",
                goods.getAreaCode(),
                BigDecimal.ZERO,
                0,
                0,
                goods.getLng(),
                goods.getLat(),
                goods.getAddress()
        );
        //存商品
        baseDaoPlus.save(goods);
        //如果是咨询，还需要更新该用户最便宜的价格
        updateCheapestPrice(goods.getUserId(), goods.getShopPrice());
        return goods;
    }

    public void updateCheapestPrice(int userId, BigDecimal price) {
        //如果是咨询，还需要更新该用户最便宜的价格
        TUserInfo userInfo = new TUserInfo();
        userInfo.setUserId(userId);
        userInfo = userInfoService.get(userInfo);
        if (userInfo.getCheapestPrice().compareTo(price) > 0) {
            userInfo.setCheapestPrice(price);
            userInfoService.update(userInfo);
        }
    }

    public TGoods update(TGoods goods) {
        TGoods source = new TGoods();
        source.setGoodsId(goods.getGoodsId());
        source = baseDaoPlus.get(source);

        source.setGoodsName(goods.getGoodsName());
        source.setShopPrice(goods.getShopPrice());
        source.setIsOnSale(goods.getIsOnSale() == null ? 0 : goods.getIsOnSale());
        source.setCatId(goods.getCatId());
        source.setGoodsImg(goods.getGoodsImg());
        source.setItemImg(goods.getItemImg());
        source.setShopPrice(goods.getShopPrice());
        source.setGoodsBrief(goods.getGoodsBrief());
        source.setGoodsDesc(goods.getGoodsDesc());
//        BeanUtils.copyProperties(goods, source, new String[]{
////                "goodsId",
////                "catId",
//                "goodsSn",
////                "goodsName",
//                "goodsNameStyle",
//                "clickCount",
//                "brandId",
//                "providerName",
////                "goodsNumber",
//                "goodsWeight",
////                "marketPrice",
////                "shopPrice",
////                "promotePrice",
////                "promoteStartDate",
////                "promoteEndDate",
//                "warnNumber",
//                "keywords",
////                "goodsBrief",
////                "goodsDesc",
////                "goodsThumb",
////                "goodsImg",
////                "originalImg",
//                "isReal",
//                "extensionCode",
////                "isOnSale",
//                "isAloneSale",
//                "isShipping",
////                "isGroupPurchase",
////                "groupPurchaseBeginTime",
////                "groupPurchaseEndTime",
//                "integral",
//                "addTime",
////                "sortOrder",
//                "isDelete",
//                "isBest",
//                "isNew",
////                "isHot",
////                "isPromote",
//                "bonusTypeId",
////                "lastUpdate",
//                "goodsType",
////                "sellerNote",
//                "giveIntegral",
//                "rankIntegral",
//                "suppliersId",
//                "isCheck",
////                "goodsVideo",
////                "goodsTaste",
//                "userId",
//                "tagName",
////                "areaCode",
////                "expressFee",
//                "collectCount",
//                "messageCount",
//                "lng",
//                "lat",
//        });
        updateCheapestPrice(goods.getUserId(), goods.getShopPrice());
        return baseDaoPlus.update(source);
    }

    @Override
    public int delete(TGoods goods) {
        TGoods source = baseDaoPlus.get(goods);
        source.setIsDelete(1);
        baseDaoPlus.update(source);
        return 1;
    }

    @Transactional
    public TGoods statusChange(Integer goodsId, Integer value, String statusType) {
        TGoods goods = get(TGoods.class, goodsId);
        if (statusType.equals("sale")) {
            goods.setIsOnSale(value);
//        } else if (statusType.equals("hot")) {
//            goods.setIsHot(value);
        } else if (statusType.equals("promote")) {
            goods.setIsPromote(value);
        } else {
            throw new BusinessLogicException("未知的商品特殊状态");
        }
        return update(goods);
    }

    /**
     * 对指定商品的库存进行修改,使用时应使用悲观锁
     *
     * @param goodsId
     * @param goodsNumber
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public TGoods updateInventory(int goodsId, short goodsNumber) {
        //TODO 淘宝初期使用拍下减库存的方式,在多年之后又增加了付款减库存的方式,项目现阶段使用拍下减库存的方式.在创建订单时调用此方法
        TGoods goods = super.get(TGoods.class, goodsId);
        goods.setGoodsNumber(goods.getGoodsNumber() + goodsNumber);
        return super.update(goods);
    }

}
