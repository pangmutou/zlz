package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TArea;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by funcong-tp on 2016/4/11.
 */
@Service
public class AreaService extends BaseServiceImpl<TArea> {

    public List page(TArea area, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  area_code AS areaCode, ");
        sql.append("  area_name AS areaName ");
        sql.append("FROM t_area ");
        sql.append("WHERE area_name NOT IN ('市辖区', '县') ");
        sql.append("      AND area_code LIKE :areaCode ", area == null || StringUtils.isEmpty(area.getAreaCode()) ? null : "%" + area.getAreaCode() + "%");
        return baseDaoPlus.list(sql, page);
    }

}
