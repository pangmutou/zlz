package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TArticleCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhangxiansen on 2016/4/17.
 */
@Service
public class ArticleCatService extends BaseServiceImpl<TArticleCat> {

    @Autowired
    BaseDaoImpl baseDaoPlus;

    public List list(TArticleCat articleCat, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append(" cat_id      AS catId, ");
        sql.append(" cat_name    AS catName, ");
        sql.append(" cat_type    AS catType, ");
        sql.append(" keywords    AS keywords, ");
        sql.append(" cat_desc    AS catDesc, ");
        sql.append(" sort_order  AS sortOrder, ");
        sql.append(" show_in_nav AS showInNav, ");
        sql.append(" parent_id   AS parentId  ");
        sql.append(" FROM ");
        sql.append(" t_article_cat ");
        sql.append(" WHERE 1=1 ");
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

    public List list(TArticleCat articleCat) {
        List list = list(articleCat, null);
        return list;
    }
}
