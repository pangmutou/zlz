package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TVars;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by zhangxiansen on 2016-10-13.
 */
@Service
public class VarsService extends BaseServiceImpl<TVars> {

    public List list(TVars vars, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tvars.id          AS id, ");
        sql.append("  tvars.parent_id   AS parentId, ");
        sql.append("  tvars.level       AS level, ");
        sql.append("  tvars.name        AS name, ");
        sql.append("  tvars.value       AS value, ");
        sql.append("  tvars.remark      AS remark, ");
        sql.append("  tvars.flag1       AS flag1, ");
        sql.append("  tvars.flag2       AS flag2, ");
        sql.append("  tvars.status      AS status, ");
        sql.append("  tvars.create_time AS createTime ");
        sql.append("FROM t_vars tvars ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND tvars.id = :id ", vars.getId());
        sql.append("      AND tvars.parent_id = :parentId ", vars.getParentId());
        sql.append("      AND tvars.level = :level ", vars.getLevel());
        sql.append("      AND tvars.name = :name ", vars.getName());
        sql.append("      AND tvars.value = :value ", vars.getValue());
        sql.append("      AND tvars.remark = :remark ", vars.getRemark());
        sql.append("      AND tvars.flag1 = :flag1 ", vars.getFlag1());
        sql.append("      AND tvars.flag2 = :flag2 ", vars.getFlag2());
        sql.append("      AND tvars.status = :status ", vars.getStatus());
        sql.append("      AND tvars.create_time = :createTime ", vars.getCreateTime());
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

    public List<TVars> listWithBean(TVars vars, Page page){
        return baseDaoPlus.list(vars, page);
    }

    public TVars get(TVars vars) {
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(vars, page);
        if (list.size() == 0) {
            throw new BusinessLogicException("没有查询到相关变量");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("查询到多个匹配的变量");
        }
        Map map = list.get(0);
        vars = new TVars();
        vars.setId(Integer.parseInt(map.get("id").toString()));
        return baseDaoPlus.get(vars);
    }

    @Override
    public TVars save(TVars x) {
        return super.save(x);
    }

    @Override
    public TVars update(TVars x) {
        return super.update(x);
    }

    @Override
    public int delete(TVars obj) {
        return super.delete(obj);
    }

    public Map getMap(TVars vars) {
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(vars, page);
        if (list.size() < 1) {
            throw new BusinessLogicException("没有该条需求记录");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("错误的筛选条件，匹配到多条记录");
        }
        return list.get(0);
    }
}
