package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TBusinessOrderGoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BusinessOrderGoodsService extends BaseServiceImpl<TBusinessOrderGoods> {

    @Autowired
    GoodsService goodsService;
//    @Autowired
//    private TradeOrderService tradeOrderService;

    public TBusinessOrderGoods save(TBusinessOrderGoods orderGoods) {
        return baseDaoPlus.save(orderGoods);
    }

    public Map getMap(TBusinessOrderGoods orderGoods) {
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(orderGoods, page);
        if (list.size() == 0) {
            throw new BusinessLogicException("没有找到该订单商品");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("数据异常，匹配到多个订单商品");
        }
        return list.get(0);
    }

    public TBusinessOrderGoods get(TBusinessOrderGoods orderGoods) {
        Map map = getMap(orderGoods);
        orderGoods = new TBusinessOrderGoods();
        orderGoods.setRecId(Integer.parseInt(map.get("recId").toString()));
        return baseDaoPlus.get(orderGoods);
    }

    public List<Map> list(TBusinessOrderGoods orderGoods, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tbog.rec_id               AS recId, ");
        sql.append("  tbog.order_sn             AS orderSn, ");
        sql.append("  tbog.goods_id             AS goodsId, ");
        sql.append("  tbog.goods_name           AS goodsName, ");
        sql.append("  tbog.goods_sn             AS goodsSn, ");
        sql.append("  tbog.goods_number         AS goodsNumber, ");
        sql.append("  tbog.market_price         AS marketPrice, ");
        sql.append("  tbog.goods_price          AS goodsPrice, ");
        sql.append("  tbog.goods_attr           AS goodsAttr, ");
        sql.append("  tbog.goods_img            AS goodsImg, ");
        sql.append("  tbog.send_number          AS sendNumber, ");
        sql.append("  tbog.is_real              AS isReal, ");
        sql.append("  tbog.extension_code       AS extensionCode, ");
        sql.append("  tbog.parent_id            AS parentId, ");
        sql.append("  tbog.is_gift              AS isGift, ");
        sql.append("  tbog.goods_attr_id        AS goodsAttrId, ");
        sql.append("  tbog.goods_create_user_id AS goodsCreateUserId ");
        sql.append("FROM t_business_order_goods tbog ");
//        sql.append("  LEFT JOIN t_goods tg ON tg.goods_id = tbog.goods_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND tbog.order_sn in ( :order_sn )", orderGoods.getOrderSn() == null ? null : orderGoods.getOrderSn().split(","));
        sql.append("ORDER BY tbog.rec_id asc");
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

//    public List listByOrderSn(TGoods goods, TGoodsCategory goodsCategory, Page page) {
//        SqlBuffer sql = new SqlBuffer();
//        sql.append("SELECT ");
//        sql.append("  tg.goods_id                  AS goodsId, ");
//        sql.append("  tg.cat_id                    AS catId, ");
//        sql.append("  tg.goods_sn                  AS goodsSn, ");
//        sql.append("  tg.goods_name                AS goodsName, ");
//        sql.append("  tg.goods_name_style          AS goodsNameStyle, ");
//        sql.append("  tg.click_count               AS clickCount, ");
//        sql.append("  tg.brand_id                  AS brandId, ");
//        sql.append("  tg.provider_name             AS providerName, ");
//        sql.append("  tg.goods_number              AS goodsNumber, ");
//        sql.append("  tg.goods_weight              AS goodsWeight, ");
//        sql.append("  tg.market_price              AS marketPrice, ");
//        sql.append("  tg.shop_price                AS shopPrice, ");
//        sql.append("  tg.promote_price             AS promotePrice, ");
//        sql.append("  tg.promote_start_date        AS promoteStartDate, ");
//        sql.append("  tg.promote_end_date          AS promoteEndDate, ");
//        sql.append("  tg.warn_number               AS warnNumber, ");
//        sql.append("  tg.keywords                  AS keywords, ");
//        sql.append("  tg.goods_brief               AS goodsBrief, ");
//        sql.append("  tg.goods_desc                AS goodsDesc, ");
//        sql.append("  tg.goods_thumb               AS goodsThumb, ");
//        sql.append("  tg.goods_img                 AS goodsImg, ");
//        sql.append("  tg.original_img              AS originalImg, ");
//        sql.append("  tg.is_real                   AS isReal, ");
//        sql.append("  tg.extension_code            AS extensionCode, ");
//        sql.append("  tg.is_on_sale                AS isOnSale, ");
//        sql.append("  tg.is_alone_sale             AS isAloneSale, ");
//        sql.append("  tg.is_shipping               AS isShipping, ");
//        sql.append("  tg.is_group_purchase         AS isGroupPurchase, ");
//        sql.append("  tg.group_purchase_begin_time AS groupPurchaseBeginTime, ");
//        sql.append("  tg.group_purchase_end_time   AS groupPurchaseEndTime, ");
//        sql.append("  tg.integral                  AS integral, ");
//        sql.append("  tg.add_time                  AS addTime, ");
//        sql.append("  tg.sort_order                AS sortOrder, ");
//        sql.append("  tg.is_delete                 AS isDelete, ");
//        sql.append("  tg.is_best                   AS isBest, ");
//        sql.append("  tg.is_new                    AS isNew, ");
//        sql.append("  tg.is_hot                    AS isHot, ");
//        sql.append("  tg.is_promote                AS isPromote, ");
//        sql.append("  tg.bonus_type_id             AS bonusTypeId, ");
//        sql.append("  tg.last_update               AS lastUpdate, ");
//        sql.append("  tg.goods_type                AS goodsType, ");
//        sql.append("  tg.seller_note               AS sellerNote, ");
//        sql.append("  tg.give_integral             AS giveIntegral, ");
//        sql.append("  tg.rank_integral             AS rankIntegral, ");
//        sql.append("  tg.suppliers_id              AS suppliersId, ");
//        sql.append("  tg.is_check                  AS isCheck, ");
//        sql.append("  tg.goods_video               AS goodsVideo, ");
//        sql.append("  tg.goods_taste               AS goodsTaste, ");
//        sql.append("  tg.user_id                   AS userId, ");
//        sql.append("  tg.tag_name                  AS tagName, ");
//        sql.append("  tg.area_code                 AS areaCode, ");
//        sql.append("  tg.express_fee               AS expressFee, ");
//        sql.append("  tg.collect_count             AS collectCount, ");
//        sql.append("  tg.message_count             AS messageCount, ");
//        sql.append("  tg.lng                       AS lng, ");
//        sql.append("  tg.lat                       AS lat, ");
//        sql.append("  tg.address                   AS address, ");
//        sql.append("  tgc.cat_name                 AS catName, ");
//        sql.append("  seller.real_name             AS realName ");
//        sql.append("FROM t_goods tg ");
//        sql.append("  LEFT JOIN t_goods_category tgc ON tgc.cat_id = tg.cat_id ");
//        sql.append("  LEFT JOIN t_user_info seller ON seller.user_id = tg.user_id ");
//        sql.append("  LEFT JOIN t_business_order tbo ON tbo.goods_id = tg.user_id ");
//        sql.append("WHERE 1 = 1 ");
//        sql.append("      AND tg.goods_id = :goods_id ", goods.getGoodsId());
//        List list = baseDaoPlus.list(sql.toString(), sql.getParam(), page);
//
//        return list;
//    }

}
