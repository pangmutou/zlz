package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangxiansen on 2016-10-21.
 */
@Service
public class MessageService extends BaseServiceImpl<TMessage> {

    @Override
    public TMessage save(TMessage x) {
        TMessage target = new TMessage(
                x.getUserId(),
                x.getTitle(),
                x.getContent(),
                0,
                new Date()
        );
        return baseDaoPlus.save(target);
    }

    public List list(TMessage message, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  m.message_id  AS messageId, ");
        sql.append("  m.user_id     AS userId, ");
        sql.append("  m.title       AS title, ");
        sql.append("  m.content     AS content, ");
        sql.append("  m.have_read   AS haveRead, ");
        sql.append("  m.create_time AS createTime ");
        sql.append("FROM t_message m ");
        sql.append("WHERE 1 = 1 ");
        sql.append("  AND m.message_id = :messageId", message.getMessageId());
        sql.append("  AND m.user_id = :userId", message.getUserId());
        sql.append("  AND m.title = :title", message.getTitle());
        sql.append("  AND m.content = :content", message.getContent());
        sql.append("  AND m.have_read = :haveRead", message.getHaveRead());
        sql.append("  AND m.create_time = :createTime", message.getCreateTime());
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

    public Map getMap(TMessage message) {
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(message, page);
        if (list.size() == 0) {
            throw new BusinessLogicException("没有获取到数据");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("获取到多条数据");
        }
        return list.get(0);
    }

    @Transactional
    public TMessage get(TMessage message) {
        Map map = getMap(message);
        message = new TMessage();
        message.setMessageId(Integer.parseInt(map.get("messageId").toString()));
        message.setUserId(Integer.parseInt(map.get("userId").toString()));
        TMessage tMessage = baseDaoPlus.get(message);
        if (tMessage.getHaveRead() == 0) {
            tMessage.setHaveRead(1);
            return baseDaoPlus.update(tMessage);
        } else {
            return tMessage;
        }
    }
}
