package com.ahnimeng.system.service;

import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.transaction.alipay.config.AlipayConfig;
import com.ahnimeng.common.transaction.alipay.model.AlipayOrderInfo;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.UniqueNumberUtil;
import com.ahnimeng.system.model.TBusinessOrder;
import com.ahnimeng.system.model.TTradeOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by funcong-tp on 2016-08-27.
 */
@Service
public class TradeOrderService extends BaseServiceImpl<TTradeOrder> {

    @Autowired
    BusinessOrderService businessOrderService;

    /**
     * 获取交易订单
     * @param orderSn
     * @return
     */
    public TTradeOrder get(String orderSn) {
        TTradeOrder tradeOrder = new TTradeOrder();
        tradeOrder.setTradeOrderSn(orderSn);
        if (tradeOrder == null) {
            throw new BusinessLogicException(String.format("编号%s的交易订单不存在", orderSn));
        }
        return baseDaoPlus.get(tradeOrder);
    }

    /**
     * 创建交易订单
     * @param businessOrder
     * @return
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public TTradeOrder save(TBusinessOrder businessOrder) {
        //订单主题和内容会包含分类名称
        String subject, body, tradeOrderSn;
        {
            //交易订单统一使用00
            tradeOrderSn = UniqueNumberUtil.create("00");
            //主题
            subject = String.format("%s订单%s", Constants.PROJECT_NAME, tradeOrderSn);
            //内容
            body = String.format("%s", businessOrder.getContent());
        }
        //创建交易订单
        TTradeOrder tradeOrder = new TTradeOrder(
                businessOrder.getOrderSn(),
                tradeOrderSn,
                businessOrder.getBuyerUserId(),
                businessOrder.getMoney(),
                0,
                new Date(),
                BigDecimal.ZERO,
                null,
                "",
                "",
                subject,
                body);
        return baseDaoPlus.save(tradeOrder);
    }

    /**
     * 当成功支付过后
     *
     * @param alipayOrderInfo
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void paid(AlipayOrderInfo alipayOrderInfo) {
        TTradeOrder tradeOrderSource;
        {
            //判断订单正确性部分
            try {
                tradeOrderSource = get(alipayOrderInfo.getOut_trade_no());
                if (alipayOrderInfo.getSeller_id().equals(AlipayConfig.seller_id)) {
                    throw new BusinessLogicException("支付宝异步通知参数中的卖家ID与服务器中配置的不同");
                }
                if (tradeOrderSource.getTotalFee().compareTo(new BigDecimal(alipayOrderInfo.getTotal_fee())) != 0) {
                    throw new BusinessLogicException("支付宝异步通知参数中的金额与订单实际金额不同");
                }
                if (tradeOrderSource.getStatus() > 0) {
                    //已经支付过的订单就不再处理了
                    //触发该条件有可能是因为网络差而导致的。因此也抛出一下，并记录一下日志
                    throw new BusinessLogicException(String.format("订单%s接收到重复的支付成功通知", alipayOrderInfo.getOut_trade_no()));
                }
            } catch (RuntimeException e) {
                //交易这部分出异常比较麻烦,都记录一下
                logger.error(e, e);
                throw e;
            }
        }

        String payType;
        if (alipayOrderInfo instanceof AlipayOrderInfo) {
            payType = "支付宝";
        } else {
            payType = "其它";
            throw new BusinessLogicException("系统暂不支持支付宝以外的付款方式");
        }

        //操作部分
        //更新成功支付的订单信息
        TTradeOrder model = new TTradeOrder(
                tradeOrderSource.getBusinessOrderSn(),
                tradeOrderSource.getTradeOrderSn(),
                tradeOrderSource.getUserId(),
                tradeOrderSource.getTotalFee(),
                1,
                tradeOrderSource.getCreateTime(),
                new BigDecimal(alipayOrderInfo.getTotal_fee()),
                new Date(),
                payType,
                "",
                tradeOrderSource.getSubject(),
                tradeOrderSource.getBody()
        );
        BeanUtils.copyProperties(model, tradeOrderSource, new String[]{
                "tradeOrderId",
                "businessOrderSn",
                "tradeOrderSn",
                "userId",
                "totalFee",
//                "status",
                "createTime",
//                "paidFee",
//                "paidTime",
//                "payType",
//                "payerInfo",
                "subject",
                "body",
        });
        //更新交易订单信息
        baseDaoPlus.update(tradeOrderSource);

        //更新业务订单信息
        TBusinessOrder businessOrder = new TBusinessOrder();
        businessOrder.setOrderSn(tradeOrderSource.getBusinessOrderSn());
        businessOrder = businessOrderService.get(businessOrder);

        businessOrder.setStatus(BusinessOrderStatusEnum.isPaid.value());
        businessOrder.setIsPaid(1);
        baseDaoPlus.update(businessOrder);
    }
}