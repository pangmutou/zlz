package com.ahnimeng.system.service;

import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.EmptyUtils;
import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.system.model.TRoleAuthority;
import com.ahnimeng.system.model.TUser;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("roleAuthorityService")
public class RoleAuthorityService extends BaseServiceImpl<TRoleAuthority> {

    @Autowired
    @Qualifier("operationLogService")
    private OperationLogService operationLogService;

    public Integer deleteByRoleId(Integer roleId, TUser user) {
        operationLogService.addOperationLogThead(user, "角色管理", "删除角色权限", Constants.OPERA_TYPE_ROLE);
        TRoleAuthority roleAuthority = new TRoleAuthority();
        roleAuthority.setRoleId(roleId);
        List<TRoleAuthority> list = baseDaoPlus.list(roleAuthority);
        for (TRoleAuthority item : list) {
            baseDaoPlus.delete(item);
        }
        return 1;
    }

    public Integer save(String roleidsStr, String authCodeStr, TUser user) {
        if (EmptyUtils.isNotEmpty(roleidsStr) && EmptyUtils.isNotEmpty(authCodeStr)) {
//            Set<TRoleAuthority> set = new HashSet<TRoleAuthority>();
            String[] roleIds = StringUtils.split(roleidsStr, SysCode.COMMA);
            String[] authCodes = StringUtils.split(authCodeStr, SysCode.COMMA);
            TRoleAuthority roleAuth;
            for (String roleId : roleIds) {
                deleteByRoleId(Integer.parseInt(roleId), user);
                for (String authCode : authCodes) {
                    roleAuth = new TRoleAuthority(authCode, Integer.parseInt(roleId));
//                    set.add(roleAuth);
                    baseDaoPlus.save(roleAuth);
                }
            }
            operationLogService.addOperationLogThead(user, "角色管理", "保存用户角色权限", Constants.OPERA_TYPE_USER);
//            baseDaoPlus.save(set);
            return 1;
        }
        return -1;
    }

}
