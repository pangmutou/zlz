package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.EmptyUtils;
import com.ahnimeng.system.model.TArea;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.model.TUserInfo;
import com.ahnimeng.system.model.TVars;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("userInfoService")
public class UserInfoService extends BaseServiceImpl<TUserInfo> {

    @Autowired
    UserService userService;
    @Autowired
    AreaService areaService;
    @Autowired
    private VarsService varsService;
    @Autowired
    @Qualifier("operationLogService")
    private OperationLogService operationLogService;
    @Autowired
    private BaseDaoImpl baseDaoPlus;

    public int getUserCount(TUser currentUser) {
        TUser user = new TUser();
        user.setOrgId(currentUser.getOrgId());
        return baseDaoPlus.count(user).intValue();
    }

    /**
     * 功能描述:保存用户以及用户与角色的关联
     *
     * @param userInfo 用户对象
     * @param roleId   角色ID(1,2,3)
     * @author yangliu 2013-9-12 下午05:23:46
     */
//    public void saveUserInfo(TUserEnhance currentUser, TUserInfo userInfo, String roleId, HttpSession session) {
//        String contents;
//        if (EmptyUtils.isEmpty(userInfo.getUserId()) || userInfo.getUserId() != 0) {// 判断Tuser表的userid是否为空
//            contents = "保存用户信息";
//            TUser user = (TUser) baseDaoPlus.getSession().get(TUser.class, userInfo.getUserId());
//            if (user != null) {
//                contents = "用户名已存在";
//                return;
//            }
//        } else {
//            contents = "修改用户信息";
//        }
//        TUser user = setDelfautl(null, userInfo.getTelephone());
//        if (SecurityUtils.isNotAllGranted(ConstantAuth.ADD_USER_AUTH, currentUser)) {
//            user.setOrgId(currentUser.getOrgId());
//            roleId = Constants.ROLE_DEFAULT + "";
//        }
//
////        userInfo.setTUser(user);
//        saveOrUpdate(userInfo);// 保存和修改用户
//        operationLogService.addOperationLogThead(HttpSessionUtils.getCurrentUser(session), "用户管理", contents, Constants.OPERA_TYPE_USER);
//    }

    public void save(TUser user, TUserInfo userInfo, TUser operationUser) {
        setDelfautl(user, userInfo.getTelephone());
        if (operationUser.getUserId() > 11) {
            throw new BusinessLogicException("没有新增用户权限");
        }
        baseDaoPlus.save(user);

        userInfo.setUserId(user.getUserId());
        baseDaoPlus.save(userInfo);

        operationLogService.addOperationLogThead(operationUser, "用户管理", "新增用户", Constants.OPERA_TYPE_USER);
    }

    public TUser setDelfautl(TUser user, String userTelephone) {
        if (user == null) {
            user = new TUser();
        }
        if (EmptyUtils.isEmpty(user.getStatus())) {
            user.setStatus(1);
        }
        if (EmptyUtils.isEmpty(user.getCreateTime())) { // 判断时间是否为空,为空重新赋值
            user.setCreateTime(new Date());
        }
        if (EmptyUtils.isEmpty(user.getPassword())) {// 判断密码是否为空，否则加入默认密码
            throw new BusinessLogicException("密码不能为空");
        }
        //TODO 会爆
//        if (EmptyUtils.isNotEmpty(user.getNewpasswod())) {// 判断用户是否修改了密码
//            user.setPassword(MD5Util.encoderByMd5(user.getNewpasswod()));
//        }
        //如果审核密码为空赋值为默认密码
        return user;
    }

    public TUser getUser(String username) {
        TUser user = new TUser();
        user.setUsername(username);
        return baseDaoPlus.get(user);
    }

    public Map getDetailByUserId(Integer userId) {
        TUser user = new TUser();
        user.setUserId(userId);
        Page page = new Page();
        page.setPageSize(2);

        List list = list(user, new TUserInfo(), page);
        if (list.size() == 0) {
            throw new BusinessLogicException("没有查询到该用户");
        }
        return (Map) list.get(0);
    }

    public TUserInfo update(TUser user, TUserInfo userInfo) {
        TUserInfo source = new TUserInfo();
        source.setUserId(userInfo.getUserId());
        source = get(source);

//        user的部分走userService
//        if (user != null) {
//            TUser sourceUser = userService.get(TUser.class, user.getUserId());
//            sourceUser.setRoleId(user.getRoleId());
//            baseDaoPlus.update(sourceUser);
//        }

        if (!StringUtils.isEmpty(userInfo.getNickname()))
            source.setNickname(userInfo.getNickname());
        if (!StringUtils.isEmpty(userInfo.getPhoto()))
            source.setPhoto(userInfo.getPhoto());

        return baseDaoPlus.update(source);
    }

    public TUserInfo get(TUserInfo userInfo) {
        userInfo = baseDaoPlus.get(userInfo);
        if (userInfo == null) {
            throw new BusinessLogicException("该用户信息不存在");
        }
        return userInfo;
    }

    public List list(TUser user, TUserInfo userInfo, Page page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tu.user_id                   AS userId, ");
        sql.append("  tu.username                  AS username, ");
//        sql.append("  tu.email                     AS email, ");
//        sql.append("  tu.org_id                    AS orgId, ");
//        sql.append("  tu.role_id                   AS roleId, ");
//        sql.append("  tu.status                    AS status, ");
//        sql.append("  tu.session_id                AS sessionId, ");
        sql.append("  tu.weixin_open_id            AS weixinOpenId, ");
        sql.append("  tu.create_time               AS createTime, ");
        sql.append("  tu.last_login_time           AS lastLoginTime, ");
        sql.append("  tu.huanxin_username          AS huanxinUsername, ");
        sql.append("  tu.huanxin_password          AS huanxinPassword, ");

//        sql.append("  tui.user_id                  AS userId, ");
        sql.append("  tui.real_name                AS realName, ");
        sql.append("  tui.nickname                 AS nickname, ");
        sql.append("  tui.sex                      AS sex, ");
        sql.append("  tui.telephone                AS telephone, ");
        sql.append("  tui.mobile                   AS mobile, ");
        sql.append("  tui.`qq`                     AS qq, ");
        sql.append("  tui.photo                    AS photo, ");
        sql.append("  tui.area_code                AS areaCode, ");
        sql.append("  tui.area_name                AS areaName, ");
        sql.append("  tui.address                  AS address, ");
        sql.append("  tui.lat                      AS lat, ");
        sql.append("  tui.lng                      AS lng, ");
        sql.append("  tui.flag1                    AS flag1, ");
        sql.append("  tui.flag2                    AS flag2, ");
        sql.append("  tui.flag3                    AS flag3, ");
        sql.append("  tui.hospital_id              AS hospitalId, ");
        sql.append("  tui.department_id            AS departmentId, ");
        sql.append("  tui.professional_title_id    AS professionalTitleId, ");
        sql.append("  tui.is_doctor                AS isDoctor, ");
        sql.append("  tui.invite_code              AS inviteCode, ");
        sql.append("  tui.collect_count            AS collectCount, ");
        sql.append("  tui.trade_count              AS tradeCount, ");
        sql.append("  hospital.name                AS hospitalName, ");
        sql.append("  department.name              AS departmentName, ");
        sql.append("  professional.name            AS professionalTitleName, ");
        sql.append("  CASE WHEN tuc.collect_id > 0 THEN 1 ELSE 0 END AS isCollect ");
        sql.append("  FROM t_user tu ");
        sql.append("    LEFT JOIN t_user_info tui ON tui.user_id = tu.user_id ");
        sql.append("    LEFT JOIN t_vars hospital ON hospital.id = tui.hospital_id ");
        sql.append("    LEFT JOIN t_vars department ON department.id = tui.department_id ");
        sql.append("    LEFT JOIN t_vars professional ON professional.id = tui.professional_title_id ");
        sql.append("    LEFT JOIN t_user_collect tuc ON tuc.target_user_id = tui.user_id AND tuc.user_id = :tuc.user_id ", userInfo.getCollectCount() != null && userInfo.getCollectCount() > 0 ? userInfo.getCollectCount() : 0);
        sql.append("  WHERE 1 = 1 ");
        sql.append("  AND tu.role_id = :roleId", user.getRoleId());
        sql.append("  AND tu.user_id = :userId ", user.getUserId());
        sql.append("  AND tu.username LIKE :username ", StringUtils.isEmpty(user.getUsername()) ? null : "%" + user.getUsername() + "%");
        sql.append("  AND tu.status = :status ", user.getStatus());
        sql.append("  AND tu.org_id = :orgId ", user.getOrgId());
        sql.append("  AND tui.real_name LIKE :real_name ", StringUtils.isEmpty(userInfo.getRealName()) ? null : "%" + userInfo.getRealName() + "%");
        sql.append("  AND tui.user_id= :userId ", userInfo.getUserId() == 0 ? null : userInfo.getUserId());
        sql.append("  AND tui.nickname= :nickname ", userInfo.getNickname());
        sql.append("  AND tui.sex= :sex ", userInfo.getSex());
        sql.append("  AND tui.telephone= :telephone ", userInfo.getTelephone());
        sql.append("  AND tui.mobile= :mobile ", userInfo.getMobile());
        sql.append("  AND tui.qq= :qq ", userInfo.getQq());
        sql.append("  AND tui.photo= :photo ", userInfo.getPhoto());
        sql.append("  AND tui.area_code= :areaCode ", userInfo.getAreaCode());
        sql.append("  AND tui.area_name= :areaName ", userInfo.getAreaName());
        sql.append("  AND tui.address= :address ", userInfo.getAddress());
        sql.append("  AND tui.lat= :lat ", userInfo.getLat());
        sql.append("  AND tui.lng= :lng ", userInfo.getLng());
        sql.append("  AND tui.hospital_id = :hospital_id ", userInfo.getHospitalId());
        sql.append("  AND tui.department_id = :department_id ", userInfo.getDepartmentId());
        sql.append("  AND tui.professional_title_id = :professional_title_id ", userInfo.getProfessionalTitleId());
        sql.append("  AND tui.is_doctor = :is_doctor ", userInfo.getIsDoctor());
        sql.append("  AND tui.invite_code = :invite_code ", userInfo.getInviteCode());
        return baseDaoPlus.list(sql, page);
    }

    public TUserInfo save(TUserInfo userInfo, String specialIdString) {
        //特长信息
        TVars specialVars = new TVars();
        StringBuffer specialId = new StringBuffer();
        StringBuffer specialValue = new StringBuffer();
        String[] specialData = specialIdString.split(",");
        for (int i = 0; i < specialData.length; i++) {
            if (EmptyUtils.isNotEmpty(specialData[i])) {
                specialVars = varsService.get(TVars.class, Integer.parseInt(specialData[i]));
                specialId.append(specialVars.getId().toString() + ",");
                specialValue.append(specialVars.getValue() + ",");
            }
        }

        TUserInfo target = new TUserInfo(
                userInfo.getUserId(),
                userInfo.getRealName(),
                userInfo.getNickname(),
                userInfo.getSex(),
                userInfo.getTelephone(),
                userInfo.getMobile(),
                userInfo.getQq(),
                userInfo.getPhoto(),
                userInfo.getAreaCode(),
                userInfo.getAreaName(),
                userInfo.getAddress(),
                userInfo.getLat(),
                userInfo.getLng(),
                userInfo.getFlag1(),
                userInfo.getFlag2(),
                userInfo.getFlag3(),
                userInfo.getHospitalId(),
                userInfo.getDepartmentId(),
                userInfo.getProfessionalTitleId(),
                userInfo.getIsDoctor(),
                userInfo.getInviteCode(),
                0,
                0,
                BigDecimal.ZERO
        );
        return baseDaoPlus.save(target);
    }

    public TUserInfo save(TUser user, TUserInfo userInfo) {
        TArea area = new TArea();
        userInfo = new TUserInfo(
                user.getUserId(),
                userInfo.getRealName(),
                userInfo.getNickname(),
                -1,
                userInfo.getTelephone(),
                user.getUsername(),
                "",
                userInfo.getPhoto(),
                area.getAreaCode(),
                area.getAreaName(),
                userInfo.getAddress(),
                userInfo.getLat(),
                userInfo.getLng(),
                userInfo.getFlag1(),
                userInfo.getFlag2(),
                userInfo.getFlag3(),
                userInfo.getHospitalId(),
                userInfo.getDepartmentId(),
                userInfo.getProfessionalTitleId(),
                userInfo.getIsDoctor(),
                userInfo.getInviteCode(),
                userInfo.getCollectCount(),
                userInfo.getTradeCount(),
                BigDecimal.ZERO
        );
        return baseDaoPlus.save(userInfo);
    }

    public Map getMap(TUserInfo userInfo) {
        TUser user = new TUser();
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(user, userInfo, page);
        if (list.size() < 1) {
            throw new BusinessLogicException("没有该条需求记录");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("错误的筛选条件，匹配到多条记录");
        }
        return list.get(0);
    }

    /**
     * 根据app登录时生成的sessionId获取用户信息
     *
     * @param sessionId
     * @return
     */
    public TUserInfo getBySessionId(String sessionId) {
        TUser user = userService.getBySessionId(sessionId);
        TUserInfo userInfo = new TUserInfo(user.getUserId());
        userInfo = get(userInfo);
        return userInfo;
    }
}
