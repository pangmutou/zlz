package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.ExcelUtils;
import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.system.model.TLoginLog;
import com.ahnimeng.system.model.TUser;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("loginLogService")
public class LoginLogService extends BaseServiceImpl<TLoginLog> {

    public void add(TUser user, String logIp) {
        TLoginLog log = new TLoginLog(
                logIp
                , user.getUserId().toString()
                , user.getUsername()
                , new Date()
        );
        baseDaoPlus.save(log);
    }

    /**
     * 功能描述:登录日志列表
     *
     * @param page
     * @param loginlog
     * @return
     * @author houkun  2013-10-25 下午03:44:16
     */
    public List list(TLoginLog loginlog, Page<TLoginLog> page) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  log_id     AS logId, ");
        sql.append("  log_ip     AS logIp, ");
        sql.append("  user_id    AS userId, ");
        sql.append("  login_name AS loginName, ");
        sql.append("  login_time AS loginTime ");
        sql.append("FROM t_login_log ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND login_name LIKE :username ", loginlog.getLoginName());
        sql.append("      AND login_time >= :create_time ", loginlog.getLoginTime());
        return baseDaoPlus.list(sql, page);
    }

    /**
     * 功能描述:
     *
     * @param response
     * @param loginlog
     * @throws Exception
     * @author houkun  2013-11-2 上午09:01:57
     */
    public void getList(HttpServletResponse response, TLoginLog loginlog, Map<String, Object> params) throws Exception {
        List<TLoginLog> list = list(loginlog, null);
        String fileName = "登录日志";
        String[] columns = {"loginName", "logIp", "loginTime"};
        String[] headers = {"登录名", "登录IP", "登录时间"};
        ExcelUtils.exportExcel(response, fileName, list, columns, headers, SysCode.DATE_FORMAT_STR_L);
    }
}
