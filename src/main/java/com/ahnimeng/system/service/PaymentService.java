package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TPayment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhangxiansen on 2016/4/1.
 */
@Service
public class PaymentService extends BaseServiceImpl<TPayment> {


    @Autowired
    BaseDaoImpl baseDaoPlus;

    public Page page(Page page, TPayment payment) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append(" p.pay_id AS payId, ");
        sql.append(" p.pay_name AS payName ");
        sql.append(" FROM ");
        sql.append(" t_payment p ");
        sql.append(" WHERE 1=1 ");
        List list = baseDaoPlus.list(sql, page);
        if (page == null) {
            page = new Page();
        }
        page.setResult(list);
        return page;
    }

    public List list(TPayment payment) {
        List list = page(null, payment).getResult();
        return list;
    }
}
