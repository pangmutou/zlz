package com.ahnimeng.system.service;

import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.ExcelUtils;
import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.system.model.TOperationLog;
import com.ahnimeng.system.model.TUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("operationLogService")
public class OperationLogService extends BaseServiceImpl<TOperationLog> {
    @Autowired
    @Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;

    /**
     * @param name    姓名
     * @param content 内容
     * @param type    类型
     */
    public void addOperationLogSession(TUser user, String name, String content, String type) {
        TOperationLog log = new TOperationLog(
                name
                , content
                , new Date()
                , user.getUserId()
                , user.getUsername()
                , type
        );
        baseDaoPlus.save(log);
    }

    /**
     * @param tuser   当前用户
     * @param name    姓名
     * @param content 内容
     * @param type    类型
     */
    public void addOperationLogThead(final TUser tuser, final String name, final String content, final String type) {
        addOperationLogSession(tuser, name, content, type);
    }

    public void getList(HttpServletResponse response, TOperationLog operationLog, Map<String, Object> params) throws Exception {
        List<TOperationLog> list = baseDaoPlus.list(operationLog);
        String fileName = "操作日志";
        String[] headers = {"操作名", "操作内容", "操作人", "时间"};
        String[] columns = {"operationName", "operationContent", "operationUserName", "createdate"};
        ExcelUtils.exportExcel(response, fileName, list, columns, headers, SysCode.DATE_FORMAT_STR_S);
    }

    public List list(TOperationLog operationLog, Page page) {
        return baseDaoPlus.list(operationLog, page);
    }
}
