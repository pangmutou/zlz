package com.ahnimeng.system.service;

import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.*;
import com.ahnimeng.system.cache.impl.PublicCache;
import com.ahnimeng.system.model.*;
import com.ahnimeng.system.model.enhance.TAuthorityEnhance;
import com.ahnimeng.system.model.enhance.TUserEnhance;
import com.ahnimeng.system.util.HttpSessionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class UserService extends BaseServiceImpl<TUser> {

    @Autowired
    UserAccountService userAccountService;
    @Autowired
    @Qualifier("authorityService")
    private AuthorityService authorityService;
    @Autowired
    @Qualifier("userInfoService")
    private UserInfoService userInfoService;
    @Autowired
    @Qualifier("roleService")
    private RoleService roleService;
    @Autowired
    @Qualifier("loginLogService")
    private LoginLogService loginLogService;
    @Autowired
    @Qualifier("orgService")
    private OrgService orgService;
    @Autowired
    @Qualifier("operationLogService")
    private OperationLogService operationLogService;
    @Autowired
    @Qualifier("publicCache")
    private PublicCache publicCache;
    @Autowired
    private SmsService smsService;

    /**
     * 功能描述: 登陆用户
     *
     * @param loginName 登陆号
     * @param password  密码
     * @return 1-- 正常登陆 -1 用户不存在  -2 密码错误
     * @author yangliu  2013-9-11 下午04:20:03
     */
    @Transactional
    public int login(String loginName, String password, HttpServletRequest request) {
        HttpSession session = request.getSession();

        if (EmptyUtils.isEmpty(loginName)) {
            throw new BusinessLogicException("请输入用户名");
        }

        TUserInfo userInfo;
        TOrg org;

        TUser user = new TUser();
        user.setUsername(loginName);
        user = baseDaoPlus.get(user);
        if (user == null) {
            throw new BusinessLogicException("该用户不存在");
        }

        TUserEnhance userEnhance = new TUserEnhance();
        try {
            BeanUtils.copyProperties(user, userEnhance);
        } catch (Exception e) {
            throw new BusinessLogicException("获取用户数据失败");
        }

        if (user == null)
            return -1;
        if (user.getStatus() == 0)
            return -7;
        if (EmptyUtils.isEmpty(user.getOrgId()))
            return -6;
        if (!user.getPassword().equals(MD5Util.encoderByMd5(password)))
            return -2;

        userInfo = userInfoService.get(TUserInfo.class, user.getUserId());
        boolean isMobileRequest = RequestUtils.isMobileRequest(request); // true 手机请求   false pc请求
        //当顾客登录时是没有关联的机构的
        if (user.getOrgId() != 0) {
            org = orgService.get(TOrg.class, user.getOrgId());
            if (org == null || org.getDeadline() == null || org.getDeadline().before(new Date()))
                return -4;
            if (EmptyUtils.isEmpty(org.getOrgStatus()))
                return -8;
            /*
            这一条注释了，以前的售后项目分pc和mobile可访问的权限不同
            if (isMobileRequest && (user.getOrgId()!=0 && org.getAppAuth() == 0))
                return -5;*/
            userEnhance.setOrgName(org.getOrgName());
            userEnhance.setOrgType(org.getOrgType());
        }
        // 手机访问查询url不等于手机的 pc访问查询url 不等于手机
        String requestType = isMobileRequest ? SysCode.REQUEST_TYPE_MOBILE : SysCode.REQUEST_TYPE_PC;
        // 判断用户的客户端类型为 手机端 还是电脑端
        userEnhance.setRequestType(isMobileRequest ? SysCode.REQUEST_TYPE_MOBILE : SysCode.REQUEST_TYPE_PC);
        userEnhance.setRealName(userInfo.getRealName());
        // 判断是否为IOS访问
        userEnhance.setIOS(RequestUtils.isIOSRequest(request));
        // 获取用户userinfo表的id
        /*user.setUserInfoId(userInfo.getUserInfoId());*/
        // 获取用户权限
        userEnhance.setAuths(authorityService.getListTAuthByUser(user.getUserId(), Constants.AUTHORITY_TYPE_AUTH, requestType));
        List<TAuthority> leftUrls = authorityService.getListTAuthByUser(user.getUserId(), Constants.AUTHORITY_TYPE_URL, requestType);
        List<TAuthorityEnhance> leftUrlsEnhances = new ArrayList<TAuthorityEnhance>();
        for (TAuthority authority : leftUrls) {
            TAuthorityEnhance authorityEnhance = new TAuthorityEnhance();
            try {
                BeanUtils.copyProperties(authority, authorityEnhance);
            } catch (Exception e) {
                throw new BusinessLogicException("用户权限转换失败");
            }
            leftUrlsEnhances.add(authorityEnhance);
        }
        // 获取用户url
        userEnhance.setUrls(leftUrls);
        // 获取用户角色
        userEnhance.setRole(roleService.getRoleListByUserId(user.getUserId()));
        HttpSessionUtils.setUserToSession(session, userEnhance);
        // 把左边菜单放入session中
        HttpSessionUtils.setObjectToSession(session, Constants.SEESION_KEY_LEFTURLS, authorityService.getAuths(leftUrlsEnhances));
        loginLogService.add(user, request.getRemoteAddr());
        return 1;
    }

    @Transactional
    public TUser register(TUser user, TUserInfo userInfo) {
        {
            //验证用户是否已注册
            TUser source = new TUser();
            source.setUsername(user.getUsername());
            if (baseDaoPlus.isExists(source)) {
                throw new BusinessLogicException("注册失败，用户名已存在");
            }
            //如果注册的是患者,必须要医生邀请码
            if (userInfo.getIsDoctor().compareTo(0) == 0) {
                if (userInfo.getInviteCode() == null) {
                    throw new BusinessLogicException("患者注册必须使用邀请码");
                }
                try {
                    TUserInfo doctor = new TUserInfo();
                    doctor.setUserId(userInfo.getInviteCode());
                    doctor = userInfoService.get(doctor);
                    if (doctor.getIsDoctor().compareTo(1) != 0) {
                        throw new BusinessLogicException("该邀请码不正确");
                    }
                } catch (RuntimeException e) {
                    if (e instanceof BusinessLogicException) {
                        throw new BusinessLogicException("该邀请码不正确");
                    } else {
                        throw e;
                    }
                }
            }
        }

        String password = user.getPassword();
        //存用户
        user = new TUser(
                user.getUsername(),
                MD5Util.encoderByMd5(user.getPassword()),
                "",
                0,
                userInfo.getIsDoctor().compareTo(1) == 0 ? 21 : 22,
                1,
                "",
                "",
                new Date(),
                new Date(),
                user.getUsername(),
                user.getPassword()
        );
        user = baseDaoPlus.save(user);
        userInfoService.save(user, userInfo);
        userAccountService.save(user);

        //存环信用户
        HuanxinUtil.register(new HuanxinUser(
                user.getHuanxinUsername(),
                Constants.HUANXIN_DEFAULT_PASSWORD,
                userInfo.getNickname()
        ));
        return user;
    }

    /**
     * 这是一个无需原密码即可修改密码的方法，要谨慎使用
     *
     * @param username
     * @param password
     * @return
     */
    public TUser updatePassword(String username, String password) {
        TUser user = new TUser();
        user.setUsername(username);
        user = get(user);
        if (user == null) {
            throw new BusinessLogicException("用户不存在");
        }
        user.setPassword(MD5Util.encoderByMd5(password));
        user.setHuanxinPassword(Constants.HUANXIN_DEFAULT_PASSWORD);
        user = baseDaoPlus.update(user);
        //修改环信密码，如果用户有注册环信
        if (!StringUtils.isEmpty(user.getHuanxinUsername())) {
            HuanxinUser huanxinUser = new HuanxinUser(user.getHuanxinUsername(), Constants.HUANXIN_DEFAULT_PASSWORD, user.getHuanxinUsername());
            HuanxinUtil.updatePassword(huanxinUser);
        }
        return user;
    }

    /**
     * 功能描述:删除用户
     *
     * @return
     * @author L H T 2013-10-11 下午04:25:20
     */
    public void delete(Integer userId, TUser tuser) {
        //通过id查询用户信息
        TUser user = super.get(TUser.class, userId);
        if (user.getStatus() == 1) {
            user.setStatus(Constants.USER_DEL);            //冻结用户
        } else {
            user.setStatus(Constants.USER_STATUS);        //解冻用户
        }
        update(user);
        operationLogService.addOperationLogThead(tuser, "用户管理", "删除用户操作", Constants.OPERA_TYPE_USER);
    }

    public void exportExcel(HttpServletResponse response, TUserInfo userinfo) throws Exception {
        List<TUserInfo> list = userInfoService.getAll(userinfo);
        String fileName = "用户信息";
        String[] columns = {"userName", "userTelephone", "userEmail"};
        String[] headers = {"用户姓名", "手机号码", "电子邮箱"};
        ExcelUtils.exportExcel(response, fileName, list, columns, headers, SysCode.DATE_FORMAT_STR_S);
    }

    @Transactional
    public TUser appLogin(TUser user, TUserInfo userInfo, String captcha) {
        if (!StringUtils.isEmpty(captcha)) {
            //如果有短信,则用短信登录
            //获取最后一条短信
            TSms sms = smsService.getLast(user.getUsername());
            String lastCaptcha = sms == null ? "" : sms.getFlag1();
            if (lastCaptcha.equals(captcha)) {
                String loginName = user.getUsername(),
                        password = user.getPassword();
                user = baseDaoPlus.get(user);
                if (user == null) {
                    user = register(user, userInfo);
//                    throw new BusinessLogicException("登录失败,用户名不存在");
                }
            } else {
                throw new BusinessLogicException("短信登录,验证失败");
            }
        } else {
            user.setPassword(MD5Util.encoderByMd5(user.getPassword()));
            user = baseDaoPlus.get(user);
            if (user == null) {
                throw new BusinessLogicException("登录失败,用户名或密码有误");
            }
        }
        user.setSessionId(UUID.randomUUID().toString());
        user = super.update(user);
        return user;
    }

    @Transactional
    public Map appLogin(TUser user) {
        user.setPassword(MD5Util.encoderByMd5(user.getPassword()));
        user = baseDaoPlus.get(user);
        if (user == null) {
            throw new BusinessLogicException("用户名或密码错误");
        }
        user.setSessionId(UUID.randomUUID().toString());
        TUser source = baseDaoPlus.update(user);
        Map map = userInfoService.getDetailByUserId(source.getUserId());
        map.put("sessionId", user.getSessionId());
        return map;
    }

    public void passwordReset(TUser user) {
        user = get(TUser.class, user.getUserId());
        user.setPassword(Constants.USER_DEFULT_PASSWORD);
        super.update(user);
    }

    /**
     * 根据app登录时生成的sessionId获取用户信息
     *
     * @param sessionId
     * @return
     */
    public TUser getBySessionId(String sessionId) {
        if (EmptyUtils.isEmpty(sessionId)) {
            throw new BusinessLogicException("请先登录", "2");
        }
        TUser user = new TUser();
        user.setSessionId(sessionId);
        user = get(user);

        if (EmptyUtils.isEmpty(user)) {
            throw new BusinessLogicException("登录信息无效,请重新登录", "2");
        }
        return user;
    }

    public int validateUserKeyInfo(TUser user) {
        user.setPassword(MD5Util.encoderByMd5(user.getPassword()));
        user = baseDaoPlus.get(user);
        if (user == null) {
            logger.info("用户名或密码错误");
            return 0;
        } else {
            return 1;
        }
    }

    public TUser get(TUser user) {
        return baseDaoPlus.get(user);
    }
}