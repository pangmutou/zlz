package com.ahnimeng.system.service;

import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TUserAccount;
import com.ahnimeng.system.model.TUserAccountFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by funcong-tp on 2016-07-19.
 */
@Service
public class UserAccountFlowService extends BaseServiceImpl<TUserAccountFlow> {
    public static final String PAY_SUCCESS = "PAY_SUCCESS";
    public static final String COMFIRM_RECEIPT = "COMFIRM_RECEIPT";
    @Autowired
    UserAccountService userAccountService;

    public TUserAccountFlow save(Integer userId, BigDecimal money, BigDecimal freezingMoney, String flowType, String flowSource, String flowRemark) {
        //更新用户账户
        TUserAccount userAccount = userAccountService.get(TUserAccount.class, userId);
        userAccount.setMoney(userAccount.getMoney().add(money));
        userAccount.setFreezingMoney(userAccount.getFreezingMoney().add(freezingMoney));
        userAccountService.update(userAccount);

        //存流水
        TUserAccountFlow userAccountFlow = new TUserAccountFlow(
                money,
                freezingMoney,
                userAccount.getMoney(),
                userAccount.getFreezingMoney(),
                flowType,
                flowSource,
                flowRemark,
                new Date()
        );
        return super.save(userAccountFlow);
    }
}
