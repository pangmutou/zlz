package com.ahnimeng.system.service;

public enum BusinessOrderStatusEnum {
    newOrder(0),       //0新订单
    isPaid(1),         //1已付款
    isTransport(22),    //2已发货
    isFinish(3),       //3交易成功
    isComment(4);       //4已评价

    private int value = -1;

    BusinessOrderStatusEnum(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}