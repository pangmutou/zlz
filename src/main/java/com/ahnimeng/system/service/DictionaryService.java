package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.dao.impl.BaseDaoImpl;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.system.model.TDictionary;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by funcong on 2015/9/6.
 */
@Service
public class DictionaryService extends BaseServiceImpl<TDictionary> {

    @Autowired
    BaseDaoImpl baseDaoPlus;

    public List<TDictionary> getAvailable() {
        List list = list(TDictionary.class
                , new Criterion[]{Restrictions.eq("status", 1)}
                , new Order[]{Order.asc("parentCode"), Order.asc("orderby")}
                , Integer.MIN_VALUE, Integer.MAX_VALUE
        );
        return list;
    }

    public TDictionary get(String parentCode, String itemCode) {
        TDictionary dictionary = new TDictionary();
        dictionary.setParentCode(parentCode);
        dictionary.setItemCode(itemCode);
        return baseDaoPlus.get(dictionary);
    }

    public TDictionary getByFlag1(String flag1) {
        TDictionary dictionary = new TDictionary();
        dictionary.setFlag1(flag1);
        return baseDaoPlus.get(dictionary);
    }

    /**
     * 获取店铺类目
     *
     * @return
     */
    public List listFormOrgCategory() {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT * ");
        sql.append("FROM t_dictionary ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND parent_code = 'ORG_CATEGORY' ");
        sql.append("      AND status = 1 ");
        sql.append("ORDER BY orderby ASC ");
        return baseDaoPlus.list(sql.toString(), sql.getParam());
    }

    public TDictionary saveCategory(TDictionary dictionary) {
        TDictionary target = get("ORG_CATEGORY", dictionary.getItemCode());
        if (target != null)
            throw new RuntimeException("objectExists");

        dictionary = new TDictionary("ORG_CATEGORY", "店铺分类",
                dictionary.getItemCode(),
                dictionary.getItemName(),
                dictionary.getOrderby(),
                1,
                null);
        return super.save(dictionary);
    }

    public TDictionary updateCategory(TDictionary dictionary) {
//        TDictionary target = get("ORG_CATEGORY", dictionary.getItemCode());
//        if (target != null)
//            throw new RuntimeException("objectExists");
        TDictionary source = get(TDictionary.class, dictionary.getId());
        source.setItemName(dictionary.getItemName());
        source.setItemCode(dictionary.getItemCode());
        source.setOrderby(dictionary.getOrderby());
        return super.update(source);
    }

    public int deleteCategory(TDictionary obj) {
        TDictionary dictionary = baseDaoPlus.get(obj);
        if (!dictionary.getParentCode().equals("ORG_CATEGORY")) {
            throw new RuntimeException("-3");
        }
        dictionary.setStatus(0);
        baseDaoPlus.update(dictionary);
        return 1;
    }

}
