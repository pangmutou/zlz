package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.EmptyUtils;
import com.ahnimeng.common.util.UniqueNumberUtil;
import com.ahnimeng.system.model.TBusinessOrder;
import com.ahnimeng.system.model.TBusinessOrderGoods;
import com.ahnimeng.system.model.TGoods;
import com.ahnimeng.system.model.TUserInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
public class BusinessOrderService extends BaseServiceImpl<TBusinessOrder> {

    @Autowired
    GoodsService goodsService;
    @Autowired
    BusinessOrderGoodsService businessOrderGoodsService;
//    @Autowired
//    TradeOrderService tradeOrderService;
    @Autowired
UserInfoService userInfoService;

    public Map getMap(TBusinessOrder businessOrder) {
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(businessOrder, page, null, null);
        if (list.size() == 0) {
            throw new BusinessLogicException("没有获取到该订单");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("数据有误，获取到多条订单");
        }
        Map orderMap = list.get(0);
//        TBusinessOrderGoods orderGoods = new TBusinessOrderGoods();
//        orderGoods.setOrderSn(orderMap.get("orderSn").toString());
//        List<Map> goodsList = businessOrderGoodsService.list(orderGoods, null);
//        orderMap.put("goods", goodsList);
        return orderMap;
    }

    public TBusinessOrder get(TBusinessOrder businessOrder) {
        Map map = getMap(businessOrder);
        businessOrder = new TBusinessOrder();
        businessOrder.setBusinessOrderId(Integer.parseInt(map.get("businessOrderId").toString()));
        businessOrder = baseDaoPlus.get(businessOrder);
        if (businessOrder == null) {
            throw new BusinessLogicException("该业务订单不存在");
        }
        return businessOrder;
    }

    public List<Map> list(TBusinessOrder businessOrder, Page page, String buyerUsername, String sellerUsername) {
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT ");
        sql.append("  tbo.business_order_id  AS businessOrderId, ");
        sql.append("  tbo.order_sn           AS orderSn, ");
        sql.append("  tbo.buyer_user_id      AS buyerUserId, ");
        sql.append("  tbo.seller_user_id     AS sellerUserId, ");
        sql.append("  tbo.type               AS type, ");
        sql.append("  tbo.status             AS status, ");
        sql.append("  tbo.is_paid            AS isPaid, ");
        sql.append("  tbo.is_finish          AS isFinish, ");
        sql.append("  tbo.money              AS money, ");
        sql.append("  tbo.title              AS title, ");
        sql.append("  tbo.content            AS content, ");
        sql.append("  tbo.image              AS image, ");
        sql.append("  tbo.create_time        AS createTime, ");
        sql.append("  tbo.buyer_has_comment  AS buyerHasComment, ");
        sql.append("  tbo.seller_has_comment AS sellerHasComment, ");
        sql.append("  buyer.mobile           AS buyerUsername, ");
        sql.append("  buyer.photo            AS buyerPhoto, ");
        sql.append("  seller.mobile          AS sellerUsername, ");
        sql.append("  seller.photo           AS sellerPhoto ");
        sql.append("FROM t_business_order tbo ");
        sql.append("    LEFT JOIN t_user_info buyer ON buyer.user_id = tbo.buyer_user_id ");
        sql.append("    LEFT JOIN t_user_info seller ON seller.user_id = tbo.seller_user_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("      AND buyer.mobile  = :buyer.mobile ", StringUtils.isEmpty(buyerUsername) ? null : buyerUsername);
        sql.append("      AND seller.mobile = :seller.mobile ", StringUtils.isEmpty(sellerUsername) ? null : sellerUsername);
        sql.append("      AND tbo.business_order_id = :tbo.business_order_id ", businessOrder.getBusinessOrderId());
        sql.append("      AND tbo.order_sn = :tbo.order_sn ", businessOrder.getOrderSn());
        sql.append("      AND tbo.buyer_user_id = :tbo.buyer_user_id ", businessOrder.getBuyerUserId());
        sql.append("      OR  tbo.seller_user_id = :tbo.seller_user_id ", businessOrder.getSellerUserId());
        sql.append("      AND tbo.type = :tbo.type ", businessOrder.getType());
        sql.append("      AND tbo.status = :tbo.status ", businessOrder.getStatus());
        sql.append("      AND tbo.is_paid = :tbo.is_paid ", businessOrder.getIsPaid());
        sql.append("      AND tbo.is_finish = :tbo.is_finish ", businessOrder.getIsFinish());
        sql.append("      AND tbo.money = :tbo.money ", businessOrder.getMoney());
        sql.append("      AND tbo.title = :tbo.title ", businessOrder.getTitle());
        sql.append("      AND tbo.content = :tbo.content ", businessOrder.getContent());
        sql.append("      AND tbo.image = :tbo.image ", businessOrder.getImage());
        sql.append("      AND tbo.create_time = :tbo.create_time ", businessOrder.getCreateTime());
        sql.append("      AND tbo.buyer_has_comment = :tbo.buyer_has_comment ", businessOrder.getBuyerHasComment());
        sql.append("      AND tbo.seller_has_comment = :tbo.seller_has_comment ", businessOrder.getSellerHasComment());
        sql.append(" ORDER BY tbo.create_time DESC");
        List<Map> orderList = baseDaoPlus.list(sql, page);
        //根据orderSn，插入多个商品
        StringBuilder orderIdArray = new StringBuilder();
        for (int i = 0; i < orderList.size(); i++) {
            orderIdArray.append(orderList.get(i).get("orderSn"));
            if (i < orderList.size() - 1) orderIdArray.append(",");
        }
        TBusinessOrderGoods businessOrderGoods = new TBusinessOrderGoods();
        businessOrderGoods.setOrderSn(orderIdArray.toString());
        List<Map> orderGoodsList = businessOrderGoodsService.list(businessOrderGoods, null);
        for (int i = 0; i < orderList.size(); i++) {
            Map order = orderList.get(i);
            List<Map> filterGoodsList = new ArrayList<Map>();
            Iterator<Map> orderGoodsIterator = orderGoodsList.iterator();
            while (orderGoodsIterator.hasNext()) {
                Map orderGoods = orderGoodsIterator.next();
                if (orderGoods.get("orderSn").equals(order.get("orderSn"))) {
                    filterGoodsList.add(orderGoods);
                }
                orderGoodsIterator.remove();
            }
            order.put("goods", filterGoodsList);
        }
        return orderList;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public TBusinessOrder save(TBusinessOrder businessOrder, List<TBusinessOrderGoods> orderGoodsList) {
        if (!(businessOrder.getType() >= 0 && businessOrder.getType() < 2)) {
            throw new BusinessLogicException("未知的订单类型");
        }
//        BusinessOrderStatusEnum.isComment.value()
        String orderType = String.format("%02d", businessOrder.getType() + 1);
        TBusinessOrder target = new TBusinessOrder(
                UniqueNumberUtil.create(orderType),
                businessOrder.getBuyerUserId(),
                businessOrder.getSellerUserId(),
                businessOrder.getType(),
                BusinessOrderStatusEnum.newOrder.value(),
                0,
                0,
                null,
                EmptyUtils.isEmpty(businessOrder.getTitle()) ? "" : businessOrder.getTitle(),
                businessOrder.getContent(),
                businessOrder.getImage(),
                new Date(),
                0,
                0,
                "",
                ""
        );

        //是咨询订单还是购物订单，最好让前台传一下
        boolean isAdvice;
        BigDecimal total = BigDecimal.ZERO;
        StringBuilder orderContent = new StringBuilder();
        for (int i = 0; i < orderGoodsList.size(); i++) {
            TBusinessOrderGoods orderGoods = orderGoodsList.get(i);
            TGoods goods = new TGoods();
            goods.setGoodsId(orderGoods.getGoodsId());
            Map map = goodsService.get(goods);
            goods = baseDaoPlus.get(goods);
            isAdvice = goods.getGoodsType().intValue() == 0 ? true : false;
            orderGoods = new TBusinessOrderGoods(
                    target.getOrderSn(),
                    goods.getGoodsId(),
                    goods.getGoodsName(),
                    goods.getGoodsSn(),
                    isAdvice ? 1 : orderGoods.getGoodsNumber(),
                    goods.getMarketPrice(),
                    orderGoods.getGoodsPrice(),
                    "N/A",
                    goods.getGoodsImg(),
                    0,
                    goods.getIsReal(),
                    "N/A",
                    0,
                    0,
                    "N/A",
                    goods.getUserId()
            );
            businessOrderGoodsService.save(orderGoods);
            total = total.add(goods.getShopPrice().multiply(new BigDecimal(orderGoods.getGoodsNumber())));
            if (isAdvice) {
                target.setSellerUserId(goods.getUserId());
                orderContent.append(goods.getGoodsName());
                break;
            }
            orderContent.append(orderGoods.getGoodsName()).append("*").append(orderGoods.getGoodsNumber()).append(i < orderGoodsList.size() - 1 ? "" : ",");
        }
        target.setMoney(total);
        target.setContent(orderContent.toString());
        target = baseDaoPlus.save(target);
        return target;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Map pay(String businessOrderSn) {
        TBusinessOrder businessOrder = new TBusinessOrder();
        businessOrder.setOrderSn(businessOrderSn);
        businessOrder = get(businessOrder);
//        TTradeOrder tradeOrder = tradeOrderService.save(businessOrder);
        Map map = new HashMap();
//        map.put("orderSn", tradeOrder.getTradeOrderSn());
//        map.put("subject", tradeOrder.getSubject());
//        map.put("body", tradeOrder.getBody());
//        map.put("total_fee", tradeOrder.getTotalFee());
        return map;
    }

    /**
     * 上传处方
     *
     * @param businessOrder
     * @return
     */
    @Transactional
    public TBusinessOrder prescriptionUpload(TBusinessOrder businessOrder) {
        TBusinessOrder source = new TBusinessOrder();
        source.setSellerUserId(businessOrder.getSellerUserId());
        source = get(source);

        if (source.getIsPaid() == 0) {
            throw new BusinessLogicException("订单尚未完成支付");
        } else if (source.getStatus() != BusinessOrderStatusEnum.isPaid.value()) {
            throw new BusinessLogicException("订单当前状态不允许开处方");
        }
        source.setFlag1(businessOrder.getFlag1());
        return update(source);
    }

    /**
     * 结束订单，对订单表的几个状态修改的方法
     *
     * @param businessOrder
     * @return
     */
    public TBusinessOrder finish(TBusinessOrder businessOrder) {
        TBusinessOrder source = get(businessOrder);
        source.setIsFinish(1);
        source.setStatus(BusinessOrderStatusEnum.isFinish.value());
        source = baseDaoPlus.update(source);
        //如果是咨询订单，还要修改用户成交数
        if (source.getType().intValue() == 1) {
            TUserInfo userInfo = new TUserInfo();
            userInfo.setUserId(source.getSellerUserId());
            userInfo = userInfoService.get(userInfo);
            userInfo.setTradeCount(userInfo.getTradeCount() + 1);
            baseDaoPlus.update(userInfo);
        }
        return source;
    }

}
