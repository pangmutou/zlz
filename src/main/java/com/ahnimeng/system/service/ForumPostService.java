package com.ahnimeng.system.service;

import com.ahnimeng.common.dao.SqlBuffer;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.common.util.EmptyUtils;
import com.ahnimeng.system.model.TForumPost;
import com.ahnimeng.system.model.TForumPostComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangxiansen on 2016-10-13.
 */
@Service
public class ForumPostService extends BaseServiceImpl<TForumPost> {
    @Autowired
    private ForumPostCommentService forumPostCommentService;

    public List list(TForumPost post, Page page) {
//        String areaCode = post.getAreaCode();
//        if (!StringUtils.isEmpty(areaCode)) {
//            areaCode=areaCode.replaceAll("00","%");
//        }
        SqlBuffer sql = new SqlBuffer();
        sql.append("SELECT DISTINCT ");
        sql.append("  fp.post_id          AS postId, ");
        sql.append("  fp.brief_content    AS briefContent, ");
        sql.append("  fp.content          AS content, ");
        sql.append("  fp.image            AS image, ");
        sql.append("  fp.user_id          AS userId, ");
        sql.append("  fp.is_show          AS isShow, ");
        sql.append("  fp.create_time      AS createTime, ");
        sql.append("  tui.real_name       AS realName, ");
        sql.append("  tui.nickname        AS nickname, ");
        sql.append("  tui.mobile          AS mobile, ");
        sql.append("  tui.photo           AS photo ");
        sql.append("FROM t_forum_post fp ");
        sql.append("  LEFT JOIN t_user_info tui ON tui.user_id = fp.user_id ");
        sql.append("WHERE 1 = 1 ");
        sql.append("  AND fp.post_id = :post_id ", post.getPostId());
        sql.append("  AND fp.brief_content LIKE :brief_content ", EmptyUtils.isEmpty(post.getBriefContent()) ? null : "%" + post.getBriefContent() + "%");
        sql.append("  AND fp.content LIKE :content ", EmptyUtils.isEmpty(post.getContent()) ? null : "%" + post.getContent() + "%");
        sql.append("  AND fp.user_id = :user_id ", EmptyUtils.isNotEmpty(post.getUserId()) && post.getUserId() > 0 ? post.getUserId() : null);
        sql.append("  AND fp.is_show = :is_show ", post.getIsShow());
        sql.append("  AND fp.create_time = :create_time ", post.getCreateTime());
        List list = baseDaoPlus.list(sql, page);
        return list;
    }

    public TForumPost get(TForumPost forumPost) {
        Map post = getMap(forumPost);
        TForumPost source = new TForumPost();
        source.setPostId(Integer.parseInt(post.get("postId").toString()));
        return baseDaoPlus.get(source);
    }

    public Map getMap(TForumPost forumPost) {
        Page page = new Page();
        page.setPageSize(2);
        List<Map> list = list(forumPost, page);
        if (list.size() < 1) {
            throw new BusinessLogicException("没有该条需求记录");
        } else if (list.size() > 1) {
            throw new BusinessLogicException("错误的筛选条件，匹配到多条记录");
        }
        return list.get(0);
    }

    public TForumPost save(TForumPost forumPost) {
        TForumPost post = new TForumPost(
                forumPost.getBriefContent(),
                forumPost.getContent(),
                forumPost.getImage(),
                forumPost.getUserId(),
                1,
                new Date()
        );
        post = baseDaoPlus.save(post);

        //当发布时，需要在comment表中也新增上这条记录
        TForumPostComment comment = new TForumPostComment(
                post.getPostId(),
                post.getUserId(),
                post.getContent(),
                post.getImage(),
                1,
                new Date()
        );
        //此处无需和常规回复一样走流程
        baseDaoPlus.save(comment);
        return post;
    }

}