package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class SystemLoginController extends BaseController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String toLogin() {
        session.invalidate();
        return "pc/admin/login_new";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ModelAndView doLogin(@Validated(SystemLoginController.ValidUserLogin.class) TUser user, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        int result = userService.login(user.getUsername(), user.getPassword(), request);
        if (result == SUCCESS) {
            modelAndView.setViewName(REDIRECT + "/index");
        } else {
            modelAndView.addObject(MSG, result);
            modelAndView.setViewName(REDIRECT + "/login");
        }
        return modelAndView;
    }


    /**
     * 功能描述:退出当前系统
     *
     * @return
     * @author lht  2013-9-5 上午09:39:16
     */
    @RequestMapping("loginout")
    public String loginOut(HttpSession session) {
        //让SESSION失效
        session.invalidate();
        return REDIRECT + "/login";
    }

    /**
     * 功能描述:404跳转路径
     *
     * @return
     * @author houkun  2013-11-23 上午09:55:40
     */
    @RequestMapping("notFound")
    public String notFound() {
        return "pc/admin/notfound";
    }

    /**
     * 功能描述:500/503跳转路径
     *
     * @return
     * @author houkun  2013-11-23 上午09:56:28
     */
    @RequestMapping("toError")
    public String error() {
        return "pc/admin/error";
    }

    public interface ValidUserLogin {
    }
}
