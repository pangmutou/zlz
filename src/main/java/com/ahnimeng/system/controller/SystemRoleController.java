package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.system.model.TAuthority;
import com.ahnimeng.system.model.TRole;
import com.ahnimeng.system.service.AuthorityService;
import com.ahnimeng.system.service.RoleService;
import com.ahnimeng.system.util.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * #(c) ahnimeng shouhou <br/>
 * <p>
 * 版本说明: $id:$ <br/>
 * <p>
 * 功能说明: 角色管理
 * <p>
 * <br/>创建说明: 2013-9-16 下午07:07:27 yangliu  创建文件<br/>
 * <p>
 * 修改历史:<br/>
 */
@Controller
@RequestMapping("role")
public class SystemRoleController extends BaseController {

    @Autowired
    @Qualifier("roleService")
    private RoleService roleService;

    @Autowired
    @Qualifier("authorityService")
    private AuthorityService authorityService;

    @RequestMapping("list")
    public String roles(Model model) {
        page.setOrderBy("orderby");
        page.setOrder("DESC");
        addModel(model, "roles", roleService.list(new TRole(), page));
//        addModel(model, "roles", roleService.getAll(TRole.class, "orderby", ICommonHqlDao.ORDER_DESC));
        return "pc/admin/role_list";
    }

    /**
     * 功能描述:修改角色信息
     *
     * @param roleId 角色ID
     * @param model
     * @return
     * @author yangliu  2013-9-25 上午11:23:08
     */
    @RequestMapping("edit")
    public String toEdit(Integer roleId, Model model) {
        TRole role = roleService.get(TRole.class, roleId);
        addModel(model, "role", role);
        List list = authorityService.getListAuthBindingRoleId(roleId);
        addModel(model, "authList", list);
        return "pc/admin/role";
    }

    /**
     * 功能描述:添加角色信息
     *
     * @param model
     * @return
     * @author yangliu  2013-9-25 上午11:31:32
     */
    @RequestMapping("add")
    public String toAdd(Model model) {
        page.setOrderBy("authOrderby");
        page.setOrder("DESC");
        addModel(model, "authList", authorityService.list(new TAuthority(), page));
        return "pc/admin/role";
    }

    /**
     * 功能描述:保存角色信息
     *
     * @param model    model
     * @param role     角色
     * @param authCode 权限code  格式(A_1000,A_1001)
     * @return
     * @author yangliu  2013-9-25 下午01:51:21
     */
    @RequestMapping("saveOrUpdate")
    public String saveOrUpdate(Model model, TRole role, String authCode, HttpSession session) {
        addModel(model, MSG, roleService.saveOrUpdateRole(role, authCode, HttpSessionUtils.getCurrentUser(session)));
        return REDIRECT + "/role/list";
    }

    /**
     * 功能描述:删除角色
     *
     * @return
     * @author houkun  2013-10-11 下午02:54:25
     */
    @RequestMapping("del")
    public String doDelete(Integer roleId, HttpSession session) {
        roleService.deleteRole(roleId, HttpSessionUtils.getCurrentUser(session));
        return REDIRECT + "/role/list";
    }
}
