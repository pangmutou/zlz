package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.common.util.MD5Util;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.service.SmsService;
import com.ahnimeng.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("app/user")
public class AppUserController extends BaseController {

    @Autowired
    UserService userService;
    @Autowired
    SmsService smsService;

    @RequestMapping("update/password")
    public void doUpdatePassword(HttpServletResponse response, int userId, String password, String newPassword) {
        TUser user = userService.get(TUser.class, userId);
        if (user == null) {
            throw new BusinessLogicException("用户不存在");
        }
        if (!user.getPassword().equals(MD5Util.encoderByMd5(password))) {
            throw new BusinessLogicException("原密码错误，无法修改");
        }
        user.setPassword(MD5Util.encoderByMd5(newPassword));
        userService.update(user);
        super.writeJsonData(response, new AppCommonModel(1, "修改成功"));
    }

    @RequestMapping("forget/password")
    public void doForgetPassword(HttpServletResponse response, String loginName, String newPassword, String captcha) {
        AppCommonModel appCommonModel = null;
        if (smsService.isValid(loginName, captcha)) {
            appCommonModel = new AppCommonModel(1, "密码修改成功", userService.updatePassword(loginName, newPassword));
        }
        super.writeJsonData(response, appCommonModel);
    }
}
