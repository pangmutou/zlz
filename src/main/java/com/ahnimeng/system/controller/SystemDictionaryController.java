package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.system.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by funcong-tp on 2016-07-20.
 */
@Controller
@RequestMapping("dictionary")
public class SystemDictionaryController extends BaseController {

    @Autowired
    DictionaryService dictionaryService;

}
