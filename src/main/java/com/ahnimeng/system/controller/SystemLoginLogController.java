package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TLoginLog;
import com.ahnimeng.system.service.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping("loginLog")
public class SystemLoginLogController extends BaseController {

    @Autowired
    @Qualifier("loginLogService")
    private LoginLogService loginLogService;

    @InitBinder
    public void initBinders(WebDataBinder binder) {
        super.initBinder(binder, "yyyy-MM-dd", true);
    }

    /**
     * 功能描述:登录日志列表
     *
     * @param model
     * @param page
     * @param loginlog
     * @return
     * @author houkun  2013-10-25 下午04:08:41
     */
    @RequestMapping("list")
    public String toList(Model model, Page<TLoginLog> page, TLoginLog loginlog) {
        loginLogService.list(loginlog, page);
        addModel(model, "pageList", page);
        addModel(model, "loginlog", loginlog);
        return "pc/admin/login_log_list";
    }

    /**
     * 功能描述:导出Excel
     *
     * @param response
     * @param loginlog
     * @throws Exception
     * @author houkun  2013-11-2 上午11:09:02
     */
    @RequestMapping("exportExcel")
    public void exportExcel(HttpServletResponse response, TLoginLog loginlog, Map<String, Object> params) throws Exception {
        loginLogService.getList(response, loginlog, params);
    }

}
