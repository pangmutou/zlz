package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TOperationLog;
import com.ahnimeng.system.service.OperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping("operationLog")
public class SystemOperationLogController extends BaseController {

    @Autowired
    @Qualifier("operationLogService")
    private OperationLogService operationLogService;

    @InitBinder
    public void initBinders(WebDataBinder binder) {
        super.initBinder(binder, "yyyy-MM-dd", true);
    }

    /**
     * 功能描述:返回操作日志列表
     *
     * @param model
     * @param page
     * @param operationLog
     * @return
     * @author houkun  2013-11-4 下午03:04:24
     */
    @RequestMapping("list")
    public String getList(Model model, Page page, TOperationLog operationLog) {
        operationLogService.list(operationLog, page);
        model.addAttribute("page", page);
        model.addAttribute("operationlog", operationLog);
//        addModel(model, "page", page);
//        addModel(model, "operationlog", operationLog);
        return "pc/admin/operation_log_list";
    }

    /**
     * 功能描述:导出Excel
     *
     * @param response
     * @param operationLog
     * @param params
     * @throws Exception
     * @author houkun  2013-11-4 下午03:04:13
     */
    @RequestMapping("exportExcel")
    public void exportExcel(HttpServletResponse response, TOperationLog operationLog, Map<String, Object> params) throws Exception {
        operationLogService.getList(response, operationLog, params);
    }
}
