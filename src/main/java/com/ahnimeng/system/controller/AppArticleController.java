package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TArticle;
import com.ahnimeng.system.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by funcong on 2016/2/14.
 */
@Controller
@RequestMapping("app/article")
public class AppArticleController extends BaseController {

    @Autowired
    ArticleService articleService;

    @RequestMapping("list")
    public void toList(HttpServletRequest request, HttpServletResponse response, Page page, TArticle article) {
        List list = articleService.list(article);
        page.setResult(list);
        super.writeJsonData(response, new AppCommonModel(SUCCESS, "查询成功", list));
    }

}
