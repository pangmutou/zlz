package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.util.FileUtils;
import com.ahnimeng.system.service.OrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class ExampleController extends BaseController {

    @Autowired
    private OrgService orgService;

    @RequestMapping("/example")
    public String index() {
        return "pc/admin/example";
    }

    @RequestMapping("example/upload")
    public ModelAndView doUpload(HttpServletResponse response, HttpServletRequest request, @RequestParam(required = false) MultipartFile[] files) {
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("pc/index");
        FileUtils.saveFile(files[0], "d:/img/", files[0].getOriginalFilename());
        Map hashMap = new HashMap();
        hashMap.put("msg", files[0].getOriginalFilename());
        hashMap.put("success", 1);
        hashMap.put("result", 1);
        super.writeJsonData(response, hashMap);
        return null;
    }

}
