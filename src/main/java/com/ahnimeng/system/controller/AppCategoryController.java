package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.system.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zhangxiansen on 2016/4/1.
 */
@Controller
@RequestMapping("app/category")
public class AppCategoryController extends BaseController {
    @Autowired
    private CategoryService categoryService;


}
