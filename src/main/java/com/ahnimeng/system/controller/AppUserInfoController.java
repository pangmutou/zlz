package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.system.model.TUserInfo;
import com.ahnimeng.system.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by funcong-tp on 2016-06-14.
 */
@Controller
@RequestMapping("app/user/info")
public class AppUserInfoController extends BaseController {

    @Autowired
    UserInfoService userInfoService;

    @RequestMapping("update")
    public void doUpdate(HttpServletResponse response, TUserInfo userInfo) {
        userInfo = userInfoService.update(null, userInfo);
        super.writeJsonData(response, new AppCommonModel(1, "success", userInfo));
    }
}
