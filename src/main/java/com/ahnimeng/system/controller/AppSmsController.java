package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.service.SmsService;
import com.ahnimeng.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("app/sms")
public class AppSmsController extends BaseController {

    @Autowired
    UserService userService;
    @Autowired
    private SmsService smsService;

    @RequestMapping("captcha")
    public void toCaptcha(HttpServletResponse response, String phone, String type) {
        if (!(type != null && type.equalsIgnoreCase("forgetPassword"))) {
            TUser user = new TUser();
            user.setUsername(phone);
            user = userService.get(user);
            if (user != null)
                throw new BusinessLogicException("该用户已经注册过了");
        }
        smsService.generateSmsCode(phone);
        super.writeJsonData(response, new AppCommonModel(1, "验证码发送成功"));
    }

}