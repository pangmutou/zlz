package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zhangxiansen on 2016/4/17.
 */
@Controller
@RequestMapping("article/cat")
public class SystemArticleCatController extends BaseController {

}
