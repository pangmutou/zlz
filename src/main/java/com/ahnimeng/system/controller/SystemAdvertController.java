package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TAdvert;
import com.ahnimeng.system.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("advert")
public class SystemAdvertController extends BaseController {

    @Autowired
    AdvertService advertService;

    @RequestMapping("list")
    public String toList(Model model, Page page, TAdvert advert) {
        advertService.page(page, advert);
        model.addAttribute("page", page);
        model.addAttribute("object", advert);
        return "pc/admin/advert_list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String toAdd(Model model, Page page, TAdvert advert) {
        return "pc/admin/advert";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String doAdd(HttpServletRequest request, Model model, Page page, TAdvert advert) {
        advertService.save(advert);
        return REDIRECT + "/advert/page?advertLayout=" + advert.getAdvertLayout();
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String toEdit(Model model, Page page, TAdvert advert) {
        model.addAttribute("object", advertService.get(TAdvert.class, advert.getAdvertId()));
        model.addAttribute("method", "edit");
        return "pc/admin/advert";
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String doEdit(Model model, HttpServletRequest request, TAdvert advert) {
        advertService.update(advert);
        return REDIRECT + "/advert/page?advertLayout=" + advert.getAdvertLayout();
    }

    @RequestMapping(value = "delete")
    public String doDelete(Model model, HttpServletRequest request, TAdvert advert) {
        advert = advertService.get(TAdvert.class, advert.getAdvertId());
        advertService.delete(advert);
        return REDIRECT + "/advert/page?advertLayout=" + advert.getAdvertLayout();
    }
}
