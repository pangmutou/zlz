package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.transaction.alipay.config.AlipayConfig;
import com.ahnimeng.system.service.DictionaryService;
import com.ahnimeng.system.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by funcong-tp on 2016-05-19.
 */
@Controller
@RequestMapping("transaction")
public class SystemTransactionController extends BaseController {

    @Autowired
    DictionaryService dictionaryService;
    @Autowired
    TransactionService transactionService;

    {
        AlipayConfig.partner_id = "2088421399073507";//沈奔飞的
        AlipayConfig.seller_id = "106747261@qq.com";//沈奔飞的
        AlipayConfig.notify_url = "http://121.40.202.253/transaction/notify";//沈奔飞的
//        AlipayConfig.notify_url = "http://www.ahnimeng.com:8088/transaction/notify";
//        AlipayConfig.notify_url = "http://124.73.37.212:92/transaction/notify";
    }

    @RequestMapping("notify")
    public void toNotifyCallback(HttpServletRequest request, HttpServletResponse response) {
        String result = transactionService.notifyResponse(request.getParameterMap()).toString();
        super.writeText(response, result);
    }

    @RequestMapping("batchTrans")
    public ModelAndView toBatchTrans(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        return modelAndView;
    }
}
