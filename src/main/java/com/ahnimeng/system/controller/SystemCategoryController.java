package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TCategory;
import com.ahnimeng.system.service.CategoryService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("category")
public class SystemCategoryController extends BaseController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping("list")
    public String toList(Model model, Page page, TCategory category) {
        model.addAttribute("page", categoryService.page(page, category));
        return "pc/admin/category_list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String toAdd(Model model, TCategory category) {
        model.addAttribute("methodName", "添加");
        model.addAttribute("url", "category/add");

        List list = categoryService.list(category);
        model.addAttribute("categoryList", JSONArray.fromObject(list));
        return "pc/admin/category";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String doAdd(TCategory category) {
        categoryService.save(category);
        return REDIRECT + "/category/list";
    }

    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String toUpdate(Model model, TCategory category) {
        model.addAttribute("methodName", "修改");
        model.addAttribute("url", "category/update");
        model.addAttribute("category", categoryService.get(TCategory.class, category.getCatId()));

        List list = categoryService.list(category);
        model.addAttribute("categoryList", JSONArray.fromObject(list));
        return "pc/admin/category";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String doUpdate(TCategory category) {
        categoryService.update(category);
        return REDIRECT + "/category/list";
    }

    @RequestMapping("detail")
    public String detail(Model model, TCategory category) {
        model.addAttribute("methodName", "查看");
        model.addAttribute("category", categoryService.get(TCategory.class, category.getCatId()));
        return "pc/admin/category";
    }

    @RequestMapping("delete")
    public String delete(TCategory category) {
        categoryService.delete(category);
        return REDIRECT + "/category/list";
    }
}
