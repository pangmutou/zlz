package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TOrg;
import com.ahnimeng.system.service.OrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016-01-29.
 */
@Controller
@RequestMapping("app/org")
public class AppOrgController extends BaseController {

    @Autowired
    private OrgService orgService;

    @RequestMapping("list")
    public void toList(HttpServletResponse response, Page page, TOrg org) {
        List list = orgService.page(page, org).getResult();
        super.writeJsonData(response, new AppCommonModel(1, "success", list));
    }

    @RequestMapping("detail")
    public void toDetail(HttpServletResponse response, TOrg org) {
        org = orgService.getOrgById(org.getOrgId());
        Map map = new HashMap();
        map.put("org", org);
        super.writeJsonData(response, new AppCommonModel(1, "success", map));
    }
}
