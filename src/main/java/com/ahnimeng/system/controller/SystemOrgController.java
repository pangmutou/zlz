/**
 * Copyright (c) 2016 amorzhang.com. All Rights Reserved.
 */
package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TDictionary;
import com.ahnimeng.system.model.TOrg;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.service.DictionaryService;
import com.ahnimeng.system.service.OrgService;
import com.ahnimeng.system.util.HttpSessionUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("org")
public class SystemOrgController extends BaseController {

    @Autowired
    DictionaryService dictionaryService;
    @Autowired
    OrgService orgService;

    @RequestMapping("list")
    public ModelAndView toList(Page page, TOrg org) {
        ModelAndView modelAndView = new ModelAndView("pc/admin/org_list");
        org.setOrgStatus(1);
        modelAndView.addObject("page", orgService.page(org, page));
        modelAndView.addObject("title", "机构");
        return modelAndView;
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView toAdd(TOrg org) {
        ModelAndView modelAndView = new ModelAndView("pc/admin/org");
        modelAndView.addObject("method", "add");
        modelAndView.addObject("tOrgRequest", org);
        modelAndView.addObject("methodName", "新增");
        return modelAndView;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public ModelAndView doAdd(TOrg org, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView(REDIRECT + "/org/page");
        TUser user = HttpSessionUtils.getCurrentUser(session);
        //1开发 2运营 3使用
        org.setOrgType(3);
        orgService.save(org, user);
        modelAndView.addObject("org", org);
        return modelAndView;
    }

    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String toUpdate(TOrg org, Model model) {
        model.addAttribute("method", "update");
        model.addAttribute("methodName", "修改");
        org = orgService.get(TOrg.class, org.getOrgId());
        model.addAttribute("org", org);
        List orgCategoryList = dictionaryService.list(TDictionary.class, new Criterion[]{Restrictions.eq("parentCode", "ORG_CATEGORY")}, new Order[]{Order.desc("orderby")}, 0, Integer.MAX_VALUE);
        model.addAttribute("orgCategoryList", orgCategoryList);
        return "pc/admin/org";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String doUpdate(TOrg org, HttpSession session) {
        orgService.update(org, HttpSessionUtils.getCurrentUser(session));
        return REDIRECT + "/org/page";
    }

    @RequestMapping("detail")
    public String toDetail(TOrg org, Model model) {
        model.addAttribute("org", orgService.getOrgById(org.getOrgId()));
        return "pc/sys/detail";
    }

    @RequestMapping("delOrg")
    public String doDelete(TOrg org, HttpSession session) {
        orgService.deleteOrg(org, HttpSessionUtils.getCurrentUser(session));
        return REDIRECT + "/org/page";
    }

    @RequestMapping("exportExcel")
    public void doExportExcel(TOrg org, HttpServletResponse response) throws Exception {
        orgService.exportExcel(response, org);
    }

    @RequestMapping("mine")
    public ModelAndView toMine(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("pc/admin/org");

        TUser user = HttpSessionUtils.getCurrentUser(session);
        TOrg org = orgService.getByUserId(user.getUserId());
        modelAndView.addObject("org", org);
        return modelAndView;
    }
}