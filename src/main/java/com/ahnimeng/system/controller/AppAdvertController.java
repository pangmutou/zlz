package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.system.model.TAdvert;
import com.ahnimeng.system.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by funcong on 2016/2/14.
 */
@Controller
@RequestMapping("app/advert")
public class AppAdvertController extends BaseController {

    @Autowired
    AdvertService advertService;

    @RequestMapping("list")
    public void doList(HttpServletResponse response, TAdvert advert) {
        List list = advertService.page(null, advert);
        super.writeJsonData(response, new AppCommonModel(SUCCESS, "success", list));
    }

}
