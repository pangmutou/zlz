package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.exception.BusinessLogicException;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.common.util.ConstantAuth;
import com.ahnimeng.common.util.MD5Util;
import com.ahnimeng.system.model.TRole;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.model.TUserInfo;
import com.ahnimeng.system.model.enhance.TUserEnhance;
import com.ahnimeng.system.service.OrgService;
import com.ahnimeng.system.service.RoleService;
import com.ahnimeng.system.service.UserInfoService;
import com.ahnimeng.system.service.UserService;
import com.ahnimeng.system.util.HttpSessionUtils;
import com.ahnimeng.system.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("user/info")
public class SystemUserInfoController extends BaseController {

    @Autowired
    @Qualifier("userService")
    UserService userService;
    @Autowired
    @Qualifier("orgService")
    OrgService orgService;
    @Autowired
    @Qualifier("userInfoService")
    private UserInfoService userInfoService;
    @Autowired
    @Qualifier("roleService")
    private RoleService roleService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        super.initBinder(binder, "yyyy-MM-dd", true);
    }

    @RequestMapping("searchUser")
    public void getUser(HttpServletResponse response, String loginName) {
        String result = null;
        loginName = loginName.trim();
        loginName = loginName.replaceAll(" ", "");
        TUser info = userInfoService.getUser(loginName);
        if (info == null) {
            result = "success";
        } else {
            result = "fail";
        }
        super.writeText(response, result);
    }

    @RequestMapping("exportExcel")
    public void exportExcel(HttpServletResponse response, TUserInfo userinfo) throws Exception {
        userService.exportExcel(response, userinfo);
    }

    @RequestMapping(value = "updatePwd", method = RequestMethod.GET)
    public String toUpdatePwd(Integer userid, HttpSession session, Model model) {
        TUser user = HttpSessionUtils.getCurrentUser(session);
//        TUserInfo userinfo = userInfoService.get(TUserInfo.class, "TUser.userId", user.getUserId());
        addModel(model, "user", user);
//        addModel(model, "userinfo", userinfo);
        return "pc/admin/password";
    }

    @RequestMapping(value = "updatePwd", method = RequestMethod.POST)
    public String doUpdatePwd(HttpSession session, String oldPassword, String password) {
        TUser user = HttpSessionUtils.getCurrentUser(session);
        TUser source = userService.get(TUser.class, user.getUserId());
        if (user == null) {
            throw new BusinessLogicException("用户不存在");
        }
        if (!user.getPassword().equals(MD5Util.encoderByMd5(oldPassword))) {
            throw new BusinessLogicException("原密码错误，无法修改");
        }
        user.setPassword(MD5Util.encoderByMd5(password));
        userService.update(user);
        return REDIRECT + "/login";
    }

/*------------------------------------------以下是新写的，上面未注释部分觉得后面可能用得到----------------------------------------------------*/

    @RequestMapping("list")
    public String toList(Model model, HttpSession session, Page<TUserInfo> page, TUserInfo userInfo, TUser user) {
        TUserEnhance currentUser = HttpSessionUtils.getCurrentUser(session);
//        if (!SecurityUtils.isAnyGranted(ConstantAuth.SELECT_ALL_COMMUNITY, currentUser)) {
//            user.setOrgId(currentUser.getOrgId());
//        }
        userInfoService.list(user, userInfo, page);
        addModel(model, "pageList", page);
        boolean result = true;
        if (SecurityUtils.isNotGranted(ConstantAuth.ADD_USER_AUTH, currentUser)) {
            int count = userInfoService.getUserCount(currentUser);
            if (count > 11) {
                result = false;
            }
        }
        addModel(model, "resultCount", result);
        return "pc/admin/user_list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String toAdd(Model model, TRole role) {
        model.addAttribute("pageName", "添加新用户");
        model.addAttribute("roleList", roleService.getAll(role));
        return "pc/admin/user";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String doAdd(HttpSession session, Model model, TUserInfo userInfo, TUser user, String roleId) {
        TUser loggedUser = HttpSessionUtils.getCurrentUser(session);
        try {
            user.setPassword(MD5Util.encoderByMd5(user.getPassword()));
            userInfo.setRealName(user.getUsername());

            userInfoService.save(user, userInfo, loggedUser);
            addModel(model, MSG, SUCCESS);
        } catch (Exception e) {
            logger.error("操作失败，错误信息为：" + e.toString(), e);
            addModel(model, MSG, ERROR);
        }
        return REDIRECT + "/user/info/list";
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public ModelAndView toEdit(@RequestParam Integer userId, TUser user, TUserInfo userInfo, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("pc/admin/user");
        TUser loggedUser = HttpSessionUtils.getCurrentUser(session);
        Map map;
        if (loggedUser.getRoleId() > 11) {
            //如果不是管理员级别的人，只能看看自己的用户信息
            map = userInfoService.getDetailByUserId(loggedUser.getUserId());
        } else {
            map = userInfoService.getDetailByUserId(user.getUserId());
        }
        modelAndView.addObject("object", map);
        return modelAndView;
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String doEdit(Model model, TUser user, TUserInfo userInfo) {
//        userService.update(user);
        userInfoService.update(user, userInfo);
        return REDIRECT + "/user/info/list";
    }

    @RequestMapping("detail/mine")
    public String toDetailMine(Model model, HttpSession session) {
        TUser loggedUser = HttpSessionUtils.getCurrentUser(session);
        Map userDetail = userInfoService.getDetailByUserId(loggedUser.getUserId());
        addModel(model, "object", loggedUser);
        return "pc/admin/user";
    }

    @RequestMapping("del")
    public String doDel(Model model, String userId, HttpSession session) {
//        if(true) throw new RuntimeException("test");
//        TUserInfo userinfo = userInfoService.get(TUserInfo.class, "TUser.userId", userInfoId);
        try {
            userService.delete(Integer.parseInt(userId), HttpSessionUtils.getCurrentUser(session));
            addModel(model, MSG, SUCCESS);
        } catch (Exception e) {
            addModel(model, MSG, ERROR);
        }
//        String url = "/user/querypage?TUser.orgId=" + UnicodeUtils.convert(userinfo.getTUser().getOrgId().toString());
        return REDIRECT + "/user/page";
    }

    @RequestMapping("password/reset")
    public String doPasswordReset(HttpSession session, TUser user) {
//        if(true) throw new RuntimeException("test");

        TUserEnhance loggedUser = HttpSessionUtils.getCurrentUser(session);
        if (SecurityUtils.isGranted("", loggedUser)) {
            userService.passwordReset(user);
        }
        return REDIRECT + "/user/page";
    }
}
