package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TArticle;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.service.ArticleService;
import com.ahnimeng.system.util.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
@RequestMapping("article")
public class SystemArticleController extends BaseController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping("list")
    public String toList(HttpSession session, Model model, Page page, TArticle article) {
//        TUserEnhance loggedUser = HttpSessionUtils.getCurrentUser(session);
//        article.setOrgId(loggedUser.getOrgId());
        page.setResult(articleService.list(page, article));
        model.addAttribute("page", page);
        return "pc/admin/article_list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String toAdd(Model model) {
        model.addAttribute("methodName", "添加公告");
        model.addAttribute("url", "article/add");
        return "pc/admin/article";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String doAdd(HttpSession session, TArticle article) {
        TUser user = HttpSessionUtils.getCurrentUser(session);
        article.setCreateTime(new Date());
        article.setOrgId(user.getOrgId());
        articleService.save(article);
        return REDIRECT + String.format("/article/page?catId=%s", article.getCatId());
    }

    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String toUpdate(Model model, TArticle article) {
        model.addAttribute("methodName", "修改");
        model.addAttribute("url", "article/update");
        model.addAttribute("article", articleService.get(TArticle.class, article.getArticleId()));
        return "pc/admin/article";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String doUpdate(TArticle article) {
        article = articleService.update(article);
        return REDIRECT + String.format("/article/page?catId=%s", article.getCatId());
    }

    @RequestMapping("delete")
    public String delete(TArticle article) {
        articleService.delete(article);
        return REDIRECT + "/article/page";
    }

}
