package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.common.model.ValidSave;
import com.ahnimeng.common.model.ValidSelect;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.model.TUserInfo;
import com.ahnimeng.system.service.SmsService;
import com.ahnimeng.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("app")
public class AppController extends BaseController {

    @Autowired
    SmsService smsService;
    @Autowired
    UserService userService;

    @RequestMapping("register")
    public void register(@Validated({ValidSave.class}) TUser user, @Validated({ValidSave.class}) TUserInfo userInfo, String captcha) {
//        if (user.getPassword().length() < 6) {
//            throw new BusinessLogicException("密码长度不得小于6位");
//        }
        if (smsService.isValid(user.getUsername(), captcha)) {
            userService.register(user, userInfo);
            AppCommonModel appCommonModel = new AppCommonModel(1, "注册成功");
            super.writeJsonData(response, appCommonModel);
        }
    }

    @RequestMapping(value = "login")
    public void login(@Validated({ValidSelect.class}) TUser user) {
        AppCommonModel appCommonModel = new AppCommonModel(1, "登录成功", userService.appLogin(user));
        super.writeJsonData(response, appCommonModel);
    }

}
