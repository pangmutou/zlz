package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.system.cache.impl.AuthCache;
import com.ahnimeng.system.cache.impl.PublicCache;
import com.ahnimeng.system.cache.impl.StaticObjectCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("cacheOperate")
public class SystemUpdateCacheController extends BaseController {

	@Autowired
	@Qualifier("authCache")
	private AuthCache authCache;
	@Autowired
	@Qualifier("publicCache")
	private PublicCache publicCache;
	@Autowired
	@Qualifier("staticObjectCache")
	private StaticObjectCache staticObjectCache;

	@RequestMapping("toUpdate")
	public String toUpdate(){
		return "pc/admin/update";
	}
	
	@RequestMapping("updateCache")
	public String updateCache(Model model){
		authCache.update();
		publicCache.update();
		staticObjectCache.update();
		return REDIRECT+"/cacheOperate/forward";
	}
	
	@RequestMapping("forward")
	public String forward(Model model){
		addModel(model, "message", "缓存更新成功");
		return "pc/admin/update";
	}
}
