package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.common.util.CommonMethod;
import com.ahnimeng.common.util.Constants;
import com.ahnimeng.common.util.FileUtils;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("file")
public class SystemFileController extends BaseController {

    @RequestMapping("upload")
    public ModelAndView doUpload(HttpServletResponse response, HttpServletRequest request, @RequestParam("file") MultipartFile[] file) {
        JSONObject jsonObject;
        try {
            StringBuilder filesName = new StringBuilder();
            for (MultipartFile multipartFile : file) {
                String fileName = FileUtils.saveFile(multipartFile, CommonMethod.getProjectPath(request) + Constants.UPLOAD_FILE_SAVE_PATH, null);
                filesName.append(Constants.UPLOAD_FILE_URL_PATH).append(fileName).append(",");
            }
            if (filesName.length() > 0 && filesName.substring(filesName.length() - 1).equals(",")) {
                filesName.deleteCharAt(filesName.length() - 1);
            }
            AppCommonModel appCommonModel = new AppCommonModel(1, "上传成功", filesName.toString());
            jsonObject = JSONObject.fromObject(appCommonModel, jsonConfig);
            jsonObject.put("success", true);
            super.writeJsonData(response, jsonObject);
        } catch (Exception e) {
            AppCommonModel appCommonModel = new AppCommonModel(0, "上传失败," + e.getMessage());
            super.writeJsonData(response, appCommonModel);
        }
        return null;
    }

    @RequestMapping(value = "delete", method = RequestMethod.DELETE)
    public ModelAndView doDelete() {
        return null;
    }

}
