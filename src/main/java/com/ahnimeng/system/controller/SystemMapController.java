package com.ahnimeng.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("map")
public class SystemMapController {
    /**
     * 功能描述:跳往 经纬度 选择页面
     *
     * @return
     * @author L H T  2013-10-19 下午02:00:34
     */
    @RequestMapping("pick")
    public String toPick() {
        return "pc/admin/pick_map";
    }

}
