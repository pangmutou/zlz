package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.system.model.TAuthority;
import com.ahnimeng.system.service.AuthorityService;
import com.ahnimeng.system.util.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("auth")
public class SystemAuthorityController extends BaseController {

    @Autowired
    private AuthorityService authorityService;

    @RequestMapping("auths")
    public String authoritys(Model model, String msg, String authCode) {
        page.setOrderBy("authOrderby");
        page.setOrder("DESC");
        addModel(model, "authList", authorityService.list(new TAuthority(), null));
//        addModel(model, "authList", authorityService.list(new TAuthority(), "authOrderby", ICommonHqlDao.ORDER_DESC));
        addModel(model, MSG, msg);
        addModel(model, "authCode", authCode);
        return "pc/admin/auth_list";
    }

    @RequestMapping("edit")
    public String editAuth(Integer authId, Model model) {
        addModel(model, "auth", authorityService.get(TAuthority.class, authId));
        page.setOrderBy("authOrderby");
        page.setOrder("DESC");
        addModel(model, "authList", authorityService.list(new TAuthority(), page));
//        addModel(model, "authList", authorityService.getAll(TAuthority.class, "authOrderby", ICommonHqlDao.ORDER_DESC));
        return "pc/admin/auth_ajax";
    }

    @RequestMapping("add")
    public String addAuth(TAuthority auth, Model model) {
        addModel(model, "auth", auth);
        return "pc/admin/auth_ajax";
    }

    @RequestMapping("saveOrUpdate")
    public String saveOrUpdate(TAuthority auth, Model model, HttpSession session) {
        Integer result = authorityService.saveOrUpdateAuth(auth, HttpSessionUtils.getCurrentUser(session));
        if (result > 0) {
            addModel(model, "authCode", auth.getAuthCode());
        }
        addModel(model, MSG, result);
        return REDIRECT + "/auth/auths";
    }

    @RequestMapping("deleteAuth")
    public String delete(String authCode, Model model, HttpSession session) {
        Integer result = authorityService.delete(authCode, HttpSessionUtils.getCurrentUser(session));
        addModel(model, MSG, result);
        return REDIRECT + "/auth/auths";
    }

}
