package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SystemIndexController extends BaseController {

    @RequestMapping("index")
    public String index() {
        return "pc/admin/index";
    }

//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public String toRoot() {
//        return REDIRECT + "/web/index";
//    }
//
//    @RequestMapping(value = "/", method = RequestMethod.DELETE)
//    public void toDelete() {
//    }

}
