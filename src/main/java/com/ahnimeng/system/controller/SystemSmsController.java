package com.ahnimeng.system.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TSms;
import com.ahnimeng.system.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by zhangxiansen on 2016/4/8.
 */
@Controller
@RequestMapping("sms")
public class SystemSmsController extends BaseController {

    @Autowired
    private SmsService smsService;

    @RequestMapping("page")
    public String page(Model model, Page page, TSms sms) {
//        model.addAttribute("page",smsService.queryPage(page, sms));
        model.addAttribute("sms", sms);
        return "pc/sms/sms_list";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String toAdd(Model model) {
        model.addAttribute("methodName", "添加");
        model.addAttribute("url", "sms/add");
        return "pc/sms/sms";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String doAdd(TSms sms) {
        smsService.save(sms);
        return REDIRECT + "/sms/page";
    }

    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String toUpdate(Model model, TSms sms) {
        model.addAttribute("methodName", "修改");
        model.addAttribute("url", "sms/update");
        model.addAttribute("sms", smsService.get(TSms.class, sms.getSmsId()));
        return "pc/sms/sms";
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String doUpdate(TSms sms) {
        smsService.update(sms);
        return REDIRECT + "/sms/page";
    }

    @RequestMapping("delete")
    public String delete(TSms sms) {
        smsService.delete(sms);
        return REDIRECT + "/sms/page";
    }

}
