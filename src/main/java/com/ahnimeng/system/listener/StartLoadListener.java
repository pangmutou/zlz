package com.ahnimeng.system.listener;

import com.ahnimeng.system.cache.SystemCacheService;
import com.ahnimeng.system.cache.impl.AuthCache;
import com.ahnimeng.system.cache.impl.DictionaryCache;
import com.ahnimeng.system.cache.impl.PublicCache;
import com.ahnimeng.system.cache.impl.StaticObjectCache;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 启动时加载监听器
 * Created by funcong on 2016/2/1.
 */
@Component
public class StartLoadListener implements ApplicationListener<ContextRefreshedEvent> {

    Logger logger = Logger.getLogger(this.getClass());

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        //ApplicationEvent有多种子类，ContextRefreshedEvent是ApplicationContext初始化时的事件
        if (event.getApplicationContext().getParent() == null) {
            List<Class<? extends SystemCacheService>> classList = new ArrayList();
            classList.add(AuthCache.class);
            classList.add(DictionaryCache.class);
            classList.add(PublicCache.class);
            classList.add(StaticObjectCache.class);

            for (Class<? extends SystemCacheService> cls : classList) {
                event.getApplicationContext().getBean(cls).run();
                logger.info(cls.getSimpleName() + " loading completed");
            }
        }
    }
}
