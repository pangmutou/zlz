package com.ahnimeng.system.job;

import com.ahnimeng.common.util.SysCode;
import com.ahnimeng.common.util.TimeUtil;
import com.ahnimeng.system.cache.SystemCacheService;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by funcong on 2014/11/4.
 */
@Component
public class AutoExecuteJob implements SystemCacheService {

    static final long oneDay = 24 * 60 * 60 * 1000;
    static final long fifteen = 15 * 60 * 1000;

    org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());

    public void job() {
        Timer timer = new Timer();
        TimerTask autoCheckOrder = new TimerTask() {
            @Override
            public void run() {
                logger.debug("定时执行开始");
                try {
                    expireOrder();
                } catch (Exception e) {
                    logger.error(e, e);
                }
                logger.info("定时执行结束");
            }
        };
        Calendar firstTime = Calendar.getInstance();
//        if (firstTime.getTime().before(new Date())) {
//            firstTime.add(Calendar.MINUTE, 15);
//        }
        timer.schedule(autoCheckOrder, firstTime.getTime(), fifteen);
    }

    /**
     * 对部分工单做过期处理
     */
    public void expireOrder() {
//        List<TOrder> orderList = orderService.listFirstInspectionOrderByExpired();
//        for (TOrder order : orderList) {
//            try {
//                orderService.expireOrder(order.getOrderNumber());
//            } catch (Exception e) {
//                logger.error(e, e);
//            }
//        }
    }

    @Override
    public void run() {
        job();
    }

    @Override
    public void update() {
        logger.info("没有提供自动执行 update方法");
    }

    @Override
    public String getCacheName() {
        return "定时处理模块已加载,时间为:" + TimeUtil.getCurrentDay(SysCode.DATE_FORMAT_NUM_L);
    }
}
