package com.ahnimeng.zlz.model;
// Generated 2017-12-5 19:57:53 by Hibernate Tools 3.2.2.GA


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TBlog generated by hbm2java
 */
@Entity
@Table(name="t_blog"
    ,catalog="zlz"
)
public class TBlog  implements java.io.Serializable {


     private Integer blogId;
     private String blogTitle;
     private String blogAbstract;
     private String blogContent;
     private Integer blogStatus;
     private Date blogCreateTime;
     private Integer blogUserId;
     private Integer blogCateId;

    public TBlog() {
    }

    public TBlog(String blogTitle, String blogAbstract, String blogContent, Integer blogStatus, Date blogCreateTime, Integer blogUserId, Integer blogCateId) {
       this.blogTitle = blogTitle;
       this.blogAbstract = blogAbstract;
       this.blogContent = blogContent;
       this.blogStatus = blogStatus;
       this.blogCreateTime = blogCreateTime;
       this.blogUserId = blogUserId;
       this.blogCateId = blogCateId;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="blog_id", unique=true, nullable=false)
    public Integer getBlogId() {
        return this.blogId;
    }
    
    public void setBlogId(Integer blogId) {
        this.blogId = blogId;
    }
    
    @Column(name="blog_title", nullable=false, length=64)
    public String getBlogTitle() {
        return this.blogTitle;
    }
    
    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }
    
    @Column(name="blog_abstract", nullable=false, length=65535)
    public String getBlogAbstract() {
        return this.blogAbstract;
    }
    
    public void setBlogAbstract(String blogAbstract) {
        this.blogAbstract = blogAbstract;
    }
    
    @Column(name="blog_content", nullable=false)
    public String getBlogContent() {
        return this.blogContent;
    }
    
    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }
    
    @Column(name="blog_status", nullable=false)
    public Integer getBlogStatus() {
        return this.blogStatus;
    }
    
    public void setBlogStatus(Integer blogStatus) {
        this.blogStatus = blogStatus;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="blog_create_time", nullable=false, length=19)
    public Date getBlogCreateTime() {
        return this.blogCreateTime;
    }
    
    public void setBlogCreateTime(Date blogCreateTime) {
        this.blogCreateTime = blogCreateTime;
    }
    
    @Column(name="blog_user_id", nullable=false)
    public Integer getBlogUserId() {
        return this.blogUserId;
    }
    
    public void setBlogUserId(Integer blogUserId) {
        this.blogUserId = blogUserId;
    }
    
    @Column(name="blog_cate_id", nullable=false)
    public Integer getBlogCateId() {
        return this.blogCateId;
    }
    
    public void setBlogCateId(Integer blogCateId) {
        this.blogCateId = blogCateId;
    }




}


