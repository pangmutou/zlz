package com.ahnimeng.zlz.service;

import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.zlz.model.TBlog;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface AdminService {

    /**
     * 后台登录
     * @param user
     * @return
     */
    int adminUserLogin(TUser user,HttpServletRequest request);

    /**
     * 跳转到后台首页
     * @return
     */
    int adminIndex();

    /**
     * 添加博客内容
     * @param blog
     * @param user
     * @return
     */
    TBlog addBlog(TBlog blog,TUser user);

    /**
     * 查询blog列表
     * @param page
     * @param blog
     * @return
     */
    List blogList(Page page,TBlog blog);

}
