package com.ahnimeng.zlz.service.impl;

import com.ahnimeng.common.model.Page;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.service.UserService;
import com.ahnimeng.zlz.model.TBlog;
import com.ahnimeng.zlz.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Service("adminService")
public class AdminServiceImpl implements AdminService {

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @Autowired
    @Qualifier("blogService")
    private BlogServiceImpl blogService;

    /**
     * 后台登录
     *
     * @param user
     * @return
     */
    @Override
    @Transactional
    public int adminUserLogin(TUser user,HttpServletRequest request) {
        return userService.login(user.getUsername(),user.getPassword(),request);
    }

    /**
     * 跳转到后台首页
     *
     * @return
     */
    @Override
    public int adminIndex() {
        return 1;
    }

    /**
     * 添加博客内容
     *
     * @param blog
     * @param user
     * @return
     */
    @Override
    public TBlog addBlog(TBlog blog, TUser user) {
        blog.setBlogStatus(1);
        blog.setBlogCreateTime(new Date());
        blog.setBlogUserId(user.getUserId());
        return blogService.save(blog);
    }

    /**
     * 查询blog列表
     *
     * @param page
     * @param blog
     * @return
     */
    @Override
    public List blogList(Page page, TBlog blog) {
        return null;
    }
}
