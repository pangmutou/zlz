package com.ahnimeng.zlz.service.impl;

import com.ahnimeng.common.service.impl.BaseServiceImpl;
import com.ahnimeng.zlz.model.TBlog;
import org.springframework.stereotype.Service;

@Service("blogService")
public class BlogServiceImpl extends BaseServiceImpl<TBlog> {

    @Override
    public TBlog save(TBlog blog) {
        return baseDaoPlus.save(blog);
    }
}
