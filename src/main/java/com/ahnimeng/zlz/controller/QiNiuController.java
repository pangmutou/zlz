package com.ahnimeng.zlz.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.zlz.utils.QiNiuUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
public class QiNiuController extends BaseController {

    /**
     * 获取客户端上传
     */
    @RequestMapping("file/qiniu/uptoken")
    public void getQiNiuUpToken(){
        Map data = new HashMap();
        data.put("uptoken", QiNiuUtils.getQiniuUploadUpToken());
        super.writeJsonData(response,data);
    }
}
