package com.ahnimeng.zlz.controller;

import com.ahnimeng.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController extends BaseController {

    @RequestMapping("web/index")
    public String webIndex(){
        return "pc/web/index";
    }

    @RequestMapping("web/blog/list")
    public String webBlogList(){

        return "pc/web/blog_list";
    }
}
