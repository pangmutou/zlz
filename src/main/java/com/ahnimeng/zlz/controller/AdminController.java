package com.ahnimeng.zlz.controller;

import com.ahnimeng.common.controller.BaseController;
import com.ahnimeng.system.model.TUser;
import com.ahnimeng.system.util.HttpSessionUtils;
import com.ahnimeng.zlz.model.TBlog;
import com.ahnimeng.zlz.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AdminController extends BaseController {

    @Autowired
    @Qualifier("adminService")
    private AdminService adminService;


    @RequestMapping(value = "admin/user/login",method = RequestMethod.GET)
    public String toAdminUserLogin(){
        return "pc/admin/page/admin_user_login";
    }

    @RequestMapping(value = "admin/user/login",method = RequestMethod.POST)
    public String doAdminUserLogin(HttpServletRequest request,TUser user){
        adminService.adminUserLogin(user,request);
        return REDIRECT+"/admin/index";
    }

    /**
     * 后台首页
     * @return
     */
    @RequestMapping("admin/index")
    public String toAdminIndex(){
        return "pc/admin/page/admin_index";
    }

    /**
     * 跳往添加博客的界面
     * @param blog
     * @return
     */
    @RequestMapping(value = "admin/blog/add",method = RequestMethod.GET)
    public String toAdminBlogAdd(TBlog blog){
        return "pc/admin/page/admin_blog_add";
    }

    @RequestMapping(value = "admin/blog/add",method = RequestMethod.POST)
    public String doAdminBlogAdd(TBlog blog){
        TUser user = HttpSessionUtils.getCurrentUser(session);
        adminService.addBlog(blog,user);
        return REDIRECT+"/admin/blog/list";
    }

    @RequestMapping("admin/blog/list")
    public String adminBlogList(){
        return "pc/admin/page/admin_blog_list";
    }
}
