package com.ahnimeng.common.util;

public class Constants {

    //项目名
    public static final String PROJECT_NAME = "胖师兄的博客";

    //环信默认密码
    public static final String HUANXIN_DEFAULT_PASSWORD = "111111";

    /**
     * url 地址配置
     */
    public static final String URL_PATH = "/url.properties";

    /**
     * email 地址配置
     */
    public static final String EMAILCONFIG_PATH = "/emailconfig.properties";

    /**
     * 页面消息配置 路径
     */
    public static final String PAGECONFIG_PATH = "/pagemessage.properties";

    /**
     * 短信通知配置文件
     */
    public static final String SMS_PATH = "/sms.properties";

    /**
     * 用户的session key值
     */
    public static final String SESSION_KEY_USERINFO = "systemUser";

    /**
     * 左边菜单url sessionKEY
     */
    public static final String SEESION_KEY_LEFTURLS = "leftUrls";

    /**
     * 机构状态
     */
    public static final int ORG_STATUS = 1;

    /**
     * 默认角色
     */
    public static final int ROLE_DEFAULT = 1;

    /**
     * 用户正常状态
     */
    public static final int USER_STATUS = 1;
    /**
     * 用户删除状态
     */
    public static final int USER_DEL = 0;

    /**
     * 权限类型URL
     */
    public static final String AUTHORITY_TYPE_URL = "1";

    /**
     * 权限类型 权限
     */
    public static final String AUTHORITY_TYPE_AUTH = "2";

    /**
     * 新加用户默认密码
     */
    public static final String USER_DEFULT_PASSWORD = "123456";

    /**
     * 上传文件最大50
     */
    public static final int FILE_MAX_SIZE_FILE = 50 * 1000 * 1000;

    public static final long NEED_COMPRESS_SIZE = 500 * 1024;

    /**
     * 需要的点号
     */
    public static final String FILE_BIT = ".";

    /**
     * 需要的逗号
     */
    public static final String FILE_COMMA = ",";

    /**
     * 文件路径
     */
    public static final String UPLOAD_FILE_SAVE_PATH = "\\file\\upload\\";
    public static final String UPLOAD_FILE_URL_PATH = "file/upload/";

    /**
     * 操作日志的类型--用户管理
     */
    public static final String OPERA_TYPE_USER = "5";

    /**
     * 操作日志的类型--角色管理
     */
    public static final String OPERA_TYPE_ROLE = "6";

    /**
     * 操作日志的类型--权限管理
     */
    public static final String OPERA_TYPE_AUTH = "7";

    /**
     * 操作日志的类型 --机构管理
     */
    public static final String OPERA_TYPE_ORG = "9";

    /**
     * 判断用户填写的是否为电话号码
     */
    public static final String BARS = "-";

    public static final Integer IMG_BYTES = 500 * 1024;

    /**
     * 朋友圈屏蔽状态值
     */
    public static final Integer CIRCLE_STATUS_DELETE = 0;

    /**
     * 朋友圈正常状态值
     */
    public static final Integer CIRCLE_STATUS_NORMAL = 1;

    /**
     * 返回执行结果值
     * RESULT_SUCCESS 成功
     * RESULT_FAIL 失败
     */
    public static final Integer RESULT_SUCCESS = 1;
    public static final Integer RESULT_FAIL = 0;
}
