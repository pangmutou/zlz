package com.ahnimeng.common.exception;

import com.ahnimeng.common.model.AppCommonModel;
import com.ahnimeng.common.util.EmptyUtils;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

/**
 * Created by funcong on 2017/5/2.
 */
public class CommonExceptionHandlerResolver implements HandlerExceptionResolver {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        AppCommonModel appCommonModel;
        if (e instanceof ExtendException) {
            //是自定义的错误
            if (e instanceof BusinessLogicException) {
                Integer errCode = Integer.valueOf(0);
                if(EmptyUtils.isNotEmpty(((BusinessLogicException) e).getErrorCode())){
                    errCode = Integer.valueOf(((BusinessLogicException) e).getErrorCode());
                }
                appCommonModel = new AppCommonModel(errCode, e.getMessage());
                log.debug("[业务逻辑错误]" + e.toString());
            } else {
                throw new RuntimeException("未知的错误类型，请检查异常代码");
            }
        } else if (e instanceof BindException) {
            BindException bindException = (BindException) e;
            appCommonModel = new AppCommonModel(0,
                    String.format("参数'%s'%s",
                            bindException.getFieldError().getField(),
                            bindException.getFieldError().getDefaultMessage()));
            log.debug("[参数错误]" + e.toString());
        } else {
            appCommonModel = new AppCommonModel(-1, e.toString());
            log.error(httpServletRequest.getServletPath() + " - " + e.getMessage(), e);
        }

        if (httpServletRequest.getServletPath().indexOf("/app/") != 0
                && StringUtils.isEmpty(httpServletRequest.getParameter("ajax"))) {
            try {
                String msg = URLEncoder.encode(appCommonModel.getMsg(), "UTF-8");
                httpServletResponse.sendRedirect(httpServletRequest.getServletPath() + "?msg=" + msg);
                return null;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        JSONObject jsonObject = JSONObject.fromObject(appCommonModel);

        httpServletResponse.setContentType("application/json");
        PrintWriter printWriter = null;
        try {
            printWriter = httpServletResponse.getWriter();
            printWriter.print(jsonObject.toString());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        }
        return null;
    }

}
